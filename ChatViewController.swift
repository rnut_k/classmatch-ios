//
//  ChatViewController.swift
//  ClassMatch
//
//  Created by Mac on 10/15/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import RealmSwift
import FirebaseDatabase
class ChatViewController: JSQMessagesViewController {
    
    var room:Room!
    var messages = [Message]()
//    var messages = [JSQMessage]()
    var avatars = Dictionary<String, UIImage>()
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    var incomingAvatarImageView: JSQMessagesAvatarImage!
    var ref: FIRDatabaseReference!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor();
        self.inputToolbar.contentView.leftBarButtonItem = nil
        title = room.getPartner().name
        configureDatabase()
        hideTabbar()
        setupBubbles()
        setUpAvatar()
        
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero
        self.senderId = String(User.getSharedInStance()!.id)
        self.senderDisplayName = User.getSharedInStance()!.name
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(animated: Bool) {
        showTabbar()
        cm.activeRoom = nil
        cm.shoutRoom = nil
        super.viewWillDisappear(animated)
    }
    
    func configureDatabase() {
        cm.delegate = self
        cm.activeRoom = room
        ref = FIRDatabase.database().reference().child("Chat/Rooms").child(room.path)
        parseMessageFromRoom()
        finishReceivingMessage()
    }
    private func parseMessageFromRoom(){
        messages.removeAll()
        for msg in room.messages {
            addMessage(msg)
        }
    }
    func addMessage(item:Message) {
//        let message = JSQMessage(senderId: id, senderDisplayName: "", date: date, text: text)
        messages.append(item)
    }
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!,
                                     senderDisplayName: String!, date: NSDate!) {
        let itemRef = ref.childByAutoId()
        let messageItem = [
            "senderId": senderId,
            "senderName": User.getSharedInStance()!.name,
            "receiverId" : room.getPartner().id,
            "text": text,
            "roomId" : room.id,
            "timestamp":FIRServerValue.timestamp()
        ]
        itemRef.setValue(messageItem)
        self.finishSendingMessage()
    }
    
    
    override func textViewDidChange(textView: UITextView) {
        super.textViewDidChange(textView)
    }
    override func collectionView(collectionView: JSQMessagesCollectionView!,messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        let message = messages[indexPath.item]
        let jsqMessage = JSQMessage(senderId: message.senderId, senderDisplayName: "", date: message.createdAt, text: message.text)
        return jsqMessage
    }
    
    override func collectionView(collectionView: UICollectionView,numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    override func collectionView(collectionView: JSQMessagesCollectionView!,messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    override func collectionView(collectionView: JSQMessagesCollectionView!,avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId != senderId{
            return incomingAvatarImageView
        }
        return nil
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.item]
        var strBottomLable = ""
        if message.id != room.partnerReadMessageId{
            strBottomLable = "Delivered  "
        }else{
            strBottomLable = "Read  "
        }
        strBottomLable += message.createdAt.toStringOnlyTime()
        let attribute = NSAttributedString(string: strBottomLable)
        return attribute;
    }
    override func collectionView(collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        let message = messages[indexPath.item]
        log.info("message.id: \(message.id)  readMessage: \(room.partnerReadMessageId)")
        if message.id == room.partnerReadMessageId {
            return 20
        }
        if messages.last?.id == message.id{
            return 20
        }
        return 0
        
        
    }

    override func collectionView(collectionView: UICollectionView,cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            cell.textView!.textColor = UIColor.whiteColor()
        } else {
            cell.textView!.textColor = UIColor.blackColor()
        }
        return cell
    }
    override func collectionView(collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAtIndexPath indexPath: NSIndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.item]
        let attribute = NSAttributedString(string: message.createdAt.toString(true))
        return attribute;
    }
    private func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        outgoingBubbleImageView = factory.outgoingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleBlueColor())
        incomingBubbleImageView = factory.incomingMessagesBubbleImageWithColor(UIColor.jsq_messageBubbleLightGrayColor())
    }
    func setUpAvatar(){
        if let image = room.getRoomImage(){
            ImageHelper.downLoadAvatarImage(image.path!, completionHandler: { (success, image) in
                if success{
                    self.incomingAvatarImageView = JSQMessagesAvatarImageFactory.avatarImageWithImage(image, diameter: 150)
                    self.finishSendingMessage()
                }
            })
        }
    }
}
extension ChatViewController:ChatManagerDelegate{
    func didUpdateReadMessageId(messageId: String) {
//        readMessageId = messageId
        finishReceivingMessageAnimated(false)
    }
    func didReceiveMessage(message: Message) {
        if message.senderId != senderId{
            //is Receiver
            JSQSystemSoundPlayer.jsq_playMessageReceivedAlert()
            JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
            ref.child("read").child(senderId).setValue(message.id)
        }else{
            //is Sender
            JSQSystemSoundPlayer.jsq_playMessageSentSound()
            
        }
        addMessage(message)
        finishSendingMessage()
    }
}

//
//  Extension+UITableViewCell.swift
//  ClassMatch
//
//  Created by Mac on 9/25/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit
public extension UITableViewCell
{
    func addSeparator(y: CGFloat, margin: CGFloat, color: UIColor)
    {
        let sepFrame = CGRectMake(margin, y, self.frame.width - margin-margin, 0.7);
        let seperatorView = UIView(frame: sepFrame);
        seperatorView.backgroundColor = color;
        self.addSubview(seperatorView);
    }
    
    public func addTopSeparator(tableView: UITableView)
    {
        let margin = tableView.separatorInset.left;
        self.addSeparator(0, margin: margin, color: tableView.separatorColor!);
    }
    public func addSectionTopSeparator(tableView: UITableView,color:UIColor)
    {
        let margin:CGFloat = 5;
        let sepFrame = CGRectMake(0, 0, tableView.bounds.width - ( margin * 2 ), 0.7);
        let seperatorView = UIView(frame: sepFrame);
        seperatorView.center = CGPointMake(tableView.center.x, seperatorView.center.y)
        seperatorView.backgroundColor = color;
        self.addSubview(seperatorView);
    }
    public func addBottomSeparator(tableView: UITableView, cellHeight: CGFloat,color:UIColor)
    {
        let margin = tableView.separatorInset.left;
        self.addSeparator(cellHeight-2, margin: margin, color: color);
    }
    public func addBottomSeparator(tableView: UITableView, cellHeight: CGFloat)
    {
        let margin = tableView.separatorInset.left;
        
        self.addSeparator(cellHeight-2, margin: margin, color: tableView.separatorColor!);
    }
    
    public func removeSeparator(width: CGFloat)
    {
        self.separatorInset = UIEdgeInsetsMake(0.0, width, 0.0, 0.0);
    }
    
}

public extension UIViewController{
    
    
    func presentLoadingHud(){
        let progress = HUDProgressViewController(status: "Logging in ...")
        self.presentViewController(progress, animated: true, completion: nil)
    }
    func changeBackButtonToCloseButton(){
        let image = UIImage(named: "closeButton")
        let backButton = UIBarButtonItem(image: image, style: .Plain, target: self, action: #selector(self.closeViewController(_:)))
        self.navigationItem.leftBarButtonItem = backButton;
    }
    func closeViewController(sender:AnyObject){
        dismissViewControllerAnimated(false, completion: nil)
    }
    func hideTabbar(){
        self.tabBarController?.tabBar.hidden = true
    }
    func showTabbar(){
        self.tabBarController?.tabBar.hidden = false
    }
    
    func dismissFreezeViewController(completion:(()->Void)?){
        guard let presentingViewController = self.presentingViewController else { return completion!() }
        presentingViewController.dismissViewControllerAnimated(false) {
            return completion
        }
//        if let presentedViewController = self.presentedViewController{
//            presentedViewController.dismissViewControllerAnimated(true){
//                completion?()
//            }
//        }else{
//            completion?()
//        }
    }
    func showLoadingHudViewController(completion:(()->Void)?){
        if let presentedViewController = self.presentedViewController{
            presentedViewController.dismissViewControllerAnimated(true){
                let progress = HUDProgressViewController(status: "Doing...")
                self.presentViewController(progress, animated: true, completion: {
                    completion?()
                })
            }
        }else{
            let progress = HUDProgressViewController(status: "Doing...")
            self.presentViewController(progress, animated: true, completion: {
                completion?()
            })
        }
    }
    func showErrorHudViewController(message:String,completion:(()->Void)?){
        if let presentedViewController = self.presentedViewController{
            presentedViewController.dismissViewControllerAnimated(true){
                let alertController = UIAlertController(title: ERROR_MESSAGE.alertError.rawValue, message: message, preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                    completion?()
                })
                alertController.addAction(defaultAction)
                
                self.presentViewController(alertController, animated: true, completion: {})
            }
        }else{
            let alertController = UIAlertController(title: ERROR_MESSAGE.alertError.rawValue, message: message, preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                completion?()
            })
            alertController.addAction(defaultAction)
        
            self.presentViewController(alertController, animated: true, completion: {})
        }
    }
    func showMessageViewController(title:String,message:String,completion:(()->Void)?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in })
        alertController.addAction(defaultAction)
        
        self.presentViewController(alertController, animated: true, completion: {
            completion?()
        })
    }
    func showAlertViewController(title:String,message:String,completion:(()->Void)?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
            return completion
        })
        alertController.addAction(defaultAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}

extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.dateByAddingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: NSTimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.dateByAddingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}

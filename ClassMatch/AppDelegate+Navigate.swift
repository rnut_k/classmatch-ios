//
//  AppDelegate+Navigate.swift
//  ClassMatch
//
//  Created by Arnut on 12/9/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import HUDKit
//MARK: NAVIGATE TO VIEWCONTROLLER
extension AppDelegate{
    
    func guestLanding(){
        guard let window = self.window else { return }
        guard let mainStoryboard = Utility.getStoryboard(STORYBOARD.main) else{ return }
        guard let entryNavigation = mainStoryboard.instantiateInitialViewController() as? UINavigationController else{
            return
        }
        window.rootViewController = entryNavigation
    }
    func memberLanding(){
        
    }
    func gotoLandingViewController(){
        guard let window = self.window else { return }
        guard let mainStoryboard = Utility.getStoryboard(STORYBOARD.main) else{ return }
        let entryNavigation = mainStoryboard.instantiateInitialViewController()
        window.rootViewController = entryNavigation
    }
    func getCurrentPresentViewController()->UIViewController?{
        guard let window = self.window else { return nil }
        guard let rootVc = window.rootViewController else { return nil }
        if let navigationControllerVC = rootVc as? UINavigationController{
            return navigationControllerVC.visibleViewController
        }
        return rootVc
    }
    func presentAlertAdmissionCanceled(admissionId:Int){
        let alertController = UIAlertController(title: "Classmatch", message: "การขอรับสอนของคุณถูกปฏิเสธจากนักเรียน ลองใหม่อีกครั้งนะค้ะ ขอบคุณที่ใช้บริการค่ะ", preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "ตกลง", style: .Cancel) { (action) in
        }
        alertController.addAction(actionOk)
        
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController?.presentViewController(alertController, animated: false, completion: nil)
            })
        }else{
            window?.rootViewController?.presentViewController(alertController, animated: false, completion: nil)
        }
    }
    func presentAlertReNewTransaction(admissionId:Int){
        let alertController = UIAlertController(title: "แจ้งเตือน", message: "หลักฐานยืนยันการโอนเงินของคุณไม่ชัด กรุณายืนยันการโอนเงินอีกครั้งค่ะ", preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "ตกลง", style: .Cancel) { (action) in
            self.gotoStudyingApprovalPaymentViewController(admissionId)
        }
        alertController.addAction(actionOk)
        
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController?.presentViewController(alertController, animated: false, completion: nil)
            })
        }else{
            window?.rootViewController?.presentViewController(alertController, animated: false, completion: nil)
        }
    }
    func gotoChatViewController(room:Room){
        cm.shoutRoom = room
        
        guard let tabbar = ad.getTopViewController() as? CustomTabbarController else { return }
        if tabbar.selectedIndex == 3{
            if let navController = tabbar.selectedViewController as? NavController{
                if let roomsViewController = navController.viewControllers[0] as? RoomsViewController{
                    roomsViewController.gotoRoom(room)
                }
            }
        }
        tabbar.selectedIndex = 3
        
        
    }
    func gotoMessageViewController(){
        let messageViewController = Utility.getMessageViewController()
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController = messageViewController
            })
        }else{
            window?.rootViewController = messageViewController
        }
    }
    func gotoEntryViewController(){
        let entryViewController = Utility.getEntryViewController()
        window?.rootViewController = entryViewController
    }
    func gotoTabbarViewController(){
        let tabbarViewController = Utility.getTabbarViewController()
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController = tabbarViewController
            })
        }else{
            window?.rootViewController = tabbarViewController
        }
    }
    func gotoTabbarViewController(atIndex:Int){
        let tabbarViewController = Utility.getTabbarViewController()
        if let rootViewController = window?.rootViewController as? UITabBarController{
            if rootViewController == tabbarViewController{
                return rootViewController.selectedIndex = atIndex
            }
        }
        window?.rootViewController = tabbarViewController
        tabbarViewController.selectedIndex = atIndex
    }
    func presentAlertSessionExpire(){
        let alertController = UIAlertController(title: "การเข้าสู่ระบบผิดพลาด", message: "เซสชั่นของคุณหมดอายุ กรุณาเข้าสู่ระบบอีกครั้งค่ะ", preferredStyle: .Alert)
        let actionOk = UIAlertAction(title: "ตกลง", style: .Cancel) { (action) in
            self.gotoEntryViewController()
        }
        alertController.addAction(actionOk)
        
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController?.presentViewController(alertController, animated: false, completion: nil)
            })
        }else{
            window?.rootViewController?.presentViewController(alertController, animated: false, completion: nil)
        }
        
    }
    func gotoStudyingApprovalViewController(){
        let storyboard = Utility.getStudyingApprovalStoryboard()
        let studyingApprovalViewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.studying_approval.rawValue) as! StudyingApprovalViewController
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController?.presentViewController(studyingApprovalViewController, animated: false, completion: nil)
            })
        }else{
            window?.rootViewController?.presentViewController(studyingApprovalViewController, animated: false, completion: nil)
        }
        
    }
    func gotoStudyingApprovalPaymentViewController(admissionId:Int,dismissCurrentViewController:Bool){
        let storyboard = Utility.getStudyingApprovalStoryboard()
        let studyingPaymentViewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.studying_payment.rawValue) as! StudyingPaymentViewController
        studyingPaymentViewController.admissionId = admissionId
        guard let window = window else { return }
        guard let rootView = window.rootViewController else { return }
    
        if dismissCurrentViewController{
            rootView.dismissViewControllerAnimated(false, completion: {
                rootView.presentViewController(studyingPaymentViewController, animated: true, completion: nil)
            })
        }
        rootView.presentViewController(studyingPaymentViewController, animated: true, completion: nil)
    }
    func gotoStudyingApprovalPaymentViewController(admissionId:Int){
        let storyboard = Utility.getStudyingApprovalStoryboard()
        let studyingPaymentViewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.studying_payment.rawValue) as! StudyingPaymentViewController
        studyingPaymentViewController.admissionId = admissionId
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController?.presentViewController(studyingPaymentViewController, animated: false, completion: nil)
            })
        }
        else{
            window?.rootViewController?.presentViewController(studyingPaymentViewController, animated: false, completion: nil)
        }
    }
    func gotoStudyingApprovedViewController(){
        let storyboard = Utility.getStudyingApprovalStoryboard()
        let studyingApprovedViewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.studying_approved.rawValue) as! StudyingApprovedViewController
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController?.presentViewController(studyingApprovedViewController, animated: false, completion: nil)
            })
        }
        else{
            window?.rootViewController?.presentViewController(studyingApprovedViewController, animated: false, completion: nil)
        }
    }
    func gotoStudentApproveStudyingViewController(admissionId:Int){
        let storyboard = Utility.getStudyingApprovalStoryboard()
        let studentApproveStudyingViewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.student_approve_studying.rawValue) as! StudentApproveViewController
        studentApproveStudyingViewController.admissionId = admissionId
        //        window?.rootViewController = studentApproveStudyingViewController
        if let presented = window?.rootViewController?.presentedViewController{
            presented.dismissViewControllerAnimated(false, completion: {
                self.window?.rootViewController?.presentViewController(studentApproveStudyingViewController, animated: false, completion: nil)
            })
        }
        else{
            window?.rootViewController?.presentViewController(studentApproveStudyingViewController, animated: false, completion: nil)
        }
    }
    func getTopViewController()->UIViewController?{
        if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    func presentHudInRootViewController(){
        let progress = HUDProgressViewController(status: "Authenticationing")
        if let presenting = getCurrentPresentViewController(){
            presenting.presentLoadingHud()
        }
        return (window?.rootViewController?.presentViewController(progress, animated: true, completion: nil))!
    }
    func dismissHudViewController(completionHandler:()->Void){
        if let presenting = getCurrentPresentViewController(){
            if let _ = presenting as? HUDProgressViewController{
                presenting.dismissViewControllerAnimated(false, completion: {
                    return completionHandler()
                })
            }
            
        }
    }

    
}
     
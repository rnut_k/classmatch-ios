//
//  CustomTabbarController.swift
//  ClassMatch
//
//  Created by Mac on 10/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//
import UIKit
import Foundation

class CustomTabbarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor =  colorBase
        let firstTab = self.tabBar.items![0]
        firstTab.image = UIImage(named: TABBAR_IMAGE.HOME.rawValue)?.imageWithRenderingMode(.AlwaysOriginal)
        let secondTab = self.tabBar.items![1]
        secondTab.image = UIImage(named: TABBAR_IMAGE.ALL_CLASSES.rawValue)?.imageWithRenderingMode(.AlwaysOriginal)
        let thirdTab = self.tabBar.items![2]
        thirdTab.image = UIImage(named: TABBAR_IMAGE.NOTIFICATIONS.rawValue)?.imageWithRenderingMode(.AlwaysOriginal)
        let fourthTab = self.tabBar.items![3]
        fourthTab.image = UIImage(named: TABBAR_IMAGE.MESSAGE.rawValue)?.imageWithRenderingMode(.AlwaysOriginal)
        let fifthTab = self.tabBar.items![4]
        fifthTab.image = UIImage(named: TABBAR_IMAGE.ME.rawValue)?.imageWithRenderingMode(.AlwaysOriginal)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
}

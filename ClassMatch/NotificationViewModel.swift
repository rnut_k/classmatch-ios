//
//  NotificationViewModel.swift
//  ClassMatch
//
//  Created by Mac on 12/7/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import RealmSwift
class NotificationViewModel: NSObject {
    
    weak var controller:NotificationViewController!
    var studyings = List<Studying>()
    var admissions = List<Admission>()
    var teachings = List<Teaching>()
    
    convenience init(controller:NotificationViewController) {
        self.init()
        self.controller = controller
    }
    func callApi(){
        ProfileManager.getNotifications { (success, admissions, studyings, teachings) in
            if success{
                if let studyings = studyings{
                    self.studyings = studyings
                }
                if let admissions = admissions{
                    self.admissions = admissions
                }
                if let teachings = teachings{
                    self.teachings = teachings
                }
                self.controller.tableView.reloadData()
            }
        }
    }
}

//MARK: Tableview Datasource-Delegate
extension NotificationViewModel:UITableViewDataSource,UITableViewDelegate{
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationCell", forIndexPath: indexPath) as! NotificationViewCell
        switch indexPath.section {
        case 0:
            let studying = studyings[indexPath.row]
            let status:StatusNotification = studying.approved == 0 ? .waiting : studying.approved == 1 ? .approved : .cancel
            let detail = studying.getDetail()
            cell.configureCell(status, notificationDetail: detail,admissionStatus: studying.getLatestAdmision()?.status)
        case 1:
            let admission = admissions[indexPath.row]
            let status:StatusNotification = admission.approved == 0 ? .waiting : admission.approved == 1 ? .active : admission.approved == 2 ? .approved : .cancel
            let detail = admission.getDetail()
            cell.configureCell(status, notificationDetail: detail,admissionStatus: nil)
        default:
            break
        }
        return cell
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return studyings.count
        case 1:
            return admissions.count
        default:
            return teachings.count
        }
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "โพสต์เรียน"
        case 1:
            return "สนใจเป็นผู้สอน"
        default:
            return "มีผู้สนใจมาเรียน"
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            //studying
            let studying = studyings[indexPath.row]
            tapStudyingSection(studying)
            break
        case 1:
            //adminssion
            let admission = admissions[indexPath.row]
            tapAdmissionSection(admission)
        default:
            break
        }
    }
    
    private func tapAdmissionSection(admission:Admission){
        log.info("tapAdmissionSection::\(admission)")
        if admission.approved == 0{
            let alertController = UIAlertController(title: "Classmatch", message: "โพสต์ที่คุณสนใจนี้ยังอยู่ระหว่างรอคิว ระบบจะแจ้งเตือนเมื่อถึงคิวของคุณ ขอบคุณค่ะ", preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in })
            alertController.addAction(defaultAction)
            controller.presentViewController(alertController, animated: true, completion: nil)
        }else if admission.approved == 1{
            controller.showAlertViewController("Classmatch", message: "โพสต์ที่คุณสนใจรับสอน อยู่ระหว่างการพิจารณายืนยันจากนักเรียน ระบบจะแจ้งเตือนเมื่อมีการยืนยัน ขอบคุณค่ะ", completion: {
            })
            
        }else if admission.approved == 2{
            guard let relateStudying = admission.studying else {
                return controller.showAlertViewController("Classmatch", message: "ไม่สามารถเรียกข้อมูล การโพสต์เรียนได้", completion: {
                    
                })
            }
            if relateStudying.approved == 0{
                ad.gotoStudyingApprovalPaymentViewController(admission.id, dismissCurrentViewController: false)
            }else if admission.studying.approved == 1 {
                controller.showAlertViewController("Classmatch", message: "ขั้นตอนการรับสอนของคุณสมบูรณ์แล้ว ขอบคุณที่ใช้บริการค่ะ", completion: {
                })
            }else if admission.studying.approved == 2 {
                controller.showAlertViewController("Classmatch", message: "การโพสต์เรียนนี้ ถูกระงับ ขออภัยในความไม่สะดวกค่ะ", completion: {
                })
            }
        }else if admission.approved == 3{
            controller.showAlertViewController("Classmatch", message: "โพสต์ที่คุณสนใจรับสอน ไม่ผ่านการพิจารณาจากนักเรียน ขอบคุณที่ใช้บริการค่ะ", completion: {
                
            })
        }
    }
    
    private func tapStudyingSection(studying:Studying){
        log.info("tapStudyingSection::\(studying)")
        switch studying.approved {
        case 0:
            if let activeAdmission = studying.getActiveAdmission(){
                ad.gotoStudentApproveStudyingViewController(activeAdmission.id)
            }else{
                controller.showAlertViewController("Classmatch", message: "อยู่ระหว่างดำเนินการหาคุณครูค่ะ", completion: {
                })
            }
            return
        case 1:
            return controller.showAlertViewController("Classmatch", message: "การโพสต์เรียนนี้เสร็จสมบูรณ์แล้ว ขอบคุณที่ใช้บริการค่ะ", completion: {
            })
        default:
            return controller.showAlertViewController("Classmatch", message: "การโพสต์เรียนนี้ถูกยกเลิก ขออภัยในความไม่สะดวกค่ะ", completion: {
            })
        }
    }
    
    
}
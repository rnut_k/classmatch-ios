//
//  StudentPostViewModel.swift
//  ClassMatch
//
//  Created by Mac on 9/11/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

protocol StudentPostViewModelDelegate {
    func didScrollToRow(index:Int)
    func beginRequest()
    func endRequest()
    func didFailRequest()
    func didEndRequestSubjects()
}
protocol StudentPostCellDelegate {
    func didTabAdmission(studying:Studying)
    func didTabSeeDetail(studying:Studying)
}
class StudentPostViewModel:NSObject {
    

    var delegate:StudentPostViewModelDelegate!
    var delegatePostCell:StudentPostCellDelegate!
    private var datasource = [Studying]()
    
    
    var subjects = [Subject]()
    override init() {
        super.init()
        getSubjects()
    }
    
    func getSubjects(){
        StudentHelper.getSubjects { (success, subjects) in
            if success{
                guard let subjects = subjects else {return}
                self.subjects = subjects
                self.delegate.didEndRequestSubjects()
            }
        }
    }
    
    func get(index:Int)->Studying?{
        if index < datasource.count{
            return datasource[index]
        }
        return nil
    }
    func count()->Int{
        return datasource.count
    }
    func add(studying:Studying){
        datasource.append(studying)
    }
    func clear(){
        datasource.removeAll()
    }
    
    
    private func mockUpModel(){
        for _ in  0..<4 {
            let studying = Studying(value: ["age":"25","number":"3"])
            add(studying)
        }
        
    }
    
    func requestPostDataFromServer(){
        delegate.beginRequest()
        StudentHelper.getAllPost { (success, posts) in
            if success{
                if let posts = posts{
                    self.datasource = posts
                    self.delegate.endRequest()
                }
            }else{
                self.delegate.didFailRequest()
            }
        }
    }
}

//MARK: TABLEVIEW
extension StudentPostViewModel:UITableViewDataSource,UITableViewDelegate{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count()
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("studentPostCell", forIndexPath: indexPath) as! StudentPostCell
        if let studying = get(indexPath.row){
            cell.configureCell(studying)
            cell.vm = self
        }
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if (count() > 0) {
            return 1;
        } else {
            let frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height)
            let messageLabel = UILabel(frame: frame)
            messageLabel.text = "No data is curently available."
            messageLabel.textColor = colorBase
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .Center
            messageLabel.sizeToFit()
            tableView.backgroundView = messageLabel
            tableView.separatorStyle = .None;
        }
        return 0;
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        delegate.didScrollToRow(indexPath.row)
    }
}



//
//  RegisterViewModel.swift
//  ClassMatch
//
//  Created by Mac on 8/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
protocol RegisterViewModelDelegate {
    func registerViewModelshoudGotoNext(index:Int)
    func registerViewModelshoudGotoPrevious(index:Int)
    func registerViewModelDidRegisterSuccess()
    func registerViewModelDidRegisterError()
}
class RegisterViewModel:NSObject,UITextFieldDelegate {
    let user = User()
    var password:String!
    var delegate:RegisterViewModelDelegate!
    
    override init() {
        super.init()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("textFieldDidEndEditing")
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("textField_shouldChangeCharactersInRange")
        return true
    }
    func goNextPage(fromIndex:Int,info:String){
        switch fromIndex {
        case 0:
            //email
            user.email = info
            break
        case 1:
            //name
            user.name = info
            break
        case 2:
            //birthdate
            user.birthDate = info
            break
        default:
            //password
            password = info
            break
        }
        self.delegate.registerViewModelshoudGotoNext(fromIndex)
    }
}

extension RegisterViewModel:FBSDKLoginButtonDelegate{
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if result.isCancelled{
            print("ERROR::RegisterViewModel::didCompleteWithResult::isCancelled")
            self.delegate.registerViewModelDidRegisterError()
            return
        }
        let token = result.token.tokenString
        print("loginButtondidCompleteWithResult : \(token)")
        startLogin(token)
        
    }
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    func startRegister(){
        AuthenticationManager.register(user.email, passWord: password, name: user.name, birthDate: user.birthDate) { (success, jwt) in
            if success{
                if let jwt = jwt{
                    self.getUserInfo(jwt)
                    return
                }
            }else{
                print("\nERROR::LoginViewModel::startLogin\n")
                self.delegate.registerViewModelDidRegisterError()
            }
            
        }
    }
    
    func startLogin(token:String){
        AuthenticationManager.loginWithFacebook(token) { (success, jwt) in
            if success{
                if let jwt = jwt{
                    self.getUserInfo(jwt)
                    return
                }
            }
            print("\nERROR::LoginViewModel::startLogin\n")
        }
        
    }
    
    func getUserInfo(jwt:String){
        AuthenticationManager.getUserInfo(jwt) { (success, userInfo) in
            if success{
                if let userInfo = userInfo{
                    if let email = userInfo["email"].string{
                        if let name = userInfo["name"].string{
                            print("RegisterViewModel::getUserInfo::Success::\(email),\(name)")
                            self.delegate.registerViewModelDidRegisterSuccess()
                            return
                        }
                    }
                }
                print("\nERROR::LoginViewModel::getUserInfo\n")
                self.delegate.registerViewModelDidRegisterError()
            }
        }
        
    }
}
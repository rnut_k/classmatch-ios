//
//  TeacherCreateViewModel.swift
//  ClassMatch
//
//  Created by Mac on 9/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

protocol TeacherCreateViewModelDelegate {
    func didReloadData()
    func didUpdateLabelSubjects()
    func didUpdateLabelDisplayable(profile:String)
    
}

class Profile:NSObject {
    var profile:AnyObject!
    var isActive = false
    override init() {
        super.init()
    }
}
class TeacherCreateViewModel: NSObject {
    
    let teacher = Teacher()
    var delegate:TeacherCreateViewModelDelegate!
    
    private var profiles = [Profile]()
    private var educations = [Education]()
    private var workings = [Working]()
    private var subjects = [Subject]()
    private var profileSubjects = [Profile]()
    
    
    override init() {
        super.init()
    }
    func requestDataSource(){
        requestSubjects()
        queryProfiles()
    }

}

//MARK: SUBJECT DATASOURCE
extension TeacherCreateViewModel{
    func requestSubjects(){
        StudentHelper.getSubjects { (success, subjects) in
            if success{
                if let subjects = subjects{
                    self.subjects = subjects
                    for subject in subjects{
                        let profile = Profile()
                        profile.isActive = false
                        profile.profile = subject
                        self.profileSubjects.append(profile)
                    }
                    self.delegate.didReloadData()
                }
            }
        }
    }
    private func toggleSelectedSubject(index:Int){
        if let profile  = getProfileSubject(index){
            if setSelectSubjectIsExist(index){
                profile.isActive = false
            }
            else{
                profile.isActive = true
            }
            
        }
        if teacher.subjects.count > 4{
            let removingObject = teacher.subjects[0]
            for profileSubject in profileSubjects{
                if let subject = profileSubject.profile as? Subject{
                    if subject.id == removingObject.id{
                        profileSubject.isActive = false
                    }
                }
            }
            teacher.subjects.removeAtIndex(0)
        }
        delegate.didReloadData()
        delegate.didUpdateLabelSubjects()
    }
    
    private func setSelectSubjectIsExist(index:Int)->Bool{
        if let index = teacher.subjects.indexOf(subjects[index]){
            teacher.subjects.removeAtIndex(index)
            return true
        }
        teacher.subjects.append(subjects[index])
        return false
    }
    private func getProfileSubject(index:Int)->Profile?{
        if index < profileSubjects.count{
            return profileSubjects[index]
        }
        return nil
    }
    private func getProfileSubjectsTitle(index:Int)->String{
        var title = ""
        let selectedItem = getProfileSubject(index)!
        if let subject = selectedItem.profile as? Subject{
            if let name = subject.name{
                title += name
            }
        }
        return title
    }
}
//MARK: PROFILE DATASOURCE
extension TeacherCreateViewModel{
    private func queryProfiles(){
        guard let user = User.getSharedInStance() else {return }
        educations = user.educations.toArray()
        workings = user.workings.toArray()
        for education in educations {
            let profile = Profile()
            profile.isActive = false
            profile.profile = education
            profiles.append(profile)
        }
        for working in workings {
            let profile = Profile()
            profile.isActive = false
            profile.profile = working
            profiles.append(profile)
        }
        delegate.didReloadData()
    }
    private func getProfile(index:Int)->Profile?{
        if index < getProfilesCount(){
            return profiles[index]
        }
        return nil
    }
    private func getProfileTitle(index:Int)->String{
        let selectedItem = getProfile(index)!
        if let education =  selectedItem.profile as? Education{
            return education.getFullDetail()
        }
        else if let working = selectedItem.profile as? Working{
            return working.getFullDetail()
        }
        else if let subject = selectedItem.profile as? Subject{
            if let name = subject.name{
                return name
            }
        }
        return ""
    }
    private func getProfilesCount()->Int{
        return profiles.count
    }
    
    private func setSelectProfile(index:Int){
        let selectedItem = getProfile(index)!
        if let education = selectedItem.profile as? Education{
            teacher.education = education
            teacher.displayableType = Displayable.Education.rawValue
            return delegate.didUpdateLabelDisplayable(education.getFullDetail())
        }
        if let working = selectedItem.profile as? Working{
            teacher.working = working
            teacher.displayableType = Displayable.Working.rawValue
            return delegate.didUpdateLabelDisplayable(working.getFullDetail())
        }
    }
    private func toggleSelectedProfile(index:Int){
        for profile in profiles {
            profile.isActive = false
        }
        if let profile = getProfile(index){
            profile.isActive = true
        }
        setSelectProfile(index)
        delegate.didReloadData()
        
    }
}


extension TeacherCreateViewModel:UITableViewDataSource,UITableViewDelegate{
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if getProfilesCount() > 0{
                return 45
            }
            return 0
        default:
            if subjects.count > 0{
                return 45
            }
            return 0
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if getProfilesCount() > 0{
                return 45
            }
            return 0
        default:
            if subjects.count > 0{
                return 45
            }
            return 0
        }
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL.TEACHER_CREATE_FOOTER.rawValue)
        cell?.removeSeparator(tableView.bounds.size.width)
        return cell
        
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL.TEACHER_CREATE_HEADER.rawValue) as! TeachingRegisterHeaderCell
        switch section {
        case 0:
            cell.labelHeader.text = "เลือก 1 ข้อ"
            break
        case 1:
            cell.labelHeader.text = "เลือกได้สูงสุด 4 ข้อ"
        default:
            break
        }
        cell.removeSeparator(tableView.bounds.width)
        return cell
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL.TEACHER_CREATE_SELECTOR.rawValue, forIndexPath: indexPath) as! TeachingRegisterSeloctorCell
            cell.labelTitle.text = getProfileTitle(indexPath.row)
            if let profile = getProfile(indexPath.row){
                if profile.isActive{
                    cell.imageViewRadioControl.image = UIImage(named: kImageControlToggleActive)
                }else{
                    cell.imageViewRadioControl.image = UIImage(named: kImageControlToggleInActive)
                }
            }
            cell.removeSeparator(tableView.bounds.width)
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier(CELL.TEACHER_CREATE_SELECTOR.rawValue, forIndexPath: indexPath) as! TeachingRegisterSeloctorCell
            cell.labelTitle.text = getProfileSubjectsTitle(indexPath.row)
            if let profile = getProfileSubject(indexPath.row){
                if profile.isActive{
                    cell.imageViewRadioControl.image = UIImage(named: kImageControlToggleActive)
                }else{
                    cell.imageViewRadioControl.image = UIImage(named: kImageControlToggleInActive)
                }
            }
            cell.removeSeparator(tableView.bounds.width)
            return cell
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return getProfilesCount()
        }else{
            return subjects.count
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section
        {
        case 0:
            toggleSelectedProfile(indexPath.row)
            break
        default:
            toggleSelectedSubject(indexPath.row)
            break
        }
    }
}
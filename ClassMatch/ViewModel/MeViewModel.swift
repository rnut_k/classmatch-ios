//
//  MeViewModel.swift
//  ClassMatch
//
//  Created by Mac on 8/28/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import UIKit
protocol MeViewModelDelegate {
    func didChoosePublicMenu()
    func didChooseImagePicker()
    func performEvidence(type:EvidenceType,item:AnyObject)
}

class MeViewModel: NSObject {
    var meImage:UIImage!
    var delegate:MeViewModelDelegate!
    override init(){
        super.init()
        meImage = UIImage(named: "tempProfileImage")
    }
    
    func chooseImagePicker(){
        delegate.didChooseImagePicker()
    }
    func performEvidence(type:EvidenceType,item:AnyObject){
        delegate.performEvidence(type, item: item)
    }
}

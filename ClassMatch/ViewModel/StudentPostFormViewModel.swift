//
//  StudentPostFormViewModel.swift
//  ClassMatch
//
//  Created by Mac on 9/11/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//


import UIKit
import GooglePlaces


protocol StudentPostFormDelegate {
    func didOpenPicker(sender:AnyObject)
    func didPresentKeyboard(sender:AnyObject)
    func didDismissKeyboard(sender:AnyObject)
    func didDismissInput()
    func shouldPresentGooglePlaceField()
    func shouldDismissGooglePlaceField()
    func didReloadCollectionView()
    func didSuccessValidate(index:Int)
    func didErrorValidate(index:Int)
}
class StudentPostFormViewModel: NSObject {
    
    
    let pages = 3
    var activeTextField:UITextField?
    let kCellOverFlow:CGFloat = 150
    var delegate:StudentPostFormDelegate!
    //PickerViewDataSource
    var pickerDataSource = [AnyObject]()
    var pickerMode:PickerMode!
    //COLLECT UI ARRAY
    var controlDay = [UIImageView]()
    var controlPrice = [UIImageView]()
    var controlPlace = [UIImageView]()
    var textFieldsGeneral = [UITextField]()
    var prices = [250,300,500,750,1000,0]
    //MARK: STUDYING DATA
    let studying = Studying()
    var days = [Int]()

    
    override init(){
        super.init()
        if let subjects = Subject.getSubjects(){
            subjectPickerData = subjects
        }else{
            StudentHelper.getSubjects({ (success, subjects) in
                if success{
                    if let subjects = subjects{
                        return self.subjectPickerData = subjects
                    }
                }
            })
        }
        StudentHelper.getDegrees { (success, degree) in
            if success{
                if let degrees = degree{
                    self.degreePickerData = degrees
                }
            }
        }
    }
    
    //MARK:Image button number of student
    let imageSingleDeAction = "studying_single_student_de-action"
    let imageMultipleDeaction = "studying_multiple_student"
    let imageSingleAction = "studying_single_student_active"
    let imageMultipleAction = "studying_multiple_student_active"
    
    var numberPickerData =  [
        ["key": "2","value": 2],
        ["key": "3","value": 3],
        ["key": "4","value": 4],
        ["key": "5","value": 5],
        ["key": "มากกว่า 5","value": 6]
    ]
    var subjectPickerData = [Subject]()
    var periodPickerData = [
        ["key":"1 ชั่วโมง","value":"1 ชั่วโมง"],
        ["key":"2 ชั่วโมง","value":"2 ชั่วโมง"],
        ["key":"3 ชั่วโมง","value":"3 ชั่วโมง"],
        ["key":"ครึ่งวัน","value":"ครึ่งวัน"],
        ]
    var agePickerData = [
        ["key":"1 ปี" ,"value":"1 ปี"],
        ["key":"3 ปี" ,"value":"3 ปี"],
        ["key":"10 ปี" ,"value":"10 ปี"]
    ]
    var degreePickerData = [Degree]()
    var datingPickerData = [
        ["key":"เสานี้เป็นต้นไป" ,"value":"เสานี้เป็นต้นไป"],
        ["key":"เสาร์หน้าเป็นไป" ,"value":"เสาร์หน้าเป็นไป"],
        ["key":"เดือนหน้า" ,"value":"เดือนหน้า"]
    ]
    func getTitleForPickerView(data: [AnyObject])->[String]{
        var dataSource = [String]()
        for object in data {
            if let title = object["key"] as? String{
                dataSource.append(title)
            }
        }
        return dataSource
    }
    
    
}

enum PickerMode:Int{
    case NumberOfStudent = 0
    case Subject = 1
    case Period = 2
    case Age = 3
    case Degree = 4
    case Dating = 5
    case Tag = 6
    case Message = 7
}

//MARK: TEXTFIELD
extension StudentPostFormViewModel:UITextFieldDelegate{
    func splitTags(str:String)->[String]{
        let strArr = str.characters.split{$0 == " "}.map(String.init)
        return strArr
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if textField.tag == 9{
            if let text = textField.text{
                let tagArray = splitTags(text)
                for item in tagArray {
                    let tag = Tag(name: item)
                    studying.tags.append(tag)
                }
                
            }
        }else if textField.tag == 11{
            if let text = textField.text{
                studying.message = text
            }
        }
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        switch textField.tag {
        case 11:
            //message
            pickerMode = PickerMode.Message
            delegate.didPresentKeyboard(textField)
            break
        default:
            break
        }
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        activeTextField = textField
        switch textField.tag {
        case 1:
            //number of student
            pickerMode = PickerMode.NumberOfStudent
            pickerDataSource = numberPickerData
            delegate.didOpenPicker(textField)
            return false
        case 2:
            //subject
            pickerMode = PickerMode.Subject
            pickerDataSource = subjectPickerData
            delegate.didOpenPicker(textField)
            return false
        case 3:
            delegate.shouldPresentGooglePlaceField()
            return false
        case 4:
            delegate.shouldPresentGooglePlaceField()
            return false
        case 5:
            //period
            pickerMode = PickerMode.Period
            pickerDataSource = periodPickerData
            delegate.didOpenPicker(textField)
            return false
        case 6:
            //price
            delegate.didPresentKeyboard(textField)
            return true
        case 7:
            //age
            pickerMode = PickerMode.Age
            pickerDataSource = agePickerData
            delegate.didOpenPicker(textField)
            return false
        case 8:
            //degree
            pickerMode = PickerMode.Degree
            pickerDataSource = degreePickerData
            delegate.didOpenPicker(textField)
            return false
        case 9:
            //tag
            pickerMode = PickerMode.Tag
            delegate.didPresentKeyboard(textField)
            return true
        case 10:
            //dating
            pickerMode = PickerMode.Dating
            pickerDataSource = datingPickerData
            delegate.didOpenPicker(textField)
            return false
        case 11:
            //message
            pickerMode = PickerMode.Message
            delegate.didPresentKeyboard(textField)
            return true
        default:
            delegate.didPresentKeyboard(textField)
            return true
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        switch textField.tag {
        case 5:
            break
        case 6:
            //price
            if let value = textField.text where textField.text != ""{
                if let price = Int(value){
                    studying.price = price
                    return  delegate.didDismissInput()
                    
                }
                return studying.price = 0
            }
            
            break

        case 9:
            delegate.didDismissInput()
            break
        case 11:
            delegate.didDismissInput()
            break
        default:
            break
        }
    }
}

//MARK: PICKERVIEW
extension StudentPostFormViewModel:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        let title = pickerDataSource[row]["key"] as! String
//        activeTextField?.text = title
        var title = ""
        //update vm value
        switch pickerMode.rawValue {
        case PickerMode.NumberOfStudent.rawValue:
            let value = numberPickerData[row]["value"] as! Int
            let key = numberPickerData[row]["key"] as! String
            title = key
            studying.number = value
            break
        case PickerMode.Subject.rawValue:
            let value = subjectPickerData[row]
            if let name = value.name{
                title = name
            }
            studying.subject = value
            break
        case PickerMode.Period.rawValue:
            let value = periodPickerData[row]["value"]
            let key = periodPickerData[row]["key"]
            title = key!
            studying.period = value
            break
        case PickerMode.Age.rawValue:
            let value = agePickerData[row]["value"]
            let key = agePickerData[row]["key"]
            title = key!
            studying.age = value
            break
        case PickerMode.Degree.rawValue:
            let value = degreePickerData[row]
            let key = value.name
            title = key
            studying.degree = value
            break
        case PickerMode.Dating.rawValue:
            //TODO: fullfill getDateFromReadableString
            let value = datingPickerData[row]["value"]
            let key = datingPickerData[row]["key"]
            let date = getDateFromReadableString(value!)
            title = key!
            studying.date = date
            break
        default:
            break
        }
        activeTextField?.text = title
    }
    func getDateFromReadableString(text:String)->NSDate{
        return NSDate()
    }
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var str = ""
        switch pickerMode.rawValue {
        case PickerMode.NumberOfStudent.rawValue:
            let value = numberPickerData[row]["key"] as! String
            str = value
            break
        case PickerMode.Subject.rawValue:
            let value = subjectPickerData[row]
            if let subjectName = value.name{
                str = subjectName
            }
            break
        case PickerMode.Period.rawValue:
            let value = periodPickerData[row]["key"]
            str = value!
            break
        case PickerMode.Age.rawValue:
            let value = agePickerData[row]["key"]
            str = value!
            break
        case PickerMode.Degree.rawValue:
            let value = degreePickerData[row]
            str = value.name
            break
        case PickerMode.Dating.rawValue:
            //TODO: fullfill getDateFromReadableString
            let value = datingPickerData[row]["key"]
//            let date = getDateFromReadableString(value!)
//            return studying.date = date
            str = value!
            break
        default:
            break
        }
//        let string =  pickerDataSource[row]["key"] as! String
        return NSAttributedString(string: str, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }
}

//MARK: CONTROL LOGIC
extension StudentPostFormViewModel{
    //TEXTFILE COLLECT
    func collectTextField(textField:UITextField){
        textFieldsGeneral.append(textField)
    }
    //PLACE CONTROL
    func collectControlPlace(control:UIImageView){
        controlPlace.append(control)
    }
    func resetPlaceToggleControl(){
        for control in controlPlace {
            let imageNotToggle = UIImage(named: kImageControlToggleInActive)
            control.image = imageNotToggle
        }
    }
    func placesControlPressed(sender:UITapGestureRecognizer){
        if let imageView =  sender.view as? UIImageView{
            if let index = controlPlace.indexOf(imageView){
                resetPlaceToggleControl()
                imageView.image = toggleImage(imageView.image!)
                cleanTextfield(textFieldsGeneral[1])
                cleanTextfield(textFieldsGeneral[2])
                delegate.shouldPresentGooglePlaceField()
                let textfield = textFieldsGeneral[index+1]
                activeTextField = textfield
            }
        }
    }
    func cleanTextfield(textField:UITextField){
        textField.text = ""
        textField.resignFirstResponder()
        textField.enabled = false
    }
    //DAYS CONTROL
    func daysControlPressed(sender:UITapGestureRecognizer){
        if let imageView =  sender.view as? UIImageView{
            if let position = controlDay.indexOf(imageView){
                imageView.image = toggleImage(imageView.image!,index: position)
            }
        }
    }
    func attachDays(dayId:Int){
        if let dayModel = uiRealm.objectForPrimaryKey(Day.self, key: dayId) {
            studying.days.append(dayModel)
        }
    }
    func detachDays(dayId:Int){
        var flagFind = false
        var index = 0
        for day in studying.days {
            if day.id == dayId{
                flagFind = true
                break
            }
            index += 1
        }
        if flagFind{
            studying.days.removeAtIndex(index)
        }
        
    }
    func toggleImage(image:UIImage)->UIImage{
        let imageNotToggle = UIImage(named: kImageControlToggleInActive)
        if image == imageNotToggle{
            let imageToggle = UIImage(named: kImageControlToggleActive)
            return imageToggle!
        }
        return imageNotToggle!
    }
    func toggleImage(image:UIImage,index:Int)->UIImage{
        let imageNotToggle = UIImage(named: kImageControlToggleInActive)
        if image == imageNotToggle{
            let imageToggle = UIImage(named: kImageControlToggleActive)
            attachDays(index+1)
            return imageToggle!
        }
        detachDays(index+1)
        return imageNotToggle!
    }
    func collectControlDay(control:UIImageView){
        controlDay.append(control)
    }
    
    //PRICE CONTROL
    func pricesControlPressed(sender:UITapGestureRecognizer){
        if let imageView = sender.view as? UIImageView{
            let index = controlPrice.indexOf(imageView)
            studying.price = prices[index!]
            togglePriceControl(imageView)
            let textfileCustomPrice = textFieldsGeneral[4]
            if imageView == controlPrice.last{
                textfileCustomPrice.enabled = true
                textfileCustomPrice.becomeFirstResponder()
            }else{
                textfileCustomPrice.resignFirstResponder()
                textfileCustomPrice.enabled = false
                delegate.didDismissKeyboard(textfileCustomPrice)
            }
        }
    }
    func togglePriceControl(focusControl:UIImageView){
        let imageNotToggle = UIImage(named: kImageControlToggleInActive)
        let imageToggle = UIImage(named: kImageControlToggleActive)
        focusControl.image = imageToggle
        for control in controlPrice {
            if control == focusControl {
                continue
            }
            control.image = imageNotToggle
        }
    }
    func collectControlPrice(control:UIImageView){
        controlPrice.append(control)
    }
}



extension StudentPostFormViewModel: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        let location = ParserHelper.parsePlace(place, save: true)
        var text = location.name
        if let detail = location.detail{
            text = text + detail
        }
        activeTextField?.text = text
        studying.location = location
        delegate.shouldDismissGooglePlaceField()
    }
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // TODO: handle the error.
        print("Error: ", error.description)
        delegate.shouldDismissGooglePlaceField()
        activeTextField?.text = ""
        studying.location = nil
        resetPlaceToggleControl()
    }
    
    // User canceled the operation.
    func wasCancelled(viewController: GMSAutocompleteViewController) {
        delegate.shouldDismissGooglePlaceField()
        activeTextField?.text = ""
        studying.location = nil
        resetPlaceToggleControl()
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}

extension StudentPostFormViewModel:UICollectionViewDataSource,UICollectionViewDelegate{
    func chooseSingleStudent(sender:AnyObject){
        studying.number = 1
        delegate.didReloadCollectionView()
    }
    func chooseMultipleStudent(){
        studying.number = 2
        delegate.didReloadCollectionView()
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("student_post_number", forIndexPath: indexPath) as! StudentNumberCell
            cell.textfieldNumber.tag = 1
            cell.textfieldNumber.delegate = self
            let gesture = UITapGestureRecognizer(target: self, action: #selector(StudentPostFormViewModel.chooseSingleStudent))
            let gesture2 = UITapGestureRecognizer(target: self, action: #selector(StudentPostFormViewModel.chooseMultipleStudent))
            cell.imageButtonSingle.addGestureRecognizer(gesture)
            cell.imageButtonMultiple.addGestureRecognizer(gesture2)
            cell.vm = self
            cell.configureCell()
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("student_post_general", forIndexPath: indexPath) as! StudentGeneralCell
            cell.vm = self
            cell.textFieldSubject.tag = 2
            cell.textFieldPeriod.tag = 5
            cell.textFieldPrice.tag = 6
            cell.textFieldSubject.delegate = self
            cell.textFieldPlaceHome.delegate = self
            cell.textFieldPlaceOther.delegate = self
            cell.textFieldPeriod.delegate = self
            cell.textFieldPrice.delegate = self
            collectTextField(cell.textFieldSubject)
            collectTextField(cell.textFieldPlaceHome)
            collectTextField(cell.textFieldPlaceOther)
            collectTextField(cell.textFieldPeriod)
            collectTextField(cell.textFieldPrice)
            
            cell.textFieldPrice.enabled = false
            
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.daysControlPressed(_:)))
            let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.daysControlPressed(_:)))
            let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.daysControlPressed(_:)))
            let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.daysControlPressed(_:)))
            let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(self.daysControlPressed(_:)))
            let tapGesture6 = UITapGestureRecognizer(target: self, action: #selector(self.daysControlPressed(_:)))
            let tapGesture7 = UITapGestureRecognizer(target: self, action: #selector(self.daysControlPressed(_:)))
            cell.controlDay1.addGestureRecognizer(tapGesture)
            cell.controlDay2.addGestureRecognizer(tapGesture2)
            cell.controlDay3.addGestureRecognizer(tapGesture3)
            cell.controlDay4.addGestureRecognizer(tapGesture4)
            cell.controlDay5.addGestureRecognizer(tapGesture5)
            cell.controlDay6.addGestureRecognizer(tapGesture6)
            cell.controlDay7.addGestureRecognizer(tapGesture7)
            collectControlDay(cell.controlDay1)
            collectControlDay(cell.controlDay2)
            collectControlDay(cell.controlDay3)
            collectControlDay(cell.controlDay4)
            collectControlDay(cell.controlDay5)
            collectControlDay(cell.controlDay6)
            collectControlDay(cell.controlDay7)
            
            let tapGesturePlaceHome = UITapGestureRecognizer(target: self, action: #selector(self.placesControlPressed(_:)))
            let tapGesturePlaceOther = UITapGestureRecognizer(target: self, action: #selector(self.placesControlPressed(_:)))
            cell.controlPlaceHome.addGestureRecognizer(tapGesturePlaceHome)
            cell.controlPlaceOther.addGestureRecognizer(tapGesturePlaceOther)
            collectControlPlace(cell.controlPlaceHome)
            collectControlPlace(cell.controlPlaceOther)
            cell.textFieldPlaceHome.enabled = false
            cell.textFieldPlaceOther.enabled = false
            
            
            let pricesTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.pricesControlPressed(_:)))
            let pricesTapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.pricesControlPressed(_:)))
            let pricesTapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.pricesControlPressed(_:)))
            let pricesTapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.pricesControlPressed(_:)))
            let pricesTapGesture5 = UITapGestureRecognizer(target: self, action: #selector(self.pricesControlPressed(_:)))
            let pricesTapGesture6 = UITapGestureRecognizer(target: self, action: #selector(self.pricesControlPressed(_:)))
            cell.controlPrice250.addGestureRecognizer(pricesTapGesture)
            cell.controlPrice300.addGestureRecognizer(pricesTapGesture2)
            cell.controlPrice500.addGestureRecognizer(pricesTapGesture3)
            cell.controlPrice750.addGestureRecognizer(pricesTapGesture4)
            cell.controlPrice1000.addGestureRecognizer(pricesTapGesture5)
            cell.controlPriceCustom.addGestureRecognizer(pricesTapGesture6)
            collectControlPrice(cell.controlPrice250)
            collectControlPrice(cell.controlPrice300)
            collectControlPrice(cell.controlPrice500)
            collectControlPrice(cell.controlPrice750)
            collectControlPrice(cell.controlPrice1000)
            collectControlPrice(cell.controlPriceCustom)
            
            let gestureUp  = UISwipeGestureRecognizer(target: self, action: #selector(self.scrollGeneralCellToBottom(_:)))
            gestureUp.direction = .Up
            cell.scrollViewGeneral.addGestureRecognizer(gestureUp)
            let gestureDown  = UISwipeGestureRecognizer(target: self, action: #selector(self.scrollGeneralCellToTop(_:)))
            gestureDown.direction = .Down
            cell.scrollViewGeneral.addGestureRecognizer(gestureDown)
            cell.configure()
            return cell
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("student_post_other", forIndexPath: indexPath) as! StudentOtherCell
            cell.textfieldAge.delegate = self
            cell.textfieldDegree.delegate = self
            cell.textfieldTags.delegate = self
            cell.textfieldDating.delegate = self
            cell.textfieldMessage.delegate = self
            
            cell.textfieldAge.tag = 7
            cell.textfieldDegree.tag = 8
            cell.textfieldTags.tag = 9
            cell.textfieldDating.tag = 10
            cell.textfieldMessage.tag = 11
            return cell
        }
        
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func scrollGeneralCellToBottom(sender:UISwipeGestureRecognizer){
        let view = sender.view
        if let scrollview = view as? UIScrollView{
            let  bottomOffset = CGPointMake(0,kCellOverFlow);
            scrollview.setContentOffset(bottomOffset, animated: true)
            
        }
    }
    func scrollGeneralCellToTop(sender:UISwipeGestureRecognizer){
        let view = sender.view
        if let scrollview = view as? UIScrollView{
            let  bottomOffset = CGPointMake(0,0);
            scrollview.setContentOffset(bottomOffset, animated: true)
        }
        
    }
    func callDelegateDismissKeyboard(){
        delegate.didDismissInput()
    }
    
    
    func validateMainData(){
        let model = studying
        var validatedMainData = true
        if model.subject == nil{
            validatedMainData = false
        }
        
        if model.days.count == 0 {
            validatedMainData = false
        }
        
        if model.location == nil{
            validatedMainData = false
        }
        
        if model.period == nil{
            validatedMainData = false
        }
        
        if model.price == 0{
            validatedMainData = false
        }
        if validatedMainData {
            delegate.didSuccessValidate(1)
        }else{
            delegate.didErrorValidate(1)
        }
    }
    
    func validateNumberOfStudent(){
        if studying.number > 0{
            return delegate.didSuccessValidate(0)
        }
    }
    func validateOtherData(){
        let model = studying
        var validateOtherData = true
        if model.age != nil{
            validateOtherData = false
        }
        if model.degree != nil{
            validateOtherData = false
        }
        //TODO: other field is not require?
        if validateOtherData{
            delegate.didSuccessValidate(2)
        }
    }
}

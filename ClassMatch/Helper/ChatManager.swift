//
//  ChatManager.swift
//  ClassMatch
//
//  Created by Mac on 10/17/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import SwiftyJSON
import Whisper
import FirebaseDatabase
import RealmSwift

protocol ChatManagerDelegate {
    func didReceiveMessage(message:Message)
    func didUpdateReadMessageId(messageId:String)
}
struct RoomObserve {
    var room:Room!
    var ref:FIRDatabaseReference!
    var readRef:FIRDatabaseReference!
}
class ChatManager: NSObject {
    deinit{
        log.info("deinit")
    }
    var refs = [UInt]()
    //static let kDidNotificate = "inAppNotify"
    static let rootRef = FIRDatabase.database().reference().child("Chat/Rooms")
    var delegate:ChatManagerDelegate!
    var activeRoom:Room?
    var shoutRoom:Room?
    var roomObservers = [RoomObserve]()

    static func queryRooms(user:User)->Results<Room>{
        let userId = String(user.id)
        let condition1 = "sender.id = " + userId
        let condition2 = "receiver.id = " + userId
        let totalCondition = condition1 + " or " + condition2
        let rooms = uiRealm.objects(Room.self).filter(totalCondition)
        return rooms
    }
    func addRooms(rooms:[Room]){
        for room in rooms {
            var flagIsNewRoom = true
            for roomObserver in roomObservers {
                if roomObserver.room.isEqual(room){
                    flagIsNewRoom = false
                }
            }
            if flagIsNewRoom{
                self.addRoomObserve(room)
            }
        }
        let deletedRoom = cm.getRooms().filter({ (room) in
            var flag = true
            for newRoom in rooms{
                if room.id == newRoom.id{
                    flag = false
                }
            }
            return flag
        })
        for deleteRoom in deletedRoom{
            removeRoom(deleteRoom)
        }
    }
    func removeRoom(room:Room){
        ChatManager.removeChatRoomObserver(room.path)
        cm.roomObservers = cm.roomObservers.filter({ (roomObserve) in return roomObserve.room != room })
        
    }
    static func start(){
        cm = ChatManager()
    }
    func tapMessageFromNotification(userInfo:[NSObject : AnyObject]){
        let json = JSON(userInfo)
        guard let roomId = json["roomId"].string else{ return }
        if let _ = cm.activeRoom {
            return
        }
        if let room = cm.getRoom(roomId){
            return ad.gotoChatViewController(room)
        }else{
            ChatManager.getRooms({ (success, rooms) in
                if success{
                    guard let rooms = rooms else { return }
                    cm.addRooms(rooms)
                    guard let room  = cm.getRoom(roomId) else { return }
                    return ad.gotoChatViewController(room)
                }
            })
        }
    }
    func getRooms()->[Room]{
        var rooms = [Room]()
        for roomOb in roomObservers {
            rooms.append(roomOb.room)
        }
        return  rooms
    }
    func getRoom(id:String)->Room?{
        let rooms = roomObservers.filter { (roomObserver) -> Bool in
            return id == roomObserver.room.id
        }
        if let roomObserve = rooms.first{
            return roomObserve.room
        }
        return nil
    }
    func addRoomObserve(room:Room)->UInt{
        let messages = room.messages
        let observer:FIRDatabaseHandle!
        let roomRef = ChatManager.rootRef.child(room.path)
        if messages.count > 0{
            guard let lastMessage = messages.sorted("createdAt",ascending: false).first else {return 0}
            roomRef.queryStartingAtValue(lastMessage.id).queryOrderedByKey()
        }
        
        observer = roomRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
            let key = snapshot.key
            if let _ = room.messages.filter("id = %@",key).first{
                return
            }
            if key == ""{ return }
            guard let postDict = snapshot.value as? [String:AnyObject] else{return}
            guard let text = postDict["text"] as? String else{return}
            guard let senderId = postDict["senderId"] as? String else{return}
            guard let timeStamp = postDict["timestamp"] as? NSTimeInterval else {return}
            let created_at = NSDate(timeIntervalSince1970: timeStamp/1000)
            guard let message = Message.create(key, senderId: senderId, text: text, createdAt: created_at,room: room) else{return}
            room.add(message)
            self.shout(room, message: message)
        })
        
        let readRef = roomRef.child("read")
        readRef.observeEventType(.Value, withBlock: { (snapshot) in
            let key = snapshot.key
            guard let postDict = snapshot.value as? [String:AnyObject] else{return}
            if key == "read"{
                let partnerId = String(room.getPartner().id)
                guard let messageId = postDict[partnerId] as? String else { return }
                room.updatePartnerReadMessageId(messageId)
                if room == cm.activeRoom{
                    cm.delegate.didUpdateReadMessageId(messageId)
                }
                return room.updatePartnerReadMessageId(messageId)
            }
        })
        
        let roomObserver = RoomObserve(room: room, ref: roomRef,readRef: readRef)
        cm.roomObservers.append(roomObserver)
        return observer
    }
    func shout(room:Room,message:Message){
        if let activeRoom = cm.activeRoom{
            if room == activeRoom {
                return self.delegate.didReceiveMessage(message)
            }
        }
        if let currentTabbar  =  ad.getCurrentPresentViewController() as? CustomTabbarController{
            if currentTabbar.selectedIndex == 3{
                return
            }
        }
        if ad.activeTime.compare(message.createdAt) == .OrderedDescending{
            return
        }
        var shoutImage:UIImage!
        if let image = room.getRoomImage(){
            if let path = image.path{
                ImageHelper.downLoadAvatarImage(path, completionHandler: { (success, image) in
                    if success{
                        shoutImage = image
                    }else{
                        shoutImage = UIImage(named: IMAGENAME.placeholder_profile_square.rawValue)
                    }
                    let announcement = Announcement(title: room.getPartner().name, subtitle: message.text, image: shoutImage, duration: 3.0) {
                        ad.gotoChatViewController(room)
                    }
                    guard let presentingViewController = ad.getTopViewController() else { return }
                    show(shout: announcement, to: presentingViewController, completion: {
                        print("The shout was silent.")
                    })
                })
            }
        }
        else{
            shoutImage = UIImage(named: IMAGENAME.placeholder_profile_square.rawValue)
            let announcement = Announcement(title: room.getPartner().name, subtitle: message.text, image: shoutImage, duration: 3.0) {
                ad.gotoMessageViewController()
            }
            guard let presentingViewController = ad.getTopViewController() else { return }
            show(shout: announcement, to: presentingViewController, completion: {
                print("The shout was silent.")
            })
        }
    }
    static func removeChatRoomObserver(roomName:String){
        let ref = ChatManager.rootRef.child(roomName)
        let handle: UInt = 0
        ref.removeObserverWithHandle(handle)
    }
    func removeAllRoomObserver(){
        ChatManager.rootRef.removeAllObservers()
        for room in ChatManager.queryRooms(User.getSharedInStance()!) {
            ChatManager.rootRef.child(room.path).removeAllObservers()
        }
    }
    static func getRooms(completionHandler:(success:Bool,rooms:[Room]?)->Void){
        ApiHelper.getApi(URLAPI.GET_CHAT_ROOMS.rawValue) { (success, response) in
            if success{
                guard let response = response else {
                    log.error("ERROR::api error no response")
                    return completionHandler(success: false,rooms: nil)
                }
                guard let status = response["status"].string else{
                    log.error("ERROR::api error response status false")
                    return completionHandler(success: false,rooms: nil)
                }
                if status == "success"{
                    if let roomsJson = response["rooms"].array{
                        log.info("SUCCESS")
                        let rooms = ParserHelper.parseRooms(roomsJson)
                        return completionHandler(success: true,rooms: rooms)
                    }
                }
                if let message = response["message"].string{
                    log.error("ERROR::message::\(message)")
                }
            }
            log.error("ERROR::api error in getRooms")
            return completionHandler(success: false,rooms: nil)
        }
    }
    
    
    static func fireChatManager(){
        ChatManager.rootRef.removeAllObservers()
    }
}

//MARK: CHAT Notification
extension ChatManager{
    func chatNotification(sender:String,text:String){
        log.info("sender:\(sender) text:\(text)")
    }
}

//
//  ImageHelper.swift
//  ClassMatch
//
//  Created by Mac on 9/30/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import Alamofire
import Haneke
class ImageHelper {
    static let cache = Shared.imageCache
    static func downLoadImage(path:String , completionHandler:(success:Bool,image:UIImage?)->Void){
        var url:String!
        if path.containsString("http://") || path.containsString("https://"){
            url = path
        }else{
           url = hostAddress + path
        }
        ImageHelper.cache.fetch(key: url).onSuccess { (image) in
            log.info("SUCCESS::getImageFromCache::path::\(path)")
            return completionHandler(success: true, image: image)

        }.onFailure { (error) in
            log.info("Failure::getImageFromCache::path::\(path)")
            Alamofire.request(.GET, url).responseImage(completionHandler: { (responseImage) in
                switch responseImage.result{
                case .Success(let image):
                    log.info("SUCCESS::downLoadImage::path::\(path)")
                    ImageHelper.cache.set(value: image, key: url)
                    return completionHandler(success: true, image: image)
                    
                case .Failure(let error):
                    log.error(url + error.debugDescription)
                    return completionHandler(success: false, image: nil)
                }
            })
        }
    }
    
    static func downLoadAvatarImage(path:String , completionHandler:(success:Bool,image:UIImage?)->Void){
        let formatName = "avatar"
        var url:String!
        if path.containsString("http://") || path.containsString("https://"){
            url = path
        }else{
            url = hostAddress + path
        }
        ImageHelper.cache.fetch(key: url, formatName: formatName , failure: { (error) in
            
            let iconFormat = Format<UIImage>(name: formatName, diskCapacity: 5 * 1024 * 1024) { image in
                return ImageHelper.resizeImage(image, newWidth: 90)
            }
            cache.addFormat(iconFormat)
            Alamofire.request(.GET, url).responseImage(completionHandler: { (responseImage) in
                switch responseImage.result{
                case .Success(let image):
                    log.info("SUCCESS::downLoadImage::path::\(path)")
                    ImageHelper.cache.set(value: image, key: url, formatName: formatName, success: { (image) in
                        return completionHandler(success: true, image: image)
                    })
                case .Failure(let error):
                    log.error(url + error.debugDescription)
                    return completionHandler(success: false, image: nil)
                }
            })
            
            }) { (image) in
                return completionHandler(success: true, image: image)
        }
    }
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    

}

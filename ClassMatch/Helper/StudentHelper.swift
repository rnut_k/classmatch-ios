//
//  StudentHelper.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class StudentHelper: NSObject {
    static func getAdmission(admissionId:Int,completionHandler:(success:Bool,admission:Admission?)->Void){
        let path = URLAPI.GET_ADMISSION.rawValue + String(admissionId)
        ApiHelper.getApi(path) { (success, response) in
            if success{
                guard let response = response else {return completionHandler(success: false,admission: nil) }
                guard let status = response["status"].string else {return completionHandler(success: false,admission: nil) }
                if status == "success"{
                    log.info(response)
                    guard let admissionData = response["admission"].dictionaryObject else { return completionHandler(success: false,admission: nil) }
                    let json = JSON(admissionData)
                    guard let admission = ParserHelper.parseAdmission(json) else{ return completionHandler(success: false,admission: nil) }
                    return completionHandler(success: true,admission: admission)
                }else{
                    return completionHandler(success: false,admission: nil)
                }
                    
            }
        }
    }
    static func denyAdmission(admissionId:Int,completionHandler:(success:Bool)->Void){
        let path = URLAPI.DENY_STUDYING_ADMISSION.rawValue
        let body = JSON(["admission_id":admissionId])
        
        ApiHelper.postApi(path, body: body) { (success, response) in
            if success{
                if let response =  response{
                    if let status = response["status"].string{
                        if status == "success"{
                            log.info("SUCCESS")
                            return completionHandler(success: true)
                        }else{
                            if let message = response["message"].string{
                                log.error("ERROR::message::\(message)")
                                return completionHandler(success: false)
                            }
                        }
                    }
                }
            }
            log.error("ERROR::api error in approveAdmission")
            return completionHandler(success: false)
        }
        
    }
    static func approveAdmission(admissionId:Int,completionHandler:(success:Bool)->Void){
        let path = URLAPI.APPROVE_STUDYING_ADMISSION.rawValue
        let body = JSON(["admission_id":admissionId])
        
        ApiHelper.postApi(path, body: body) { (success, response) in
            if success{
                if let response =  response{
                    if let status = response["status"].string{
                        if status == "success"{
                            log.info("SUCCESS")
                            return completionHandler(success: true)
                        }else{
                            if let message = response["message"].string{
                                log.error("ERROR::message::\(message)")
                                return completionHandler(success: false)
                            }
                        }
                    }
                }
            }
            log.error("ERROR::api error in approveAdmission")
            return completionHandler(success: false)
        }
        
    }
    static func createStudentPost(studying:Studying,completionHandler:(success:Bool)->Void){
        let json = ParserHelper.mapStudyingToJson(studying)
        ApiHelper.postApi(URLAPI.STUDENT_POST.rawValue, body: json) { (success, response) in
            if success {
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            return completionHandler(success: true)
                        }
                    }
                }
            }
            log.error("ERROR::\(URLAPI.STUDENT_POST.rawValue)::\(response)")
            return completionHandler(success: false)
        }
    }

    static func getAllPost(completionHandler:(success:Bool,posts:[Studying]?)->Void){
        let path = URLAPI.STUDENT_POST.rawValue
        ApiHelper.getApi(path) { (success, response) in
            if success{
                guard let response = response else{ return completionHandler(success: false, posts: nil) }
                guard let status = response["status"].string else{ return completionHandler(success: false, posts: nil) }
                if status == "success"{
                    guard let posts = response["posts"].array else {
                        return completionHandler(success: false, posts: nil)
                    }
                    var studyings = [Studying]()
                    for post in posts{
                        if let studying = ParserHelper.parseStudying(post){
                            studyings.append(studying)
                        }
                    }
                    return completionHandler(success:true, posts: studyings)
                    
//                    if let posts = response["posts"].array{
//                        if let studyings = ParserHelper.parseStudyings(posts){
//                            log.info("SUCCESS")
//                            return completionHandler(success: true, posts: studyings)
//                        }
//                    }
                }else{
                    if let message = response["message"].string{
                        log.error("ERROR::message::\(message)")
                        return completionHandler(success: false, posts: nil)
                    }
                }
            }
            log.error("ERROR::api error in getAllPost")
            return completionHandler(success: false, posts: nil)
        }
    }
    
    
    static func getAllTeacher(completionHandler:(success:Bool,teachers:[Teacher]?)->Void){
        ApiHelper.getApi(URLAPI.GET_TEACHER_POST.rawValue) { (success, response) in
            if success{
                let failedCallback = completionHandler(success: false, teachers: nil)
                guard let response = response else { return failedCallback }
                guard let status = response["status"].string else { return failedCallback }
                if status != "success"{
                    return failedCallback
                }
                guard let teachersJson = response["teachers"].array else {
                    return completionHandler(success: true, teachers: nil)
                }
                if let teachers = ProfileManager.parseTeachers(teachersJson){
                    return completionHandler(success: true,teachers: teachers)
                }
            }
            return completionHandler(success: false,teachers:nil)
        }
    }
    
    
    static func getSubjects(completionHandler:(success:Bool,subjects:[Subject]?)->Void){
        ApiHelper.getApi(URLAPI.GET_SUBJECTS.rawValue) { (success, response) in
            if success{
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            if let data = response["subjects"].array{
                                if let subjects = ParserHelper.parseSubjects(data){
                                    return completionHandler(success: true, subjects: subjects)
                                }
                            }
                        }
                    }
                }
            }

            return completionHandler(success: false,subjects: nil)
        }
    }
    
    class func getDegrees(completionHandler:(success:Bool,degree:[Degree]?)->Void){
        var degreeResponse = [Degree]()
        let persisantDegree = uiRealm.objects(Degree.self)
        if persisantDegree.count > 0{
            return completionHandler(success: true, degree: persisantDegree.toArray())
        }
        
        ApiHelper.getApi(URLAPI.DEGREE.rawValue) { (success, response) in
            if success{
                guard let response = response else { return completionHandler(success: false, degree: nil) }
                guard let status = response["status"].string else { return completionHandler(success: false, degree: nil) }
                if status == "success"{
                    guard let degrees = response["degree"].array else { return completionHandler(success: false, degree: nil) }
                    for degree in degrees{
                        guard let degree = ParserHelper.parseDegree(degree) else { return completionHandler(success: false, degree: nil) }
                        degreeResponse.append(degree)
                    }
                    return completionHandler(success: true, degree: degreeResponse)
                }
            }
            return completionHandler(success: false, degree: nil)
        }
    }
    
    
    static func getTags(strTag:String,tagsCompletionHandler:(success:Bool,tags:[Tag]?)->Void){
        let encodedTags = strTag.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let path = URLAPI.TAGS.rawValue + "/" + encodedTags!
        ApiHelper.getApi(path) { (success, response) in
            if success{
                guard let response = response else {return tagsCompletionHandler(success: false, tags: nil)}
                guard let status = response["status"].string else {return tagsCompletionHandler(success: false, tags: nil)}
                if status == "success"{
                    guard let tagsData = response["tags"].array else {return tagsCompletionHandler(success: false, tags: nil)}
                    var tagsArray = [Tag]()
                    for tagData in tagsData{
                        if let tag = ParserHelper.parseTag(tagData){
                            tag.save()
                            tagsArray.append(tag)
                        }
                    }
                    return tagsCompletionHandler(success: true, tags: tagsArray)
                }
                else{
                    log.error("status::\(status)::response::\(response)")
                    return tagsCompletionHandler(success: false, tags: nil)
                }
            }
            
            
        }
    }
}

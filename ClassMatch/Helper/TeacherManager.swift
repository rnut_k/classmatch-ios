//
//  TeacherManager.swift
//  ClassMatch
//
//  Created by Mac on 10/1/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import SwiftyJSON
class TeacherManager: NSObject {
    
    static func getAdmissionExpireDate(admissionId:Int,completionHandler:(success:Bool,isExpire:Bool?,expireDate:String?)->Void){
        let path = URLAPI.GET_ADMISSION_EXPIRE.rawValue + String(admissionId)
        ApiHelper.getApi(path) { (success, response) in
            if success{
                guard let response = response else {
                    log.error("response nil")
                    return completionHandler(success:false,isExpire: nil,expireDate: nil)
                }
                guard let status = response["status"].string else {
                    log.error("status not found")
                    return completionHandler(success:false,isExpire: nil,expireDate: nil) }
                if status != "success" {
                    log.error("status ==  false")
                    return completionHandler(success:false,isExpire: nil,expireDate: nil)
                }
                guard let isExpire = response["isExpire"].string else {
                    log.error("isExpire nil")
                    return completionHandler(success:false,isExpire: nil,expireDate: nil)
                }
                guard let date = response["end"]["date"].string else
                {
                    log.error("date nil")
                    return completionHandler(success:false,isExpire: nil,expireDate: nil)
                }
                let dateformatter = NSDateFormatter()
                dateformatter.dateFormat =  "yyyy-MM-dd HH:mm:ss.SSSSSS"
                let dateObj = dateformatter.dateFromString(date)
                dateformatter.dateFormat = "HH.mm dd MMM yyyy"
                let strDate = dateformatter.stringFromDate(dateObj!)
                return completionHandler(success:true,isExpire: isExpire == "true" ? true : false ,expireDate: strDate )
            }
            return completionHandler(success:false,isExpire: nil,expireDate: nil)
        }
        
    }
    static func uploadImageTransaction(image:UIImage,admissionId:String,completionHandler:(success:Bool)->Void){
        let url = URLAPI.CREATE_TRANSACTION_ADMISSION.rawValue + admissionId
        ApiHelper.uploadImageApi(url, image: image) { (success, response) in
            if success {
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            return completionHandler(success: true)
                        }
                        else{
                            log.error("status::false")
                        }
                    }
                }
            }
            return completionHandler(success: false)
        }
    }
    static func createAdmissionTransaction(admissionId:String,image:UIImage,completionHandler:(success:Bool)->Void){
        let url = URLAPI.CREATE_TRANSACTION_ADMISSION.rawValue + admissionId
        ApiHelper.uploadImageApi(url, image: image) { (success, response) in
            if success {
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            return completionHandler(success: true)
                        }
                        else{
                            log.error("status::false::response::\(response)")
                            return completionHandler(success: false)
                        }
                    }
                }
            }
            return completionHandler(success: false)
        }
    }
    static func getAdmissionQueue(studying:Studying,completionHandler:(success:Bool,message:String?,queueCount:Int)->Void){
        let path = URLAPI.GET_ADMISSION_QUEUE.rawValue + "/" + String(studying.id)
        ApiHelper.getApi(path) { (success, response) in
            if success{
                if let response =  response{
                    if let status = response["status"].string{
                        if status == "success"{
                            if let queCount = response["queue"].int{
                                log.info("SUCCESS")
                                return completionHandler(success: true,message: nil, queueCount: queCount)
                            }
                        }
                    }
                    if let message = response["message"].string{
                        log.error("ERROR::message::\(message)")
                        return completionHandler(success: false,message: message, queueCount: 0)
                    }
                }
            }
            log.error("ERROR::api error in getAdmissionQueue")
            return completionHandler(success: false,message: nil, queueCount: 0)
        }
    }
    
    static func createAdmission(studying:Studying,completionHandler:(success:Bool,isActive:Bool,message:String?)->Void){
        let path = URLAPI.CREATE_ADMISSION.rawValue
        let body = JSON(["studying_id":studying.id])
        
        ApiHelper.postApi(path, body: body) { (success, response) in
            if success {
                if let response =  response{
                    if let status = response["status"].string{
                        if status == "success"{
                            log.info("SUCCESS")
                            if let isActive = response["isActive"].bool{
                                return completionHandler(success: true, isActive: isActive,message: nil)
                            }
                        }
                    }
                    if let message = response["message"].string{
                        log.error("ERROR::message::\(message)")
                        if message == MESSAGE_API.REDUNDANT_ADMISSION.rawValue{
                            return completionHandler(success: false, isActive: false,message: "ไม่อนุญาติให้รับสอนซ้ำค่ะ ระบบจะแจ้งเตือนคุณอีกครั้งเมื่อถึงคิว")
                        }
                        return completionHandler(success: false, isActive: false,message: message)
                    }
                }
            }
            log.error("ERROR::api error in createAdmission")
            return completionHandler(success: false, isActive: false,message: nil)
        }
    }
}

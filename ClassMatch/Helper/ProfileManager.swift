//
//  ProfileManager.swift
//  ClassMatch
//
//  Created by Mac on 8/28/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import RealmSwift
class ProfileManager {
    
    
    static func getBasicInformation(){
        let url = hostAddress + URLAPI.GET_BASICINFO.rawValue
        ApiHelper.get(url, body: nil) { (success, response) in
            if success{
                guard let response = response else { return }
                guard let status = response["status"].string else { return }
                if status != "success"{
                    return
                }
                guard let subjects = response["subjects"].array else { return }
                for json in subjects{
                    if let subject = ParserHelper.parseSubject(json){
                        subject.save()
                    }
                }
                guard let days = response["days"].array else { return }
                for json in days{
                    if let day = ParserHelper.parseDay(json){
                        day.save()
                    }
                }
                guard let degrees = response["degrees"].array else { return }
                for json in degrees{
                    if let degree = ParserHelper.parseDegree(json){
                        degree.save()
                    }
                }
            }
        }
    }
    
    static func getNotifications(notificationsCompletionHandler:(success:Bool,admissions:List<Admission>?,studyings:List<Studying>?,teachings:List<Teaching>?)->Void){
        ApiHelper.getApi(URLAPI.GET_NOTIFICATIONS.rawValue) { (success, response) in
            if success{
                guard let response = response else { return notificationsCompletionHandler(success:false,admissions:nil,studyings:nil,teachings:nil) }
                guard let status = response["status"].string else { return notificationsCompletionHandler(success:false,admissions:nil,studyings:nil,teachings:nil) }
                var studyings:List<Studying>?
                var admissions:List<Admission>?
                var teachings:List<Teaching>?
                if status != "success"{
                    return notificationsCompletionHandler(success:false,admissions:nil,studyings:nil,teachings:nil)
                }
                if let studyingsJson = response["notifications"]["studyings"].array{
                    studyings = List<Studying>()
                    for json in studyingsJson{
                        if let studying = ParserHelper.parseStudying(json){
                            studyings!.append(studying)
                        }
                    }
                }
                if let admissionsJson = response["notifications"]["teacher"]["admissions"].array{
                    admissions = List<Admission>()
                    for json in admissionsJson{
                        if let admission = ParserHelper.parseAdmission(json){
                            admissions!.append(admission)
                        }
                    }
                }
                //TODO:
//                if let teachingsJson = response["notifications"]["teachings"].array{
//                    
//                }
                return notificationsCompletionHandler(success: true, admissions: admissions, studyings: studyings, teachings: teachings)
            }
        }
    }
        //MARK: REQUEST
    static func checkIsTeacher(completionHandler:(success:Bool,isTeacher:Bool)->Void){
        ApiHelper.getApi(URLAPI.CHECK_IS_TEACHER.rawValue) { (success, response) in
            if success{
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            if let isTeacher = response["isTeacher"].bool{
                                if let teacherData = response["teacher"].dictionaryObject{
                                    if let teacher = parseTeacherData(JSON(teacherData)){
                                        teacher.save()
                                        User.getSharedInStance()!.updateTeacher(teacher)
                                        log.info("SUCCESS::TEACHER::\(teacher)")
                                        return completionHandler(success: true,isTeacher: isTeacher)
                                    }
                                }
                                return completionHandler(success: true,isTeacher: isTeacher)
                            }
                        }
                    }
                }
            }
            return completionHandler(success: false,isTeacher: false)
        }
        
    }
    static func registerTeacher(teacher:Teacher,completionHandler:(success:Bool,teacher:Teacher?)->Void){
        let dict = teacher.toDictionary()
        let body = JSON(dict)
        ApiHelper.postApi(URLAPI.REGISTER_TEACHER.rawValue, body: body) { (success, response) in
            if success {
                guard let response = response else {return completionHandler(success: false, teacher: nil)}
                guard let status = response["status"].string else {return completionHandler(success: false, teacher: nil)}
                if status != "success" {
                    log.error("response::\(response)")
                    return completionHandler(success: false, teacher: nil)
                }
                guard let teacherData = response["teacher"].dictionaryObject else { return completionHandler(success: false, teacher: nil)}
                guard let teacher = ParserHelper.parseTeacherProfile(JSON(teacherData)) else { return completionHandler(success: false, teacher: nil)}
                return completionHandler(success: true, teacher: teacher)
            }
            
            return completionHandler(success: false,teacher: nil)
        }
    }
    

    static func parseTeachers(json:[JSON])->[Teacher]?{
        var teachers = [Teacher]()
        for teacher in json{
            if let model = parseTeacherData(teacher){
                
                if let subjectsData = teacher["subjects"].array{
                    if let subjects = ParserHelper.parseSubjects(subjectsData){
                        model.subjects.removeAll()
                        for subject in subjects {
                            model.subjects.append(subject)
                        }
                    }
                }
//                model.save()
                teachers.append(model)
            }
            
        }
        if teachers.count > 0{
            return teachers
        }
        return nil
    }
    static func parseTeacherData(json:JSON)->Teacher?{
        let teacher = Teacher()
        guard let id = json["id"].int else { return nil }
        teacher.id = id
        if let certified = json["certified"].int{
            teacher.certified = certified
        }
        if let subjects = json["subjects"].array{
            teacher.subjects.removeAll()
            for subject in subjects{
                if let subjectModel = ParserHelper.parseSubject(subject){
                    teacher.subjects.append(subjectModel)
                }
            }
        }
        if let userData = json["user"].dictionaryObject {
            if let modelUser = ParserHelper.parseUser(JSON(userData)){
                teacher.user = modelUser
            }
        }
        guard let displayable = json["displayable"].dictionaryObject else { return  nil}
        guard let displayableType = json["displayable_type"].string else { return  nil}
        guard let displayableId = json["displayable_id"].int else { return nil }
        teacher.displayableType = displayableType
        
        
        
        switch displayableType {
        case Displayable.Working.rawValue:
            if let working = uiRealm.objects(Working).filter("id = \(displayableId)").first{
                teacher.working = working
            }else{
                let working = ProfileManager.parserWorking(JSON(displayable))
                teacher.working = working
            }
            break
        case Displayable.Education.rawValue:
            if let education = uiRealm.objects(Education).filter("id = \(displayableId)").first{
                teacher.education = education
            }else{
                let education = ProfileManager.parserEducation(JSON(displayable))
                teacher.education = education
            }
            break
        default:
            break
        }
        
//        if let teacherPersistant = uiRealm.objectForPrimaryKey(Teacher.self, key: id){
//            teacherPersistant.updateTeacher(teacher)
//            return teacherPersistant
//        }
        return teacher
    }
    
    static func getProfile(completionHandler:(success:Bool)->Void){
        ApiHelper.getApi(URLAPI.GET_PROFILE.rawValue) { (success, response) in
            if success{
                if let response = response{
                    guard let status = response["status"].string else { return completionHandler(success: false) }
                    if status == "success"{
                        if let profile = response["profile"].dictionary{
                            guard let email = profile["email"]?.string else {
                                log.error("cannot get email from api")
                                return completionHandler(success: false)
                            }
                            User.getSharedInStance()?.updateEmail(email)
                            if let name = profile["name"]?.string{
                                User.getSharedInStance()!.updateName(name);
                            }
                            
                            if let educations = profile["educations"]?.array{
                                ProfileManager.parserEducations(educations)
                            }
                            if let workings = profile["workings"]?.array{
                                ProfileManager.parserWorkings(workings)
                            }
                            if let tags = profile["tags"]?.array{
                                if let tagsList = ParserHelper.parseTags(tags){
                                    User.getSharedInStance()!.replaceTags(tagsList)
                                }
                            }
                            if let images = profile["images"]?.array{
                                ProfileManager.parseImages(images)
                            }
                            if let testings = profile["testings"]?.array{
                                if let testingsList = ParserHelper.parseTestings(testings){
                                    User.getSharedInStance()!.replaceTestings(testingsList)
                                }
                            }
                            if let teacherData = profile["teacher"]?.dictionaryObject{
                                if let teacher = ParserHelper.parseTeacherProfile(JSON(teacherData)){
                                    User.getSharedInStance()!.updateTeacher(teacher)
                                    teacher.save()
                                }
                                
                            }
                            return completionHandler(success: true)
                        }
                    }
                }
            }
            return completionHandler(success: false)
        }
    }
    static func parseImages(json:[JSON]){
        for image in json {
            if let id = image["id"].int{
                let model = Image()
                model.id = id
                if let path = image["path"].string{
                    model.path = path
                    log.info("path : \(path)")
                }
                if let name = image["name"].string{
                    model.name = name
                }
                model.save()
                do{
                    try uiRealm.write({
                        User.getSharedInStance()!.image = model
                        uiRealm.add(User.getSharedInStance()!, update: true)
                    })
                }
                catch(let error){
                    log.error("reaml::\(error)")
                }
            }
        }
    }
    //MARK TAGS
    static func createTags(tags:List<Tag>,completionHandler:(success:Bool)->Void){
        var array = [JSON]()
        for tag in tags {
            array.append(JSON(["name":tag.name]))
        }
        let arrayObject = JSON(array)
        let body = JSON(["tags":arrayObject])
        
        log.info("createTags::\(body)")
        ApiHelper.postApi(URLAPI.CREATE_TAGS.rawValue, body: body) { (success, response) in
            if success{
                guard let response = response else { return completionHandler(success: false) }
                guard let status = response["status"].string else { return completionHandler(success: false) }
                if status == "success" {
                    return completionHandler(success: true)
                }
            }
            
            return completionHandler(success: false)
        }
    }
    //MARK TESTING
    static func createTesting(testing:Testing,completionHandler:(success:Bool)->Void){
        var body = ["name":testing.name]
        
        if let result = testing.result{
            body["result"] = result
        }
        if let date = testing.dating{
            body["date"] = date.toString()
        }
        if let message = testing.message{
            body["message"] = message
        }
        
        ApiHelper.postApi(URLAPI.CREATE_TESTING.rawValue, body: JSON(body)) { (success, response) in
            if success{
                guard let response = response else { return completionHandler(success: false) }
                guard let status = response["status"].string else { return completionHandler(success: false) }
                if status == "success" {
                    return completionHandler(success: true)
                }
            }
            return completionHandler(success: false)
        }
    }
    
    //MARK: LEVELS
    static func getLevels(completionHandler:(success:Bool,levels:[Level]?)->Void){
        if let levels = Level.getAll(){
            if levels.count > 0 {
                return completionHandler(success: true,levels: levels)
            }
        }
        var levels = [Level]()
        ApiHelper.getApi(URLAPI.GET_LEVELS.rawValue) { (success, response) in
            if success{
                guard let json = response?["levels"].array else { return completionHandler(success: true,levels: nil) }
                for data in json{
                    if let level = ParserHelper.parseLevel(data){
                        level.save()
                        levels.append(level)
                    }
                }
                return completionHandler(success: true,levels: levels)
            }
            return completionHandler(success: false,levels: nil)
        }
    }
    
    //MARK: WORKINGS
    static func createWorking(working:Working,completionHandler:(success:Bool,working:Working?)->Void){
        let dict = working.toDictionary()
        let body = JSON(dict)
        ApiHelper.postApi(URLAPI.CREATE_WORKING.rawValue, body: body) { (success, response) in
            if success {
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            let working = response["working"]
                            let parseWorking = parserWorking(working)
                            return completionHandler(success: true,working: parseWorking)
                        }
                        log.info("status : \(status)")
                    }
                }
            }
            return completionHandler(success: false,working: nil)
        }
    }
    static func parserWorkings(json:[JSON]){
        
        do{
            try uiRealm.write({
                let user = User.getSharedInStance()
                user!.workings.removeAll()
            })
        }
        catch(let error){
            log.error("reaml::\(error)")
        }
        
        for working in json{
            let model = parserWorking(working)
            model.save()
            do{
                try uiRealm.write({
                    let user = User.getSharedInStance()
                    user!.workings.append(model)
                    uiRealm.add(user!, update: true)
                })
            }
            catch(let error){
                log.error("reaml::\(error)")
            }
        }
    }
    static func parserWorking(working:JSON)->Working{
        let model = Working()
        if let id = working["id"].int{
            model.id = id
        }
        if let name = working["name"].string{
            model.name = name
        }
        if let detail = working["detail"].string{
            model.detail = detail
        }
        if let period = working["period"].string{
            model.period = period
        }
        if let position = working["position"].string{
            model.position = position
        }
        log.info("SUCCESS::PARSER::working::\(model.id)")
        return model
    }

    
    //MARK: EDUCATIONS
    static func createEducation(education:Education,completionHandler:(success:Bool,education:Education?)->Void){
        let dict = education.toDictionary()
        
        let body = JSON(dict)
        ApiHelper.postApi(URLAPI.CREATE_EDUCATION.rawValue, body: body) { (success, response) in
            if success {
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            log.info("status : \(status)")
                            let education = response["education"]
                            let parseEducation = parserEducation(education)
                            return completionHandler(success: true,education: parseEducation)
                            
                        }else{
                            if let error =  response["message"].string{
                                log.error(error)
                            }
                        }
                        
                    }
                }
            }
            log.error("no response object")
            return completionHandler(success: false,education: nil)
        }
    }
    static func parserEducations(json:[JSON]){
        do{
            try uiRealm.write({
                let user = User.getSharedInStance()
                user!.educations.removeAll()
            })
        }
        catch(let error){
            log.error("reaml::\(error)")
        }
        
        for education in json{
            let model = parserEducation(education)
            model.save()
            do{
                try uiRealm.write({
                    let user = User.getSharedInStance()
                    user!.educations.append(model)
                    uiRealm.add(user!, update: true)
                })
            }
            catch(let error){
                log.error("reaml::\(error)")
            }
            
        }
        log.info(json)
    }
    static func parserEducation(education:JSON)->Education{
        let model = Education()
        if let id = education["id"].int{
            model.id = id
        }
        if let institude = education["name"].string{
            model.name = institude
        }
        if let faculty = education["detail"].string , subFaculty = education["subDetail"].string{
            model.detail = faculty
            model.subDetail = subFaculty
        }
        if let period = education["period"].string{
            model.period = period
        }
        if let grade = education["grade"].string{
            model.grade = grade
        }
        if let certified = education["certified"].int{
            model.certified = certified
        }
        log.info("SUCCESS::PARSER::education::\(model.id)")
        return model
        
    }
    
    //MARK: UPDATE IMAGE PROFILE
    static func updateImageProfile(image:UIImage,completionHandler:(success:Bool)->Void){
        ApiHelper.uploadImageApi(URLAPI.UPDATE_IMAGE_PROFILE.rawValue, image: image) { (success, response) in
            if success {
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            return completionHandler(success: true)
                        }
                        else{
                            log.error("status::false")
                        }
                    }
                }
            }
            return completionHandler(success: false)
        }
    }
    
    //MARK: UPLOAD Evidence
    static func uploadEvidence(type:EvidenceType,evidence_id:Int,image:UIImage,completionHandler:(success:Bool)->Void){
        var params:[String:String] = [String:String]()
        let strId = String(evidence_id)
        var url:String!
        if type == .edu{
            url = URLAPI.UPLOAD_EVIDENCE_EDU.rawValue
            params["education_id"] = strId
        }else{
            url = URLAPI.UPLOAD_EVIDENCE_TEST.rawValue
            params["testing_id"] = strId
        }
        ApiHelper.uploadImageApiWith(params, path: url, image: image) { (success, response) in
            if success {
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            return completionHandler(success: true)
                        }
                        else{
                            log.error("status::false")
                        }
                    }
                }
            }
            return completionHandler(success: false)
        }
    }
}

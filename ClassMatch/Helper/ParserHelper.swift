//
//  ParserHelper.swift
//  ClassMatch
//
//  Created by Mac on 10/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON
import RealmSwift
import GooglePlaces
class ParserHelper: NSObject {

    static func parseTeacherProfile(json:JSON)->Teacher?{
        guard let id = json["id"].int else { return nil }
        guard let certified = json["certified"].int else { return nil }
        guard let subjects = json["subjects"].array else { return nil }
        guard let displayableId = json["displayable_id"].int else { return nil }
        guard let displayableType = json["displayable_type"].string else { return  nil}
        guard let displayable = json["displayable"].dictionaryObject else { return  nil}
        
        let teacher = Teacher()
        teacher.id = id
        teacher.certified = certified
        teacher.subjects.removeAll()
        for subject in subjects{
            if let subjectModel = ParserHelper.parseSubject(subject){
                teacher.subjects.append(subjectModel)
            }
        }
        teacher.displayableType = displayableType
        switch displayableType {
        case Displayable.Working.rawValue:
            if let working = uiRealm.objects(Working).filter("id = \(displayableId)").first{
                teacher.working = working
            }else{
                let working = ProfileManager.parserWorking(JSON(displayable))
                teacher.working = working
            }
            break
        case Displayable.Education.rawValue:
            if let education = uiRealm.objects(Education).filter("id = \(displayableId)").first{
                teacher.education = education
            }else{
                let education = ProfileManager.parserEducation(JSON(displayable))
                teacher.education = education
            }
            break
        default:
            break
        }
        if let teacherPersistant = uiRealm.objectForPrimaryKey(Teacher.self, key: id){
            teacherPersistant.updateTeacher(teacher)
            return teacherPersistant
        }
        return teacher
    }
    static func parseAdmission(json:JSON)->Admission?{
        guard let id = json["id"].int else { return nil }
        guard let approved = json["approved"].int else { return nil }
        let status:AdmissionStatus = approved == 0 ? .Waiting : approved == 1 ? .Active : approved == 2 ? .Approved : .Canceled
        let admission = Admission(value: ["id":id,"approved":approved])
        admission.status = status
        if let studyingData = json["studying"].dictionaryObject {
            if let studying = ParserHelper.parseStudying(JSON(studyingData)) {
                admission.studying = studying
            }
        }
        //for get  admission specific by id Api will return images profile of Users
        if let imagesJson = json["teacher"]["user"]["images"].array{
            if let firstJson = imagesJson.first{
                if let image = ParserHelper.parseImage(firstJson){
                    if let path = image.path{
                        admission.imagePath = path
                    }
                }
            }
        }
        
        return admission
    }
    static func parseTestings(array:[JSON])->List<Testing>?{
        let testings = List<Testing>()
        for json in array {
            if let testing = ParserHelper.parseTesting(json){
                testing.save()
                testings.append(testing)
            }
        }
        if testings.count > 0 {
            return testings
        }
        return nil
    }
    static func parseTesting(json:JSON)->Testing?{
        guard let id = json["id"].int else { return nil }
        guard let name = json["name"].string else { return nil }
        
        
        if let testing = uiRealm.objectForPrimaryKey(Testing.self, key: id){
            return testing
        }
        
        let testing = Testing()
        testing.id = id
        testing.name = name
        
        if let result = json["result"].string {
            testing.result = result
        }
        if let date = json["date"].string {
            if let date = NSDate.parseDate(date){
                testing.dating = date
            }
        }
        if let message = json["message"].string {
            testing.message = message
        }
        if let certified = json["certified"].int {
            testing.certified = certified
        }
        
        return testing
    }
    static func mapStudyingToJson(object:Studying)->JSON?{
        
        guard let subject = object.subject else{ return nil }
        guard let period = object.period else{ return nil }
        guard let age = object.age else{ return nil }
        guard let degree = object.degree else{ return nil }
        guard let date = object.date else{ return nil }
        guard let message = object.message else{ return nil }
        guard let location = object.location else { return nil }
        
        //dating
        let dating = date.toString()
        //parse days
        var parsedDays = [Int]()
        for day in object.days {
            parsedDays.append(day.id)
        }
        //parse locations
        let locations = [
            "name":location.name,
            "detail" : location.detail,
            "lat" : location.lat,
            "lon" : location.lon
            ]
        //parse tags
        var tags = [String]()
        for tag in object.tags {
            tags.append(tag.name)
        }
        
        //map json
        //'number','subject_id','days','locations','period','price','age','level_id','tags','date','message'
        let json = JSON(
            [
                "number": object.number,
                "subject_id" : subject.id,
                "days" : parsedDays,
                "period" : period,
                "price" : object.price,
                "age" : age,
                "degree_id" : degree.id,
                "tags" : tags,
                "date" : dating,
                "message" : message,
                "locations" : locations,
            ])
        
        
        log.info("mapStudyingToJson::\(json)")
        return json
        
    }
    static func parseDay(day:JSON)->Day?{
        guard let id = day["id"].int else { return nil }
        if let persistantDay = uiRealm.objectForPrimaryKey(Day.self, key: id){
            return persistantDay
        }
        
        guard let name = day["name"].string else { return nil }
        let day = Day(value: ["id":id , "name": name])
        return day
    }
    static func parsePlace(place:GMSPlace,save:Bool)->Location{
        let location = Location()
        print("Place name: ", place.name)
        print("Place address: ", place.formattedAddress)
        print("Place attributions: ", place.attributions)
        print("Place lat: ", place.coordinate.latitude)
        print("Place lon: ", place.coordinate.longitude)
        
        location.name = place.name
        location.lat = String(place.coordinate.latitude)
        location.lon = String(place.coordinate.longitude)
        if let address = place.formattedAddress{
            location.detail = address
        }
        if save{
            location.save()
        }
        return location
    }
    static func parseLocation(json:JSON)->Location?{
        guard let id = json["id"].int else { return nil }
        guard let name = json["name"].string else { return nil }
        guard let lat = json["lat"].string else { return nil }
        guard let lon = json["lon"].string else { return nil }
        let location = Location(value: ["id":id,"name":name,"lat":lat,"lon":lon])
        if let detail = json["detail"].string  {
            location.detail = detail
        }
        return location
    }
    static func parseDating(json:JSON)->NSDate?{
        
        guard let date = json["date"].string else { return nil }
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateObj = dateFormatter.dateFromString(date)
        
        return dateObj
    }
    static func parseStudying(json:JSON)->Studying?{
        guard let id = json["id"].int else {
            return nil
        }
//        guard let user = json["user"].dictionary else { return nil}
        guard let subject = json["subject"].dictionary else {
            return nil
        }
        guard let days = json["days"].array else {
            return nil
        }
        guard let locations = json["locations"].array else {
            return nil
        }
        guard let degree = json["degree"].dictionary else { return nil}
        guard let tags = json["tags"].array else { return nil}
        guard let dating = json["dating"].dictionary else { return nil}
        
        guard let number = json["number"].int else { return nil}
        guard let period = json["period"].string else { return nil}
        guard let price = json["price"].string else { return nil}
        guard let age = json["age"].string else { return nil}
        guard let message = json["message"].string else { return nil}
        guard let createdAt = json["created_at"].string else { return nil }
        guard let approved = json["approved"].int else { return nil }

        guard let objSubject = ParserHelper.parseSubject(JSON(subject)) else{
            return nil
        }
        let studying = Studying()
        studying.id = id
        studying.created_at = createdAt
        studying.subject = objSubject
        for day in days {
            if let objectDay = ParserHelper.parseDay(day){
                studying.days.append(objectDay)
            }
        }
        
        for location in locations {
            if let objectLocation = ParserHelper.parseLocation(location){
                studying.location = objectLocation
            }
            
        }
        
        if let degree = ParserHelper.parseDegree(JSON(degree)){
            studying.degree = degree
        }
        
        for tag in tags {
            if let objectTag = ParserHelper.parseTag(tag){
                studying.tags.append(objectTag)
            }
        }
        
        if let date = ParserHelper.parseDating(JSON(dating)){
            studying.date = date
        }
        
        studying.number = number
        studying.period = period
        if let parsedPrice = Int(price){
            studying.price = parsedPrice
        }
        
        studying.age = age
        studying.message = message
        studying.approved = approved
        if let admissionJson = json["admissions"].array{
            for json in admissionJson{
                if let admission = ParserHelper.parseAdmission(json){
                    studying.admissions.append(admission)
                }
            }
        }
        
        return studying
        

        
    }
    static func parseTags(json:[JSON])->List<Tag>?{
        let tags = List<Tag>()
        for tagJson in json {
            if let tag = ParserHelper.parseTag(tagJson){
                tags.append(tag)
            }
        }
        if (tags.count > 0){
            return tags
        }
        return nil
        
        
        
    }
    static func parseTag(json:JSON)->Tag?{
        guard let id = json["id"].int else { return nil }
        guard let name = json["name"].string else { return nil }
        let tag = Tag(value: ["id":id,"name":name])
        return tag
        
    }
    static func parseRooms(data:[JSON])->[Room]{
        var rooms = [Room]()
        for roomJson in data {
            if let room = ParserHelper.parseRoomWithJson(roomJson){
                room.save()
                rooms.append(room)
            }
            
        }
        return rooms
    }
    static func parseRoomWithJson(data:JSON)->Room? {
        guard let roomId = data["id"].int else { return nil }
        guard let path = data["path"].string else { return nil }
        guard let sender = data["sender"].dictionaryObject else { return nil }
        guard let receiver = data["receiver"].dictionaryObject else { return nil }
//        log.info("roomId:\(roomId)")
//        log.info("path:\(path)")
//        log.info("sender:\(sender)")
//        log.info("receiver:\(receiver)")
        
        let strRoomId = String(roomId)
        let room = Room()
        room.id = strRoomId
        room.path = path
        //TODO: PARSE USER DATA
        if let sender = ParserHelper.parseStudent(JSON(sender)){
            sender.save()
            room.sender = sender
        }
        if let receiver = ParserHelper.parseStudent(JSON(receiver)){
            receiver.save()
            room.receiver = receiver
        }
        return room
    }
    static func parseStudent(data:JSON)->User?{
        
        guard let userId = data["id"].int else { return nil }
        guard let name = data["name"].string else { return nil }
        guard let email = data["email"].string else{
            log.error("Can't get email")
            return nil
        }
        if userId == User.getSharedInStance()!.id{
            return User.getSharedInStance()!
        }
        
        let student = User()
        student.id = userId
        student.name = name
        student.email = email
        
        if let images = data["images"].array{
            if(images.count >= 1){
                if let image = parseImages(images[0]){
                    image.save()
                    student.image = image
                }
            }
        }
        return student
    }
    static func parseUser(data:JSON)->User?{
        guard let userId = data["id"].int else { return nil }
        guard let name = data["name"].string else { return nil }
        
        if let user = uiRealm.objectForPrimaryKey(User.self, key: userId){
            return user
        }else{
            let student = User()
            student.id = userId
            student.name = name
            if let email = data["email"].string {
                student.email = email
            }
            
            if let images = data["images"].array{
                if(images.count >= 1){
                    if let image = parseImages(images[0]){
                        image.save()
                        student.image = image
                    }
                }
            }
            student.save()
            return student
        }
    }
    static func parseImages(data:JSON)->Image?{
        let image = Image()
        guard let imageId = data["id"].int else { return nil }
        guard let path = data["path"].string else { return nil }
        image.id = imageId
        image.path = path
        
        if let name = data["name"].string{
            image.name = name
        }
        return image
    }
    
    
    static func parseSubjects(items:[JSON])->[Subject]?{
        var subjects = [Subject]()
        for item in items {
            if let subject = parseSubject(item){
                subject.save()
                subjects.append(subject)
            }
        }
        if subjects.count > 0{
            return subjects
        }
        return nil
    }
    static func parseSubject(item:JSON)->Subject?{
        
        guard let id = item["id"].int else{ return nil }
        if let subject = uiRealm.objectForPrimaryKey(Subject.self, key: id){
            return subject
        }
        guard let name = item["name"].string else { return nil }
        let subject = Subject()
        subject.id = id
        subject.name = name
        if let image = item["image"].dictionaryObject{
            if let image =  parseImage(JSON(image)){
                image.save()
                subject.image = image
            }
        }
        return subject
    }
    
    static func parseImage(item:JSON)->Image?{
        if let id = item["id"].int{
            let image = Image()
            image.id = id
            
            if let path = item["path"].string{
                image.path = path
            }
            
            if let name = item["name"].string{
                image.name = name
            }
            
            return image
        }
        return nil
    }
    
    
    
    class func parseDegree(item:JSON)->Degree?{
        let degree = Degree()
        guard let level_id = item["level_id"].int else {return nil }
        guard let name = item["name"].string else {return nil }
        degree.id = level_id
        degree.name = name
        return degree
    }
    
    class func parseLevel(item:JSON)->Level?{
        let level = Level()
        guard let id = item["id"].int else { return nil }
        guard let name = item["name"].string else { return nil }
        level.id = id
        level.name = name
        return level
    }
}

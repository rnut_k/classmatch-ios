//
//  AuthenticationManager.swift
//  ClassMatch
//
//  Created by Mac on 8/21/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseMessaging
import FirebaseDatabase
import FirebaseInstanceID
protocol AuthicationDelegate {
    func didSuccessAuthentication()
    func didFailedAuthentication()
    func didNotAuthentication()
}
class AuthenticationManager: NSObject {
    static let sharedInstance = AuthenticationManager()
    var delegate:AuthicationDelegate!
    override init() {
        super.init()
    }
    
    func start(){
        guard let _ = AuthenticationManager.checkHasToken() else {
            return self.delegate.didNotAuthentication()
        }
        AuthenticationManager.refreshToken { (success) in
            if success{
                return self.delegate.didSuccessAuthentication()
            }else{
                return self.delegate.didFailedAuthentication()
            }
        }
    }
    static func checkIsFirstCome()->Bool{
        let preferences = NSUserDefaults.standardUserDefaults()
        let Key = "firstCome"
        if preferences.objectForKey(Key) == nil {
            preferences.setBool(false, forKey: Key)
            let didSave = preferences.synchronize()
            if didSave {
                return true
            }
        }
        return false
        
    }
    static func checkHasToken()->String?{
        guard let user = User.getSharedInStance() else {return nil}
        if let token = user.token{
            return token
        }
        return nil
    }
    static func checkHasFacebooKToken()->String?{
        if let accessToken = FBSDKAccessToken.currentAccessToken(){
            if let tokenString = accessToken.tokenString{
                return tokenString
            }
        }
        return nil
    }
    static func refreshToken(completionHandler:(success:Bool)->Void){
        var url = URLAPI.REFRESH_TOKEN.rawValue
        guard let user = User.getSharedInStance() else { return completionHandler(success: false)}
        if let apnToken = user.tokenAPN{
             url += "/" + apnToken
        }
        ApiHelper.getApi(url) { (success, response) in
            if success{
                guard let response = response else{ return completionHandler(success: false) }
                guard let token = response["token"].string else{ return completionHandler(success: false) }
                guard let user = User.getSharedInStance() else {return completionHandler(success: false)}
                user.updateToken(token)
                User.setSharedInStance(user)
                log.info("SUCCESS::REFRESHTOKEN::\(token)")
                return completionHandler(success: true)
                
                
            }
            log.error("ERROR::refreshToken")
            log.error(response?["message"].string)
            user.updateToken(nil)
            return completionHandler(success: false)
        }
    }
    static func signOut(completionHandler:(success:Bool)->Void){
        ad.offLineFirebaseSystem()
        ApiHelper.getApi(URLAPI.SIGNOUT.rawValue) { (success, response) in }
        if(FBSDKAccessToken.currentAccessToken() != nil) {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }
        User.signOut()
        try! uiRealm.write({ 
            uiRealm.deleteAll()
        })
        return completionHandler(success: true);
    }
    static func loginWithFacebook(token:String,completionHandler:(success:Bool,jwt:String?)->Void){
        let url = hostAddress + URLAPI.LOGINWITHFACEBOOK.rawValue
        let json = JSON(["token":token])
        Alamofire.request(.POST, url, parameters: json.dictionaryObject, encoding: .JSON, headers: nil)
            .responseJSON { response in
                
                switch response.result{
                case .Success(let value):
                    let json = JSON(value)
                    guard let jwt = json["jwt"].string else {return completionHandler(success: false, jwt: nil)}
                    guard let userInfo = json["user"].dictionaryObject else {return completionHandler(success: false, jwt: nil)}
                    guard let user = ParserHelper.parseUser(JSON(userInfo)) else {return completionHandler(success: false, jwt: nil)}
                    user.updateToken(jwt)
                    User.setSharedInStance(user)
                    log.info("\nAuthenticationManager::register::success::jwt::\(jwt)")
                    return completionHandler(success: true, jwt: jwt)
                case .Failure(let error):
                    log.error("response::\(response)")
                    log.error("\nApiHelper_post_error::\(url)::\(error.debugDescription)\n")
                    if(FBSDKAccessToken.currentAccessToken() != nil) {
                        let loginManager = FBSDKLoginManager()
                        loginManager.logOut()
                    }
                    return completionHandler(success: false, jwt: nil)
                    
                    
                }
        }
    }
    
    static func login(email:String,passWord:String,completionHandler:(success:Bool,jwt:String?)->Void){
        let url = hostAddress + URLAPI.LOGIN.rawValue
        let credentials = JSON(["email":email,"password":passWord])
        Alamofire.request(.POST, url, parameters: credentials.dictionaryObject, encoding: .JSON, headers: nil)
            .responseJSON { response in
                switch response.result{
                case .Success(let value):
                    let json = JSON(value)
                    guard let jwt = json["jwt"].string else {return completionHandler(success: false, jwt: nil)}
                    guard let userInfo = json["user"].dictionaryObject else {return completionHandler(success: false, jwt: nil)}
                    guard let user = ParserHelper.parseUser(JSON(userInfo)) else {return completionHandler(success: false, jwt: nil)}
                    user.updateToken(jwt)
                    User.setSharedInStance(user)
                    AuthenticationManager.updateApnToken()
                    return completionHandler(success: true, jwt: jwt)
                case .Failure(let error):
                    print("\nApiHelper_post_error : \(error.debugDescription)\n")
                    return completionHandler(success: false, jwt: nil)
                    
                }
        }
    }
    
    static func register(email:String,passWord:String,name:String,birthDate:String?,completionHandler:(success:Bool,jwt:String?)->Void) {
        
        let url = hostAddress + URLAPI.REGISTER.rawValue
        var stringBirthDate = ""
        if let birthDate = birthDate{
            stringBirthDate = birthDate
        }
        let userInfo = JSON(["email":email,"password":passWord,"name":name,"birthDate":stringBirthDate])
        Alamofire.request(.POST, url, parameters: userInfo.dictionaryObject, encoding: .JSON, headers: nil)
            .responseJSON { response in
                switch response.result{
                case .Success(let value):
                    let json = JSON(value)
                    guard let jwt = json["jwt"].string else {return completionHandler(success: false, jwt: nil)}
                    guard let userInfo = json["user"].dictionaryObject else {return completionHandler(success: false, jwt: nil)}
                    guard let user = ParserHelper.parseUser(JSON(userInfo)) else {return completionHandler(success: false, jwt: nil)}
                    user.updateToken(jwt)
                    User.setSharedInStance(user)
                    AuthenticationManager.updateApnToken()
                    log.info("\nAuthenticationManager::register::success::jwt::\(jwt)")
                    return completionHandler(success: true, jwt: jwt)
                case .Failure(let error):
                    print("\nAuthenticationManager::register::failed::\(error.debugDescription)")
                    return completionHandler(success: false, jwt: nil)
                }
        }
    }
    
    static func getUserInfo(jwt:String,completionHandler:(success:Bool,userInfo:JSON?)->Void){
        let url = hostAddress + URLAPI.GETUSERINFO.rawValue+"?token="+jwt
            Alamofire.request(.GET, url)
            .responseJSON { response in
                switch response.result{
                case .Success(let value):
                    let json = JSON(value)
                    print("\nApiHelper_post_success : \(url)\n")
                    if let responseStatus = json["status"].string{
                        if responseStatus == "success"{
                            guard let userInfo = json["user"].dictionaryObject else { return completionHandler(success: false, userInfo: nil)}
                            guard let user = ParserHelper.parseUser(JSON(userInfo)) else {return completionHandler(success: false, userInfo: nil)}
                            User.setSharedInStance(user)
                            AuthenticationManager.updateApnToken()
                            return completionHandler(success: true, userInfo: json)
                        }
                    }
                    if let error = json["error"].string{
                        print("\nApiHelper::ERROR::getUserInfo::\(error)\n")
                        return completionHandler(success: false, userInfo: nil)
                    }
                    print("\nApiHelper::ERROR::getUserInfo\n")
                    return completionHandler(success: false, userInfo: nil)
                case .Failure(let error):
                    print("\nApiHelper::ERROR::getUserInfo::\(error.debugDescription)\n")
                    return completionHandler(success: false, userInfo: nil)
                    
                }
        }
        
    }
    
    static func updateApnToken(){
        guard let user = User.getSharedInStance() else {return }
        if let apnToken = user.tokenAPN{
            let data = JSON(["notification_id":apnToken])
            ApiHelper.postApi(URLAPI.UPDATE_NOTIFICATION_ID.rawValue, body: data) { (success, response) in
                if success{
                    if let response = response{
                        if let status = response["status"].string{
                            if status == "success"{
                                log.info("SUCCESS::updateApnToken::\(apnToken)")
                            }else{
                                if let message = response["message"].string{
                                    log.error("ERROR::updateApnToken::message::\(message)")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    static func updateApnToken(apnsToken:String){
        let data = JSON(["notification_id":apnsToken])
        ApiHelper.postApi(URLAPI.UPDATE_NOTIFICATION_ID.rawValue, body: data) { (success, response) in
            if success{
                if let response = response{
                    if let status = response["status"].string{
                        if status == "success"{
                            log.info("SUCCESS::updateApnToken::\(apnsToken)")
                        }else{
                            if let message = response["message"].string{
                                log.error("ERROR::updateApnToken::message::\(message)")
                            }
                        }
                    }
                }
            }
        }
    }
}

//
//  ApiHelper.swift
//  ClassMatch
//
//  Created by Mac on 8/22/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireImage


let hostAddress = "http://classmatch.reallifefootball.com/public"
//let hostAddress = "http://localhost:8000"
//let hostAddress = "http://192.168.1.36:8000"
class ApiHelper {
    
    
    
    
    static func stop(){
        Alamofire.Manager.sharedInstance.session.invalidateAndCancel()
    }
    static func post(url:String,body:JSON?,completionHandler:(success:Bool,response:JSON?)->Void){
        
        Alamofire.request(.POST, url, parameters: body?.dictionaryObject, encoding: .JSON, headers: nil)
            .responseJSON { response in
                switch response.result{
                case .Success(let value):
                    let json = JSON(value)
                    print("\nApiHelper_post_success : \(url)\n")
                    return completionHandler(success: true, response: json)
                case .Failure(let error):
                    print("\nApiHelper_post_error::\(url)::\(error.debugDescription)\n")
                    return completionHandler(success: false, response: nil)
                    
                }
        }
    }
    static func get(url:String,body:JSON?,completionHandler:(success:Bool,response:JSON?)->Void){
        
        Alamofire.request(.GET, url, parameters: body?.dictionaryObject)
            .responseJSON { response in
                switch response.result{
                case .Success(let value):
                    let json = JSON(value)
                    print("\nApiHelper_post_success : \(url)\n")
                    completionHandler(success: true, response: json)
                case .Failure(let error):
                    print("\nApiHelper_post_error : \(error.debugDescription)\n")
                    completionHandler(success: false, response: nil)
                     
                }
        }
    }
    
    static func getApi(path:String,completionHandler:(success:Bool,response:JSON?)->Void){
        guard let user = User.getSharedInStance() else {
            return completionHandler(success: false, response: nil)
        }
        if let token = user.token{
            let url = hostAddress + path
            let json = JSON(["token":token])
            Alamofire.request(.GET, url, parameters: json.dictionaryObject)
                .responseJSON { response in
                    switch response.result{
                    case .Success(let value):
                        
                        if let statusCode = response.response?.statusCode{
                            if statusCode == 200{
                                let json = JSON(value)
                                log.info("SUCCESS::ApiHelper::getApi::\(url)")
                                return completionHandler(success: true, response: json)
                            }else if statusCode == 401{
                                log.info("ERROR::ApiHelper::getApi::\(url)::statusCode401")
                                return completionHandler(success: true, response: JSON(["message":MESSAGE_API.NOT_AUTHORIZE.rawValue]))
                            }
                        }
                        
                    case .Failure(let error):
                        log.info("ERROR::ApiHelper::getApi::\(url)::reason::\(error.debugDescription)::response::\(response)")
                        return completionHandler(success: false, response: nil)
                        
                    }
            }
        }else{
            print("\nERROR::ApiHelper::getApi::NOT_FOUND_TOKEN\n")
            return completionHandler(success: false, response: nil)
        }
        
        
    }
    
    static func postApi(path:String,body:JSON?,completionHandler:(success:Bool,response:JSON?)->Void){
        guard let user = User.getSharedInStance() else {
            return completionHandler(success: false, response: nil)
        }
        if let token = user.token{
            let url = hostAddress + path
            let headers = [
                "Authorization": "Bearer { \(token) }",
                "Content-Type": "application/json"
            ]
            Alamofire.request(.POST, url, parameters: body?.dictionaryObject, encoding: .JSON, headers: headers)
                .responseJSON { response in
                    switch response.result{
                    case .Success(let value):
                        let json = JSON(value)
                        log.info("SUCCESS::POSTAPI::\(path)")
                        return completionHandler(success: true, response: json)
                    case .Failure(let error):
                        log.error("ERROR::POSTAPI::\(path)::\(error.debugDescription)")
                        return completionHandler(success: false, response: nil)
                    }
            }
            
        }else{
            log.error("ERROR::SUCCESS:POSTAPI::\(path)::NOT_FOUND_TOKEN")
            return completionHandler(success: false, response: nil)
        }
        
    }
    static func putApi(path:String,body:JSON?,completionHandler:(success:Bool,response:JSON?)->Void){
        guard let user = User.getSharedInStance() else {
            return completionHandler(success: false, response: nil)
        }
        if let token = user.token{
            let url = hostAddress + path
            let headers = [
                "Authorization": "Bearer { \(token) }",
                "Content-Type": "application/json"
            ]
            Alamofire.request(.PUT, url, parameters: body?.dictionaryObject, encoding: .JSON, headers: headers)
                .responseJSON { response in
                    switch response.result{
                    case .Success(let value):
                        let json = JSON(value)
                        log.info("SUCCESS::PUTAPI::\(path)")
                        return completionHandler(success: true, response: json)
                    case .Failure(let error):
                        log.error("ERROR::PUTAPI::\(path)::\(error.debugDescription)")
                        return completionHandler(success: false, response: nil)
                    }
            }
            
        }else{
            log.error("ERROR:PUTAPI::\(path)::NOT_FOUND_TOKEN")
            return completionHandler(success: false, response: nil)
        }
        
    }
    
    static func uploadImageApi(path:String,image:UIImage,completionHandler:(success:Bool,response:JSON?)->Void){
        if let token = User.getSharedInStance()!.token{
            let url = hostAddress + path
            let headers = [
                "Authorization": "Bearer { \(token) }",
                ]
            let size = CGSize(width: 50.0, height: 50.0)
            let scaledImage = image.af_imageAspectScaledToFitSize(size)
            if let imageData = UIImagePNGRepresentation(scaledImage){
                Alamofire.upload(.POST, url,headers: headers, multipartFormData: { multipartFormData in
                    multipartFormData.appendBodyPart(data: imageData, name: "image",fileName: "iosFile.jpg", mimeType: "image/jpg")
                    }, encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .Success(let upload, _, _):
                            upload.responseJSON(completionHandler: { (response) in
                                switch response.result{
                                case .Success(let value):
                                    let json = JSON(value)
                                    log.info("SUCCESS::UPLOADAPI::\(path)")
                                    return completionHandler(success: true, response: json)
                                case .Failure(let error):
                                    log.error("ERROR::POSTAPI::\(path)::\(error.debugDescription)")
                                    return completionHandler(success: false, response: nil)
                                }
                            })
                        case .Failure(let encodingError):
                            log.info("ERROR::UPLOADAPI::ENCODE:\(encodingError)")
                            return completionHandler(success: false, response: nil)
                        }
                })
            }
        }else{
            log.error("ERROR::\(path)::NOT_FOUND_TOKEN")
            return completionHandler(success: false, response: nil)
        }
        
    }
    static func uploadImageApiWith(params:[String:String]!,path:String,image:UIImage,completionHandler:(success:Bool,response:JSON?)->Void){
        if let token = User.getSharedInStance()!.token{
            let url = hostAddress + path
            let headers = [
                "Authorization": "Bearer { \(token) }",
                ]
            let size = CGSize(width: 50.0, height: 50.0)
            let scaledImage = image.af_imageAspectScaledToFitSize(size)
            
            if let imageData = UIImagePNGRepresentation(scaledImage){
                Alamofire.upload(.POST, url,headers: headers, multipartFormData: { multipartFormData in
                    multipartFormData.appendBodyPart(data: imageData, name: "image",fileName: "iosFile.jpg", mimeType: "image/jpg")
                    for (key,value) in params {
                        multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                    }
                    
                    }, encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .Success(let upload, _, _):
                            upload.responseJSON(completionHandler: { (response) in
                                switch response.result{
                                case .Success(let value):
                                    let json = JSON(value)
                                    log.info("SUCCESS::UPLOADAPI::\(path)")
                                    return completionHandler(success: true, response: json)
                                case .Failure(let error):
                                    log.error("ERROR::POSTAPI::\(path)::\(error.debugDescription)")
                                    return completionHandler(success: false, response: nil)
                                }
                            })
                        case .Failure(let encodingError):
                            log.info("ERROR::UPLOADAPI::ENCODE:\(encodingError)")
                            return completionHandler(success: false, response: nil)
                        }
                })
            }
        }else{
            log.error("ERROR::\(path)::NOT_FOUND_TOKEN")
            return completionHandler(success: false, response: nil)
        }
        
    }
}
enum MESSAGE_API:String {
    case NOT_AUTHORIZE = "api_not_authorize"
    case REDUNDANT_ADMISSION = "can_not_repeat_create_admission"
}

enum MESSAGE_ERROR:String{
    case STUDYING_POST = "การโพสไม่สำเร็จ กรุณาตรวจสอบข้อมูลและลองใหม่อีกครั้งค่ะ"
    case DOWNLOAD_IMAGE = "ไม่สามารถโหลดข้อมูลรูปภาพโปรไฟล์ของผู้ใช้งานได้ กรุณาลองใหม่ หรือติดต่อผู้ดูแลระบบค่ะ"
}
enum URLAPI:String {
    case LOGINWITHFACEBOOK = "/api/v1/authenticate-facebook"
    case LOGIN = "/api/v1/authenticate"
    case REGISTER = "/api/v1/register"
    case GETUSERINFO = "/api/v1/get-my-info"
    case SIGNOUT = "/api/v1/sign-out"
    case GET_SUBJECTS = "/api/v1/subjects"
    case UPDATE_NOTIFICATION_ID = "/api/v1/student/notification-id"
    //post
    case STUDENT_POST = "/api/v1/student/post"
    case GET_TEACHER_POST = "/api/v1/student/post/teacher"
    case APPROVE_STUDYING_ADMISSION = "/api/v1/student/admission/approve"
    case DENY_STUDYING_ADMISSION = "/api/v1/student/admission/cancel"
    case GET_ADMISSION = "/api/v1/admission/"
    //profile
    case GET_PROFILE = "/api/v1/profile"
    case REFRESH_TOKEN = "/api/v1/refresh"
    case GET_LEVELS = "/api/v1/profile/levels"
    case CREATE_EDUCATION = "/api/v1/profile/educations"
    case CREATE_WORKING = "/api/v1/profile/workings"
    case CREATE_TAGS = "/api/v1/profile/create-tags"
    case CHECK_IS_TEACHER = "/api/v1/profile/is/teacher"
    case REGISTER_TEACHER = "/api/v1/student/register/teacher"
    case UPDATE_IMAGE_PROFILE = "/api/v1/profile/image"
    case CREATE_TRANSACTION_ADMISSION = "/api/v1/teacher/transaction/studying/"
    case CREATE_TESTING = "/api/v1/profile/testings"
    case GET_NOTIFICATIONS = "/api/v1/profile/notifications"
    //basic info
    case DEGREE = "/api/v1/degree"
    case TAGS = "/api/v1/tags"
    case GET_BASICINFO = "/api/v1/basicInfo"
    //teacher
    case GET_ADMISSION_QUEUE = "/api/v1/teacher/studying/admission/queue"
    case CREATE_ADMISSION = "/api/v1/teacher/admission"
    case GET_ADMISSION_EXPIRE = "/api/v1/teacher/admission/expire/"
    
    //chat
    case GET_CHAT_ROOMS = "/chat/rooms"
    
    //upload evidence
    case UPLOAD_EVIDENCE_EDU = "/api/v1/profile/image/evidence/education"
    case UPLOAD_EVIDENCE_TEST = "/api/v1/profile/image/evidence/testing"
    
    
}

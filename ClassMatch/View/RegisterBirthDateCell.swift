//
//  RegisterEmailCell.swift
//  ClassMatch
//
//  Created by Mac on 8/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class RegisterBirthDateCell: UICollectionViewCell {
    var vm:RegisterViewModel!
    @IBOutlet weak var textfieldBirthDate: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    @IBOutlet weak var buttonNext:UIButton!
    
    @IBAction func buttonNextPreesed(sender:AnyObject){
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-d"
        let date = dateformatter.stringFromDate(datePicker.date)
        print("RegisterBirthDateCell::registerDate::\(date)")
        
        vm.goNextPage(2,info: date)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textfieldBirthDate.enabled = false
        buttonNext.dimAndDisabled()
    }
    @IBAction func datePickerValueChanged(sender: AnyObject) {
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "MMMM,dd yyyy"
        dateformatter.locale = NSLocale(localeIdentifier: "th_TH")
        let date = dateformatter.stringFromDate(datePicker.date)
        self.textfieldBirthDate.text = date
        buttonNext.appearAndEnabled()
    }
}
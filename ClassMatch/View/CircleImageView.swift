//
//  CircleImageView.swift
//  ClassMatch
//
//  Created by Mac on 8/28/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

extension UIImageView{
    func setCircle(){
        layer.borderWidth = 0
        layer.masksToBounds = false
        layer.borderColor = UIColor.blackColor().CGColor
        layer.cornerRadius = self.frame.size.height/2
        clipsToBounds = true
    }
}

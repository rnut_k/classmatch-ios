//
//  StudentNumberCell.swift
//  ClassMatch
//
//  Created by Mac on 9/11/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class StudentNumberCell: UICollectionViewCell {

    
    var vm:StudentPostFormViewModel!
    @IBOutlet weak var labelMultipleUnit: UILabel!
    @IBOutlet weak var labelMultiple: UILabel!
    @IBOutlet weak var labelSingle: UILabel!
    @IBOutlet weak var textfieldNumber:UITextField!
    @IBOutlet weak var imageButtonSingle:UIImageView!
    @IBOutlet weak var imageButtonMultiple:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        textfieldNumber.keyboardType = .NumberPad
        textfieldNumber.delegate = vm
    }
    func configureCell(){
        vm.activeTextField = textfieldNumber
        if(vm.studying.number == 0){
            imageButtonSingle.image = UIImage(named: vm.imageSingleDeAction)
            imageButtonMultiple.image = UIImage(named: vm.imageMultipleDeaction)
            textfieldNumber.enabled = false
        }
        else if(vm.studying.number == 1){
            //colorize single
            imageButtonSingle.image = UIImage(named: vm.imageSingleAction)
            labelSingle.textColor = colorBase
            
            //dim multiple
            imageButtonMultiple.image = UIImage(named: vm.imageMultipleDeaction)
            textfieldNumber.text = ""
            textfieldNumber.enabled = false
            labelMultiple.textColor = UIColor.grayColor()
            labelMultipleUnit.textColor = UIColor.grayColor()
            
            
        }else{
            //dim single
            imageButtonSingle.image = UIImage(named: vm.imageSingleDeAction)
            labelSingle.textColor = UIColor.grayColor()
            
            //colorize multiple
            imageButtonMultiple.image = UIImage(named: vm.imageMultipleAction)
            textfieldNumber.enabled = true
            textfieldNumber.text = "2"
            labelMultiple.textColor = colorBase
            labelMultipleUnit.textColor = colorBase
            
            
            
        }
        
    }
    
    
}

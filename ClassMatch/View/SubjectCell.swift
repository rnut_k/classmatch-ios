//
//  SubjectCell.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
class SubjectCell: UICollectionViewCell {

    var subject:Subject!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var labelName:UILabel!
    let image = UIImage(named: "subjectIcon")
    override func awakeFromNib() {
        imageView.image = image
    }
    func configureCell(subject:Subject){
        self.subject = subject
        guard let name = self.subject.name else {return}
        labelName.text = name
        guard let image = self.subject.image else {return}
        guard let path = image.path else {return}
        ImageHelper.downLoadImage(path, completionHandler: { (success, image) in
            if success{
                guard let image = image else { return }
                self.imageView.image = image
            }
        })
    }
}

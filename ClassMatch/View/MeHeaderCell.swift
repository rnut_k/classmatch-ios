//
//  MeHeaderCell.swift
//  ClassMatch
//
//  Created by Mac on 8/27/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class MeHeaderCell: UITableViewCell {
    @IBOutlet weak var labelTitle:UILabel!
    @IBOutlet weak var buttonEdit:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


class MeTestingCell: UITableViewCell {
    var testing:Testing!
    weak var vm:MeViewModel!
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var labelResult:UILabel!
    @IBOutlet weak var labelMessage:UILabel!
    @IBOutlet weak var buttonApprove:UIButton!
    @IBAction func buttonApprovePreesed(sender:AnyObject){
        vm.performEvidence(.test, item: testing)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(){
        labelName.text = testing.name
        labelResult.text = testing.result
        labelMessage.text = testing.message
        
        if testing.certified == 1{
            buttonApprove!.setImage(UIImage(named: IMAGENAME.certified.rawValue), forState: UIControlState.Normal)
            buttonApprove!.setImage(UIImage(named: IMAGENAME.certified.rawValue), forState: UIControlState.Highlighted)
        }else{
            buttonApprove!.setImage(UIImage(named: IMAGENAME.unCertified.rawValue), forState: UIControlState.Normal)
            buttonApprove!.setImage(UIImage(named: IMAGENAME.unCertified.rawValue), forState: UIControlState.Highlighted)
        }
    }
}
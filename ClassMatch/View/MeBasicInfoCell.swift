//
//  MeBasicInfoCell.swift
//  ClassMatch
//
//  Created by Mac on 8/27/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit


class MeBasicInfoCell: UITableViewCell {
    
    weak var vm:MeViewModel!
    @IBOutlet weak var imageViewProfileImage:UIImageView!
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var labelEducation:UILabel!
    @IBOutlet weak var labelTeachingTag:UILabel!
    @IBOutlet weak var buttonTeachingPost:UIButton!

    @IBAction func buttonTeachingPostPressed(sender:AnyObject){
        print("buttonTeachingPostPressed")
        vm.delegate.didChoosePublicMenu()
    }
    
    @IBAction func buttonEditImageProfilePressed(sender:AnyObject){
        vm.delegate.didChooseImagePicker()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configureCell(){
        labelName.text = User.getSharedInStance()!.name
        labelEducation.text = TEXT.noIdentify.rawValue
        labelTeachingTag.text = TEXT.noIdentify.rawValue
        
        log.info(User.getSharedInStance()!.teacher)
        if let teacherUser = User.getSharedInStance()!.teacher {
            log.info("teacherUser : \(teacherUser)")
            labelEducation.text = teacherUser.getDisplayableInfo()
        }
        if let attrSubject = User.getSharedInStance()!.getSubjectTeacherAttributeString(){
            labelTeachingTag.attributedText = attrSubject
        }
        imageViewProfileImage.setCircle()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

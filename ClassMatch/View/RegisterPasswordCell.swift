//
//  RegisterEmailCell.swift
//  ClassMatch
//
//  Created by Mac on 8/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit


class RegisterPasswordCell: UICollectionViewCell {
    var vm:RegisterViewModel!
    @IBOutlet weak var textfieldPassword: UITextField!
    @IBOutlet weak var buttonNext:UIButton!
    
    @IBAction func buttonNextPreesed(sender:AnyObject){
        vm.goNextPage(3,info: textfieldPassword.text!)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonNext.dimAndDisabled()
        textfieldPassword.delegate = self
    }
}



extension RegisterPasswordCell:UITextFieldDelegate{
    func textFieldDidEndEditing(textField: UITextField) {
        print("textFieldDidEndEditing")
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("textField_shouldChangeCharactersInRange")
        if textField.text?.characters.count > 5{
            buttonNext.appearAndEnabled()
        }else{
            buttonNext.dimAndDisabled()
        }
        return true
    }
}
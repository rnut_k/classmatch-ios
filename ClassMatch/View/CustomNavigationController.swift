//
//  CustomNavigationController.swift
//  ClassMatch
//
//  Created by Mac on 8/27/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import UIKit
import ChameleonFramework
class CustomNavigationController: UINavigationController {
    override func viewDidLoad() {
        hidesNavigationBarHairline = true
        navigationBar.barTintColor = colorBase
        navigationBar.barStyle = UIBarStyle.Black
        navigationBar.tintColor = UIColor.whiteColor()
        
        
    }
}
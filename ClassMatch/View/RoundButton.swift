//
//  RoundButton.swift
//  ClassMatch
//
//  Created by Mac on 8/21/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import QuartzCore
@IBDesignable
class RoundButton: UIButton {
    var _layerBgColor:UIColor = UIColor.clearColor()
    @IBInspectable var layerBgColor: UIColor = UIColor.clearColor(){
        didSet{
            _layerBgColor = layerBgColor
            setUp()
        }
    }
    var _borderWidth:CGFloat = 0.1
    @IBInspectable var borderWidth:CGFloat = 0.1{
        didSet{
            _borderWidth = borderWidth
            setUp()
        }
    }
    var _borderColor:UIColor = UIColor.clearColor()
    @IBInspectable var borderColor:UIColor = UIColor.clearColor(){
        didSet{
            _borderColor = borderColor
            setUp()
        }
    }
    var _cornerRadius:CGFloat = 5.0
    @IBInspectable var cornerRadius:CGFloat = 5.0{
        didSet{
            _cornerRadius = cornerRadius
            setUp()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        setUp()
    }
    override func prepareForInterfaceBuilder() {
        setUp()
        super.prepareForInterfaceBuilder()
        
    }
    func setUp(){
        
        layer.borderWidth = _borderWidth
        layer.borderColor = _borderColor.CGColor
        layer.backgroundColor = _layerBgColor.CGColor
        layer.cornerRadius = 5.0
    }
    
    
}

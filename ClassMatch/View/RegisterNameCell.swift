//
//  RegisterEmailCell.swift
//  ClassMatch
//
//  Created by Mac on 8/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
class RegisterNameCell: UICollectionViewCell {
    var vm:RegisterViewModel!
    var stringFullName:String!
    @IBOutlet weak var textfieldName:UITextField!
    @IBOutlet weak var textfieldLastName:UITextField!
    
    @IBOutlet weak var buttonNext:UIButton!
    
    @IBAction func buttonNextPreesed(sender:AnyObject){
        vm.goNextPage(1, info: stringFullName)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonNext.dimAndDisabled()
        textfieldLastName.delegate = self
        textfieldName.delegate = self
    }
    
    func validateName(){
        var fullName = ""
        if let name = textfieldName.text where textfieldName.text != ""{
            fullName = name
        }
        if let lastName = textfieldLastName.text where textfieldLastName.text != ""{
            fullName = fullName + "  " + lastName
        }
        
        if fullName.characters.count > 2{
            buttonNext.appearAndEnabled()
            stringFullName = fullName
        }else{
            buttonNext.dimAndDisabled()
        }
    }
}



extension RegisterNameCell:UITextFieldDelegate{
    func textFieldDidEndEditing(textField: UITextField) {
        print("textFieldDidEndEditing")
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("textField_shouldChangeCharactersInRange")
        validateName()
        return true
    }
}
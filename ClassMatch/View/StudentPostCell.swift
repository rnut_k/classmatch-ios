//
//  StudentPostCell.swift
//  ClassMatch
//
//  Created by Mac on 9/11/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import Foundation
class StudentPostCell: UITableViewCell {

    var vm:StudentPostViewModel!
    private var studying:Studying!
    @IBOutlet weak var view:UIView!
    @IBOutlet weak var labelSubject:UILabel!
    @IBOutlet weak var labelCreateAt:UILabel!
    @IBOutlet weak var labelLocation:UILabel!
    @IBOutlet weak var labelDetail:UILabel!
    
    @IBOutlet weak var labelDays:UILabel!
    @IBOutlet weak var labelDating:UILabel!
    @IBOutlet weak var labelProvince:UILabel!
    @IBAction func buttonAdmissionPreesed(sender:AnyObject){
        vm.delegatePostCell.didTabAdmission(studying)
    }
    @IBAction func buttonSeeDetailPreesed(sender:AnyObject){
        vm.delegatePostCell.didTabSeeDetail(studying)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        createCardView()
    }
    private func createCardView(){
        self.contentView.backgroundColor = UIColor.clearColor()
        backgroundColor = UIColor.clearColor()
        backgroundView = nil
        selectedBackgroundView = nil
        removeSeparator(bounds.size.width)
        view.backgroundColor = UIColor.whiteColor()
        view.layer.shadowOffset = CGSizeMake(-1, 1)
        view.layer.shadowOpacity = 0.1
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configureCell(studying:Studying){
        self.studying = studying
        labelSubject.text = studying.subject?.name
        labelCreateAt.text = studying.created_at
        labelLocation.text = studying.location?.getFullDetail()
        labelDays.text = studying.getDaysTitle()
        labelDating.text = studying.date?.toString(false)
        labelDetail.text = studying.getPostDetail()
    }

}

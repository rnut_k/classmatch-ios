//
//  StudentNumberCell.swift
//  ClassMatch
//
//  Created by Mac on 9/11/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class StudentOtherCell: UICollectionViewCell {
    
    @IBOutlet weak var bulletText6:UILabel!
    @IBOutlet weak var bulletText7:UILabel!
    @IBOutlet weak var bulletText8:UILabel!
    @IBOutlet weak var bulletText9:UILabel!
    @IBOutlet weak var bulletText10:UILabel!
    @IBOutlet weak var bullet6:UIImageView!
    @IBOutlet weak var bullet7:UIImageView!
    @IBOutlet weak var bullet8:UIImageView!
    @IBOutlet weak var bullet9:UIImageView!
    @IBOutlet weak var bullet10:UIImageView!
    
    @IBOutlet weak var textfieldAge:UITextField!
    @IBOutlet weak var textfieldDegree:UITextField!
    @IBOutlet weak var textfieldTags:UITextField!
    @IBOutlet weak var textfieldDating:UITextField!
    @IBOutlet weak var textfieldMessage:UITextField!
}

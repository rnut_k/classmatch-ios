//
//  RegisterEmailCell.swift
//  ClassMatch
//
//  Created by Mac on 8/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
class RegisterEmailCell: UICollectionViewCell {
    
    var vm:RegisterViewModel!
    
    @IBOutlet weak var textfieldEmail:UITextField!
    @IBOutlet weak var facebookLoginView:UIView!
    @IBOutlet weak var buttonNext:UIButton!
    
    @IBAction func buttonNextPreesed(sender:AnyObject){
        if let email = textfieldEmail.text{
            vm.goNextPage(0,info: email)
        }
        
    }
    
    var buttonFacebookLogin:FBSDKLoginButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        buttonNext.dimAndDisabled()
        buttonFacebookLogin = FBSDKLoginButton()
        textfieldEmail.delegate = self
    }
    
    func addFacebookLoginUI(){
        buttonFacebookLogin.frame = CGRectMake(0, 0, facebookLoginView.bounds.size.width, facebookLoginView.bounds.size.height)
        facebookLoginView.addSubview(buttonFacebookLogin)
    }
}
extension UIButton{
    func dimAndDisabled(){
        enabled = false
        alpha = 0.5
    }
    func appearAndEnabled(){
        enabled = true
        alpha = 1
    }
}
extension RegisterEmailCell:UITextFieldDelegate{
    func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("textFieldDidEndEditing")
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("textField_shouldChangeCharactersInRange")
        if validateEmail(textField.text!){
            buttonNext.appearAndEnabled()
        }else{
            buttonNext.dimAndDisabled()
        }
        return true
    }
}
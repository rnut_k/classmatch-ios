//
//  TeachingRegisterHeaderCell.swift
//  ClassMatch
//
//  Created by Mac on 9/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class TeachingRegisterHeaderCell: UITableViewCell {

    
    @IBOutlet weak var labelHeader: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

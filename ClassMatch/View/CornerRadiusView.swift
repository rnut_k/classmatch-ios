//
//  CornerRadiusView.swift
//  ClassMatch
//
//  Created by Mac on 10/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable
class CornerRadiusView: UIView {
    
    var _topCorner:Bool = false
    var _bottomCorner:Bool = false
    @IBInspectable var topCorner:Bool = false{
        didSet{
            _topCorner = topCorner
            if _topCorner{
                let newPath = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width - 100, height: frame.size.height)
                let rectShape = CAShapeLayer()
                rectShape.bounds = newPath
//                rectShape.position = self.center
                rectShape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.TopLeft , .TopRight], cornerRadii: CGSize(width: 10, height: 10)).CGPath
                self.layer.backgroundColor = UIColor.greenColor().CGColor
                self.layer.mask = rectShape
                self.layer.masksToBounds = true
            }
        }
    }
    @IBInspectable var bottomCorner:Bool = false{
        didSet{
            _bottomCorner = bottomCorner
            if _bottomCorner {
                let rectShape = CAShapeLayer()
                rectShape.bounds = frame
                rectShape.position = center
                rectShape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.BottomRight,.BottomLeft] ,cornerRadii: CGSize(width: 10, height: 10)).CGPath
                self.layer.mask = rectShape
                self.layer.masksToBounds = true
            }
        }
    }
    func setUp(){
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOffset = CGSizeMake(-1, 1)
        self.layer.shadowOpacity = 0.8
    }
//    func setCornerRadius(){
//        let rectShape = CAShapeLayer()
//        rectShape.bounds = frame
//        rectShape.position = center
//        rectShape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.BottomLeft, .BottomRight , .TopLeft] ,cornerRadii: CGSize(width: 10, height: 10)).CGPath
//        self.layer.mask = rectShape
//        self.layer.masksToBounds = true
//    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setUp()
    }
}
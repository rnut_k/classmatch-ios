//
//  MeEducationCell.swift
//  ClassMatch
//
//  Created by Mac on 8/27/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class MeEducationCell: UITableViewCell {

    weak var vm:MeViewModel!
    var education:Education!
    @IBOutlet weak var labelInstitude:UILabel!
    @IBOutlet weak var labelFaculty:UILabel!
    @IBOutlet weak var labelgrade:UILabel?
    @IBOutlet weak var labelperiodTime:UILabel?
    @IBOutlet weak var buttonApprove:UIButton!
    @IBAction func buttonApprovePreesed(sender:AnyObject){
        vm.performEvidence(.edu, item: education)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(educationItem:Education){
        self.education = educationItem
        labelInstitude.text = educationItem.name
        var stringFullFaculty = educationItem.detail
        if let subFaculty = educationItem.subDetail{
            stringFullFaculty = stringFullFaculty +  subFaculty
        }
        labelFaculty.text = stringFullFaculty
        if let grade = educationItem.grade{
            labelgrade!.text = grade
        }
        if let periodTime = educationItem.period{
            labelperiodTime!.text = periodTime
        }
        if educationItem.certified == 1{
            buttonApprove!.setImage(UIImage(named: "Certified"), forState: UIControlState.Normal)
            buttonApprove!.setImage(UIImage(named: "Certified"), forState: UIControlState.Highlighted)
        }else{
            buttonApprove!.setImage(UIImage(named: "UnCertified"), forState: UIControlState.Normal)
            buttonApprove!.setImage(UIImage(named: "UnCertified"), forState: UIControlState.Highlighted)
        }
    }
}


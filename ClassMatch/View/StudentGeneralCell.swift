//
//  StudentNumberCell.swift
//  ClassMatch
//
//  Created by Mac on 9/11/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import TextFieldEffects
class StudentGeneralCell: UICollectionViewCell {
    var vm:StudentPostFormViewModel!
    
    @IBOutlet weak var bulletText1:UILabel!
    @IBOutlet weak var bulletText2:UILabel!
    @IBOutlet weak var bulletText3:UILabel!
    @IBOutlet weak var bulletText4:UILabel!
    @IBOutlet weak var bulletText5:UILabel!
    @IBOutlet weak var bullet1:UIImageView!
    @IBOutlet weak var bullet2:UIImageView!
    @IBOutlet weak var bullet3:UIImageView!
    @IBOutlet weak var bullet4:UIImageView!
    @IBOutlet weak var bullet5:UIImageView!
    
    @IBOutlet weak var textFieldSubject:UITextField!
    @IBOutlet weak var textFieldPlaceHome:UITextField!
    @IBOutlet weak var textFieldPlaceOther:UITextField!
    @IBOutlet weak var textFieldPeriod:UITextField!
    @IBOutlet weak var textFieldPrice:UITextField!
    @IBOutlet weak var scrollViewGeneral:UIScrollView!
    
    @IBOutlet weak var controlDay1:UIImageView!
    @IBOutlet weak var controlDay2:UIImageView!
    @IBOutlet weak var controlDay3:UIImageView!
    @IBOutlet weak var controlDay4:UIImageView!
    @IBOutlet weak var controlDay5:UIImageView!
    @IBOutlet weak var controlDay6:UIImageView!
    @IBOutlet weak var controlDay7:UIImageView!
    
    @IBOutlet weak var controlPlaceHome:UIImageView!
    @IBOutlet weak var controlPlaceOther:UIImageView!
    
    
    @IBOutlet weak var controlPrice250:UIImageView!
    @IBOutlet weak var controlPrice300:UIImageView!
    @IBOutlet weak var controlPrice500:UIImageView!
    @IBOutlet weak var controlPrice750:UIImageView!
    @IBOutlet weak var controlPrice1000:UIImageView!
    @IBOutlet weak var controlPriceCustom:UIImageView!
    
    
    var customPrice = 0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldPrice.keyboardType = .NumberPad
        textFieldPeriod.keyboardType = .NumberPad
        
    }
    
    func configure(){
        let model = vm.studying
        let successImage = UIImage(named: IMAGENAME.certified_without_text.rawValue)
        let emptyBullet = UIImage(named: IMAGENAME.circle_bullet.rawValue)
        if let _ = model.subject {
            bullet1.image = successImage
            bulletText1.hidden = true
            
        }else{
            bullet1.image = emptyBullet
            bulletText1.hidden = false
        }
        if model.days.count > 0 {
            bullet2.image = successImage
            bulletText2.hidden = true
        }else{
            bullet2.image = emptyBullet
            bulletText2.hidden = false
        }
        
        if let _ = model.location {
            bullet3.image = successImage
            bulletText3.hidden = true
        }else{
            bullet3.image = emptyBullet
            bulletText3.hidden = false
        }
        
        if let _ = model.period{
            bullet4.image = successImage
            bulletText4.hidden = true
        }else{
            bullet4.image = emptyBullet
            bulletText4.hidden = false
        }
        if model.price > 0{
            bullet5.image = successImage
            bulletText5.hidden = true
        }else{
            bullet5.image = emptyBullet
            bulletText5.hidden = false
        }
        
        
    }

    
    
}

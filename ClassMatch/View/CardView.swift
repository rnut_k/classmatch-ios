//
//  CardView.swift
//  ClassMatch
//
//  Created by Mac on 9/13/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import QuartzCore
@IBDesignable
class CardView: UIView {

    
    var _cornerRadius:CGFloat = 10
    @IBInspectable var cornerRadius:CGFloat = 10{
        didSet{
            _cornerRadius = cornerRadius
            setUp()
        }
    }
    var _shadowOpacity:Float = 0.8
    @IBInspectable var shadowOpacity:Float = 0.8{
        didSet{
            _shadowOpacity = shadowOpacity
            setUp()
        }
    }
    func setUp(){
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 5
        layer.shadowOpacity = _shadowOpacity
        layer.cornerRadius = _cornerRadius
        layer.masksToBounds = true
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setUp()
    }
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        setUp()
    }
}

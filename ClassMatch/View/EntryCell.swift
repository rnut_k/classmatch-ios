//
//  EntryCell.swift
//  ClassMatch
//
//  Created by Mac on 8/20/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class EntryCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var buttonLogin: UIButton!
    
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelHeading: UILabel!
}

//
//  TeacherPostCell.swift
//  ClassMatch
//
//  Created by Mac on 9/13/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import Alamofire
class TeacherPostCell: UITableViewCell {

    var teacher:Teacher!
    @IBOutlet weak var labelSubject4: UILabel!
    @IBOutlet weak var labelSubject3: UILabel!
    @IBOutlet weak var labelSubject2: UILabel!
    @IBOutlet weak var labelSubject1: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var imageViewCertified: UIImageView!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.flatWhiteColor()
        backgroundColor = UIColor.clearColor()
        backgroundView = nil
        selectedBackgroundView = nil
        removeSeparator(400)
        
        view.backgroundColor = UIColor.whiteColor()
        view.layer.shadowOffset = CGSizeMake(-1, 1)
        view.layer.shadowOpacity = 0.2
        view.layer.cornerRadius = 5.0
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
    }
    func configureCell(){
        resetLabel()
        log.info("teacher.user::\(teacher.user)")
        if let user = teacher.user{
            labelName.text = user.name
        }
        labelDetail.text = teacher.getDetail()
        var i = 1
        for subject in teacher.subjects{
            switch i {
            case 1:
                labelSubject1.text = subject.name
                break
            case 2:
                labelSubject2.text = subject.name
                break
            case 3:
                labelSubject3.text = subject.name
                break
            default:
                labelSubject4.text = subject.name
                break
            }
            i += 1
        }
        let certifiedImage = teacher.certified == 1 ? UIImage(named: IMAGENAME.uncertified_without_text.rawValue) : UIImage(named: IMAGENAME.certified_without_text.rawValue)
        imageViewCertified.image = certifiedImage
    }
    func resetLabel(){
        labelName.text = ""
        labelDetail.text = ""
        labelSubject1.text = ""
        labelSubject2.text = ""
        labelSubject3.text = ""
        labelSubject4.text = ""
    }

}

//
//  NSDate.swift
//  ClassMatch
//
//  Created by Mac on 10/29/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

extension NSDate{
    func getCurrentWeekDay(){
//        NSDate *now = [NSDate date];
//        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        
//        NSDateComponents *dateComponents = [calendar components:NSWeekdayCalendarUnit | NSHourCalendarUnit fromDate:now];
//        NSInteger weekday = [dateComponents weekday];
    }
    func getThisSaturday(){
        //        NSDate *now = [NSDate date];
        //        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        //
        //        NSDateComponents *dateComponents = [calendar components:NSWeekdayCalendarUnit | NSHourCalendarUnit fromDate:now];
        //        NSInteger weekday = [dateComponents weekday];
        //        NSDate *nextSunday = null;
        //        if (weekday == 1 && [dateComponents hour] < 5) {
        //            // The next day is today
        //            nextSunday = now;
        //        }
        //        else  {
        //            NSInteger daysTillNextSunday = 8 - weekday;
        //            int secondsInDay = 86400; // 24 * 60 * 60
        //            nextSunday = [now dateByAddingTimeInterval:secondsInDay * daysTillNextSunday];
    }
    
    func toString()->String{
        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strDate = dateFormatter.stringFromDate(self)
        return strDate
    }
    func toStringOnlyTime()->String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm a"
        let strDate = dateFormatter.stringFromDate(self)
        return strDate
    }
    func toString(withTime:Bool)->String{
        let dateFormatter = NSDateFormatter()
        if withTime{
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd"
        }
        
        let strDate = dateFormatter.stringFromDate(self)
        return strDate
    }
    var formatted: String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEEE, dd MMM yyyy HH:mm:ss Z"
        return  formatter.stringFromDate(self)
    }
    
    static func parseDate(strDateFromApi:String)->NSDate?{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat =  "yyyy-MM-dd HH:mm:ss.SSSSSS"
        return dateformatter.dateFromString(strDateFromApi)
    }

}

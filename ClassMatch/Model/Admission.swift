//
//  Admission.swift
//  ClassMatch
//
//  Created by Mac on 12/7/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

internal enum AdmissionStatus{
    case Waiting
    case Active
    case Approved
    case Canceled
}
class Admission: Object {
    
    dynamic var id:Int = 0
    dynamic var imagePath:String?
    var teacher:Teacher!
    var studying:Studying!
    dynamic var approved:Int = 0
    var status:AdmissionStatus = .Waiting
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    func getDetail()->String{
        return studying.getDetail()
    }
}

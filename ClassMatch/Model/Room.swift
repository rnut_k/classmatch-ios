//
//  Room.swift
//  ClassMatch
//
//  Created by Mac on 10/25/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

protocol RoomMessageDelegate {
    func roomDidReceiveMessage(room:Room,message:Message)
    func roomDidChangeParnerReadMessageId(messageId:String)
    
}
class Room: Object {
    
    dynamic var id:String!
    dynamic var path:String!
    dynamic var sender:User!
    dynamic var receiver:User!
    var messages = List<Message>()
    var delegate:RoomMessageDelegate?
    
    dynamic var partnerReadMessageId:String?
    
    
    override class func primaryKey() -> String {
        return "id"
    }
    func add(message:Message){
        do{
            try uiRealm.write {
                self.messages.append(message)
                uiRealm.add(self,update: true)
                if let delegate = delegate{
                    delegate.roomDidReceiveMessage(self, message: message)
                }
            }
        }catch(let error){
            log.error(error)
        }
    }
    func getPartner()->User{
        if User.getSharedInStance()!.email == sender.email{
            return receiver
        }
        return sender
    }
    func getRoomImage()->Image?{
        let partner = getPartner()
        return partner.image
    }
    func updatePartnerReadMessageId(item:String){
        do{
            try uiRealm.write({
                self.partnerReadMessageId = item
                uiRealm.add(self,update: true)
                if let delegate = delegate{
                    delegate.roomDidChangeParnerReadMessageId(item)
                }
            })
        }catch(let error){
            log.error(error)
        }
    }
    

}


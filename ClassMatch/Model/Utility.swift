//
//  Utility.swift
//  ClassMatch
//
//  Created by Mac on 8/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import UIKit

enum TABBAR_IMAGE:String{
    case HOME = "tabbar-icon-home"
    case ALL_CLASSES = "tabbar-icon-allclasses"
    case NOTIFICATIONS = "tabbar-icon-notificaion"
    case MESSAGE = "tabbar-icon-message"
    case ME = "tabbar-icon-me"
}
enum NOTIFICATION_CATEGORY:String{
    
    case STUDYING_ADMISSION_ACTIVE = "studying_admission_active"
    case STUDYING_ADMISSION_CACELED = "studying_admission_canceled"
    case STUDYING_ADMISSION_APPROVED = "studying_admission_approved"
    case STUDYING_TRANSACTION_RENEW = "studying_transaction_renew"
    case STUDYING_TRANSACTION_CONFIRMED = "studying_transaction_confirmed"
    case STUDYING_TRANSACTION_DENIED = "studying_transaction_denied"
    case STUDYING_COMPLETE = "studying_complete"
    case STUDYING_QUEUE_END = "studying_queue_end"
    case CHAT = "chat"
}
enum ERROR_MESSAGE:String {
    case login = "ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบชื่อผู้ใช้และรหัสผ่านอีกครั้งค่ะ"
    case getUserInfo = "ไม่สามารถเรียกข้อมูลผู้ใช้ได้ กรุณาลองใหม่อีกครั้งค่ะ"
    case alertOk = "ตกลง"
    case alertError = "ผิดพลาด"
    case register = "ขออภัยค่ะ ไม่สามารถดำเนินการสมัครสมาชิกได้ กรุณาตรวจสอบข้อมูลและลองใหม่อีกครั้ง"
    case signout = "การออกจากระบบผิดพลาด กรุณาลองใหม่อีกครั้งในภายหลังค่ะ"
    case teacher_register = "ขออภัยค่ะ เกิดข้อผิดพลาดในการแสดงโปรไฟล์ของคุณสู่สาธารณะ กรุณาลองใหม่อีกครั้ง หรือติดต่อผู้ดูแลระบบ ขอบคุณค่ะ"
    case admission_expired = "การรับสอนนี้ เกินระยะเวลาที่กำหนดให้โอนเงิน กรุณาทำรายการใหม่ หรือติดต่อผู้ดูแลระบบ ขอบคุณค่ะ"
    case admision_expired_error = "เกิดข้อผิดพลาดในระบบรับสอน กรุณาลองใหม่อีกครั้งในภายหลัง ขอบคุณค่ะ"
    case default_error = "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ"
}

enum STORYBOARD:String{
    case main = "Main"
    case tabbar = "Tabbar"
    case studying_approval = "StudyingApproval"
    case message = "Message"
    
}
enum VIEWCONTROLLER:String{
    case entry = "entry_viewcontroller"
    case entry_navigation = "entry_navigation_viewcontroller"
    case tabbar = "tabbar_viewcontroller"
    case find_student = "student_viewcontroller"
    case find_teacher = "teacher_viewcontroller"
    case studying_approval = "studying_approval_viewcontroller"
    case studying_payment = "studying_payment_viewcontroller"
    case studying_approved = "studying_approved_viewcontroller"
    case studying_popup = "studying_popup_post_viewcontroller"
    case student_approve_studying = "student_approve_viewcontroller"
    case navigation_message = "navigationcontroller_message"
    case chat = "chat_viewcontroller"
    case popup_evidence = "popup_evidence_viewcontroller"
    
}

enum SEGUE:String {
    case confirm_success_post = "confirm_success_post"
    case chat = "chat_viewcontroller"
    case popup_evidence_education = "popup_evidence_education"
    case popup_evidence_testing = "popup_evidence_testing"
    case studying_form_general = "studying_form_general"
    case studying_form_other = "studying_form_other"
    case studying_form_success = "studying_form_success"
    case me_public_info = "me_public_info"
}
enum CELL:String {
    case TEACHER_CREATE_SELECTOR = "teacher_create_selector"
    case TEACHER_CREATE_HEADER = "teacher_create_header"
    case TEACHER_CREATE_FOOTER = "teacher_create_footer"
    case ROOM_CELL = "RoomCell"
}

let navigationBarHeight:CGFloat = 0
let colorBase = UIColor(hexString: "22BD8E")
let colorSecond = UIColor(red:0.13, green:0.21, blue:0.43, alpha:1.0)
let colorThird = UIColor(red:0.69, green:0.69, blue:0.69, alpha:1.0)
let colorBaseSecond = UIColor(red:0.54, green:0.54, blue:0.54, alpha:1.0)
let kImageControlToggleActive = "radioButton_active"
let kImageControlToggleInActive = "radioButton"
let kImageCertified = "Certified"
let kImageUnCertified = "UnCertified"
let kImageCertifiedWithOutText = "icon-certified-notext"

enum IMAGENAME:String{
    case icon_active = "active_icon"
    case icon_cancel = "cancel_icon"
    case icon_waiting = "waiting_icon"
    case certified = "Certified"
    case unCertified = "UnCertified"
    case certified_without_text = "icon-certified-notext"
    case circle_bullet = "circleBullet"
    case placeholder_profile_square = "placeholder-profile-square"
    case uncertified_without_text = "icon_uncertified_notext"
    case plus_sign = "plus-sign"
    case true_sign = "true-sign"
    //MARK:Image button number of student
    case button_student_single_deAction = "studying_single_student_de-action"
    case button_student_multiple_deAction = "studying_multiple_student"
    case button_student_single_action = "studying_single_student_active"
    case button_student_multiple_action = "studying_multiple_student_active"
    //radio button
    case controlToggleActive = "radioButton_active"
    case controlToggleInActive = "radioButton"
    
}

enum TEXT:String{
    case placeHolder_tag = "เลือกหัวข้อถนัดเฉพาะ"
    case noIdentify = "ไม่ระบุ"
}

class Utility: NSObject {
    
    static func getStoryboard(name:STORYBOARD)->UIStoryboard?{
        var storyboard:UIStoryboard?
        storyboard = UIStoryboard(name: name.rawValue, bundle: nil)
        return storyboard
    }
    static func getChatViewController()->ChatViewController{
        let storyboard = UIStoryboard(name: STORYBOARD.message.rawValue, bundle: nil)
        let chatViewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.chat.rawValue) as! ChatViewController
        return chatViewController
    }
    static func getEntryViewController()->EntryViewController{
        let storyboard = UIStoryboard(name: STORYBOARD.main.rawValue, bundle: nil)
        let entryViewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.entry.rawValue) as! EntryViewController
        return entryViewController
    }
    
    static func getTabbarViewController()->UITabBarController{
        let storyboard = UIStoryboard(name: STORYBOARD.tabbar.rawValue, bundle: nil)
        let tabBarController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.tabbar.rawValue) as! UITabBarController
        return tabBarController
    }
    static func getStudyingApprovalStoryboard()->UIStoryboard{
        let storyboard = UIStoryboard(name: STORYBOARD.studying_approval.rawValue, bundle: nil)
        return storyboard
    }
    static func getPopUpConfirmSuccessPostViewController()->StudentPopupSuccess{
        let storyboard = UIStoryboard(name: STORYBOARD.tabbar.rawValue, bundle: nil)
        let viewController = storyboard.instantiateViewControllerWithIdentifier(VIEWCONTROLLER.studying_popup.rawValue) as! StudentPopupSuccess
        return viewController
    }
    static func getMessageViewController()->UITabBarController{
        let tabbar = Utility.getTabbarViewController()
        tabbar.selectedIndex = 3
        return tabbar
    }
}


class NavController: UINavigationController {
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
}

//
//  Studying.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift
class Studying: Object {
    
    dynamic var id:Int = 0
    dynamic var number:Int = 0
    dynamic var subject:Subject?
    var days = List<Day>()
    var location:Location?
    dynamic var period:String?
    dynamic var price:Int = 0
    dynamic var age:String?
    dynamic var degree:Degree?
    var tags = List<Tag>()
    dynamic var date:NSDate?
    dynamic var message:String?
    dynamic var created_at:String!
    dynamic var approved:Int = 0
    
    var admissions = List<Admission>()
//    0 = waiting
//    1 = approved
//    2 = canceled
    func getDaysTitle()->String{
        var title = "ทุก"
        for day in days {
            title += day.name + " "
        }
        return title
    }
    
    func getTagsTitle()->String{
        var title = ""
        for tag in tags {
            title += tag.name + " "
        }
        return title
    }
    func getPostDetail()->String{
        var strAge = "ไม่ระบุ"
        var strTags = "ไม่ระบุ"
        if let age = age{
            strAge = age
        }
        if getTagsTitle() != ""{
            strTags = getTagsTitle()
        }
        var detail =  "นักเรียน "
        detail += degree!.name
        detail += "  อายุ "
        detail += strAge
        detail += "  \(number) ท่าน  "
        detail += "\n"
        detail += "เน้น "
        detail += strTags
        return detail
    }
    func getDetail()->String{
        var detail =  subject!.name!
        detail += "\n"
        detail += degree!.name
        detail += "\n"
        detail += "จำนวน : \(number) ท่าน  "
        detail += "\(period!) \(price) บาท"
        detail += "\nเน้น : "
        detail += self.getTagsTitle()
        return detail
    }
    func getActiveAdmission()->Admission?{
        let admission = admissions.filter({(admission)  in  admission.approved == 1 })
        return  admission.count == 0 ? nil : admission.first
    }
    func getLatestAdmision()->Admission?{
        return admissions.last
    }
    
}

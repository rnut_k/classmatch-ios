//
//  Image.swift
//  ClassMatch
//
//  Created by Mac on 9/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Image: Object {
    
    dynamic var id:Int = 0
    dynamic var name:String?
    dynamic var path:String?
    
    override class func primaryKey() -> String {
        return "id"
    }

}

//
//  Location.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Location: Object {
    
    dynamic var id:Int = 0
    dynamic var name:String!
    dynamic var detail:String?
    dynamic var lat:String!
    dynamic var lon:String!
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    func getFullDetail()->String{
        var full = name
        if let detail = detail{
            full = detail + full
        }
        return full
    }
}

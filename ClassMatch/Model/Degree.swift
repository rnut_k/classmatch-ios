//
//  Degree.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Degree: Object {
    dynamic var id:Int = 0
    dynamic var name:String!
    
    override class func primaryKey() -> String {
        return "id"
    }
}
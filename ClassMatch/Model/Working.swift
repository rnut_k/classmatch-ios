//
//  Woking.swift
//  ClassMatch
//
//  Created by Mac on 9/21/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Working: Object {
    
    dynamic var id:Int = 0
    dynamic var name:String!
    dynamic var detail:String?
    dynamic var position:String?
    dynamic var period:String? //ช่วงเวลา
    override class func primaryKey() -> String {
        return "id"
    }
    func getFullDetail()->String{
        var fullDetail = ""
        if let detail = detail{
            fullDetail += detail
            fullDetail += ", "
        }
        fullDetail += name
        return fullDetail
    }
}

//
//  Day.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Day: Object {
    
    dynamic var name:String!
    dynamic var id:Int = 0
    static func getAll()->[Day]?{
        let days =  uiRealm.objects(Day).toArray()
        return days
    }
    static func createDays(){
        let sunday = Day(value: ["id":1 , "name":"วันอาทิตย์"])
        sunday.save()
        let monday = Day(value: ["id":2 , "name":"วันจันทร์"])
        monday.save()
        let tuesday = Day(value: ["id":3 , "name":"วันอังคาร"])
        tuesday.save()
        let wednesday = Day(value: ["id":4 , "name":"วันพุธ"])
        wednesday.save()
        let thursday = Day(value: ["id":5 , "name":"วันพฤหัส"])
        thursday.save()
        let friday = Day(value: ["id":6 , "name":"วันศุกร์"])
        friday.save()
        let saturday = Day(value: ["id":7 , "name":"วันเสาร์"])
        saturday.save()
    }
    
    override class func primaryKey() -> String {
        return "id"
    }
}

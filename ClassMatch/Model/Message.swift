//
//  Message.swift
//  ClassMatch
//
//  Created by Mac on 10/18/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Message: Object {
    
    dynamic var id:String!
    dynamic var senderId:String! // use string for sync with chatViewcontroller
    dynamic var senderName:String!
    dynamic var receiverId:Int = 0
    dynamic var text:String!
    dynamic var isNotify = false
    dynamic var createdAt = NSDate()
    dynamic var room:Room!
    override class func primaryKey() -> String {
        return "id"
    }
    static func createFromNotification(userInfo:[NSObject:AnyObject])->Message?{
        
        guard let senderId = userInfo["senderId"] as? String else{ return nil}
        guard let senderName = userInfo["senderName"] as? String else{ return nil}
        guard let receiverId = userInfo["receiverId"]?.integerValue else{ return nil}
        guard let text = userInfo["text"] as? String else{ return nil }
        guard let roomId = userInfo["roomId"] as? String else{ return nil }
        let message = Message()
        message.senderId = senderId
        message.senderName = senderName
        message.receiverId = receiverId
        message.text = text
        if let room = uiRealm.objectForPrimaryKey(Room.self, key: roomId) {
            message.room = room
        }
        return message
        
    }
    static func create(id:String,senderId:String,text:String,createdAt:NSDate,room:Room)->Message? {
        let message = Message()
        message.id = id
        message.senderId = senderId
        message.text = text
        message.createdAt = createdAt
        message.room = room
        message.save()
        return  message
    }
    
    func setNotified(){
        do{
            try uiRealm.write {
                self.isNotify = true
                uiRealm.add(self,update: true)
            }
        }catch(let error){
            log.error(error)
        }
    }
}

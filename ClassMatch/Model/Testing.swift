//
//  Testing.swift
//  ClassMatch
//
//  Created by Mac on 9/20/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Testing: Object {
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
    dynamic var id:Int = 0
    dynamic var name:String!
    dynamic var result:String?
    dynamic var dating:NSDate?
    dynamic var message:String?
    dynamic var certified:Int = 0 //ผ่านการรับรอง
    override class func primaryKey() -> String {
        return "id"
    }
}

//
//  User.swift
//  ClassMatch
//
//  Created by Mac on 8/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift
import UIKit
import FirebaseInstanceID
class User: Object {
    
    static private var sharedInstance:User?
    static let preferences = NSUserDefaults.standardUserDefaults()
    static let kLoggedInId = "loggedInId"
    
    static let kNotificationId = "APN"

    dynamic var id:Int = 0
    dynamic var email:String!
    dynamic var name:String!
    dynamic var birthDate:String!
    dynamic var token:String?
    dynamic var createdAt = NSDate()
    dynamic var tokenAPN:String?
    dynamic var teacher:Teacher?
    
    dynamic var flagSubscribe = false //optional
    dynamic var flagOnline = false //optional
    let educations = List<Education>()
    let workings = List<Working>()
    var testings = List<Testing>()
    var tags = List<Tag>()
    var rooms = List<Room>()
    dynamic var image:Image?
    override class func primaryKey() -> String {
        return "id"
    }
    
    //setter
    func updateOnline(item:Bool){
        do{
            try uiRealm.write({
                self.flagOnline = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func updateSubScribe(item:Bool){
        do{
            try uiRealm.write({
                self.flagSubscribe = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func addTesting(item:Testing){
        do{
            try uiRealm.write({
                self.testings.append(item)
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func replaceTags(items:List<Tag>){
        try! uiRealm.write({
            self.tags = items
            uiRealm.add(self,update: true)
        })
    }
    func replaceTestings(items:List<Testing>){
        try! uiRealm.write({
            self.testings = items
            uiRealm.add(self,update: true)
        })
        
    }
    func updateName(item:String){
        do{
            try uiRealm.write({
                self.name = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func updateId(item:Int){
        do{
            try uiRealm.write({
                self.id = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func updateEmail(item:String){
        do{
            try uiRealm.write({
                self.email = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func updateToken(item:String?){
        
        do{
            try uiRealm.write({
                self.token = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func updateTeacher(item:Teacher){
        do{
            try uiRealm.write({
                self.teacher = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    func updateTokenAPN(item:String){
        do{
            try uiRealm.write({
                self.tokenAPN = item
                uiRealm.add(self,update: true)
            })
        }catch(let error){
            log.error("realm::\(error)")
        }
    }
    
    //getter
    static func setSharedAPN(notificationId:String){
        // force set token by set from firebase class
        guard let token = FIRInstanceID.instanceID().token() else { return }
        User.preferences.setObject(token, forKey: User.kNotificationId)
        User.preferences.synchronize()
    }
    static func getSharedAPN()->String?{
        // force set token by get from firebase class
        guard let token = FIRInstanceID.instanceID().token() else { return nil}
//        guard let notificationId = User.preferences.objectForKey(User.kNotificationId) as? String else{
//            return nil
//        }
//        return notificationId
        return token
        
    }
    static func setSharedInStance(user:User?){
        if let user = user{
            User.sharedInstance = user
            User.preferences.setInteger(user.id, forKey: User.kLoggedInId)
            User.preferences.synchronize()
            user.save()
            ad.shouldSubscibeNotification()
            return
        }
        User.sharedInstance = nil
        
    }
    static func getSharedInStance()->User?{
        if let user =  User.sharedInstance{
            return user
        }
        if let id = User.preferences.objectForKey(User.kLoggedInId) {
            guard let user = uiRealm.objectForPrimaryKey(User.self, key: id) else { return nil }
            User.setSharedInStance(user)
            return user
        }
        return nil
    }
    
    private static func getPersistantSharedUser()->User{
        log.debug("getPersistantSharedUser")
        if let user =  uiRealm.objects(User).filter("id = 0").first{
            return user
        }else{
            let user = User()
            do{
                try uiRealm.write({
                    user.id = 0
                    uiRealm.add(user, update: true)
                })
            }catch(let error){
                log.error("realm::\(error)")
            }
            return user
        }
        
    }
    static func signOut(){
        User.setSharedInStance(nil)
    }
    func getSubjectTeacherAttributeString()->NSAttributedString?{
        guard let teacher = teacher else { return nil }
        let attr = NSMutableAttributedString()
        if teacher.subjects.count == 0 { return nil }
        let subAttrTitle = NSAttributedString(string: "สอนวิชา", attributes: [NSForegroundColorAttributeName:UIColor.blackColor(),NSFontAttributeName:UIFont.boldSystemFontOfSize(15)])
        attr.appendAttributedString(subAttrTitle)
        for subject in teacher.subjects{
            let subAttrBullet = NSAttributedString(string: "  •", attributes: [NSForegroundColorAttributeName:colorBase])
            attr.appendAttributedString(subAttrBullet)
            let subAttrStr = NSAttributedString(string: subject.name!, attributes: [NSForegroundColorAttributeName:UIColor.flatBlackColor()])
            attr.appendAttributedString(subAttrStr)
        }
        return attr
    }
}

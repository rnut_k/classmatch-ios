//
//  Tag.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Tag: Object {
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
    dynamic var name:String!
    dynamic var id:Int = 0
    dynamic var flagSelected:Bool = false
    
    override class func primaryKey() -> String {
        return "name"
    }
    convenience init(name: String) {
        self.init()
        self.name = name
        self.save()
    }
    
    func setSelected(value:Bool){
        do{
            try uiRealm.write({ 
                self.flagSelected = value
            })
        }catch{
            
        }
    }

}

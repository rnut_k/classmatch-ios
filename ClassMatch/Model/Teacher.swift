//
//  Teacher.swift
//  ClassMatch
//
//  Created by Mac on 9/23/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift


enum Displayable:String {
    case Working = "App\\Working"
    case Education = "App\\Education"
}
class Teacher: Object {
    
    dynamic var id:Int = 0
    dynamic var user:User?
    dynamic var displayableType:String?
    dynamic var working:Working?
    dynamic var education:Education?
    dynamic var certified:Int = 0
    
    let subjects = List<Subject>()
    override class func primaryKey() -> String {
        return "id"
    }
    func getDetail()->String{
        if let working = working{
            return working.getFullDetail()
        }
        if let education = education{
            return education.getFullDetail()
        }
        return ""
    }
    
    func updateTeacher(teacher:Teacher)->Teacher{
        do{
            try uiRealm.write {
                self.user = teacher.user
                self.displayableType = teacher.displayableType
                self.working = teacher.working
                self.education = teacher.education
                self.certified = teacher.certified
                
                self.subjects.removeAll()
//                    = teacher.subjects
                subjects.appendContentsOf(teacher.subjects)
                log.info(subjects)
                log.info(teacher.subjects)
                uiRealm.add(self,update: true)
            }
        }catch(let error){
            log.error(error)
        }
        return self
    }
    
    func getDisplayableInfo()->String{
        guard let displayableType = displayableType else { return "" }
        switch displayableType {
        case Displayable.Working.rawValue:
            guard let working = working else { return "" }
            return working.getFullDetail()
        default:
            guard let education = education else { return "" }
            return education.getFullDetail()
        }
    }
 
//    try! uiRealm.write({
//    teacherPersistant.id = teacher.id
//    teacherPersistant.user = teacher.user
//    teacherPersistant.certified = teacher.certified
//    teacherPersistant.displayableType = teacher.displayableType
//    teacherPersistant.education = teacher.education
//    teacherPersistant.working = teacher.working
//    teacherPersistant.subjects = teacher.subjects
//    teacherPersistant.save()
//    })
}

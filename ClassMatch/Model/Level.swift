//
//  Level.swift
//  ClassMatch
//
//  Created by Mac on 9/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Level: Object {
    
    dynamic var id:Int = 0
    dynamic var name:String!
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    static func getAll()->[Level]?{
        let levels =  uiRealm.objects(Level)
        if levels.count > 0{
            return levels.toArray()
        }
        return nil
    }
}

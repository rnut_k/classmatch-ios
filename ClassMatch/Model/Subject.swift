//
//  Subject.swift
//  ClassMatch
//
//  Created by Mac on 9/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Subject: Object {
    
// Specify properties to ignore (Realm won't persist these)
    dynamic var id:Int = 0
    dynamic var name:String?
    dynamic var image:Image?
    
    override class func primaryKey() -> String {
        return "id"
    }
//    func save(){
//        do{
//            try uiRealm.write {
//                uiRealm.add(self,update: true)
//            }
//        }catch(let error){
//            log.error(error)
//        }
//        
//    }
    class func getSubjects()->[Subject]?{
        let subject = uiRealm.objects(Subject.self)
        if subject.count > 0{
            return subject.toArray()
        }
        return nil
    }
}

//
//  Education.swift
//  ClassMatch
//
//  Created by Mac on 8/27/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import RealmSwift

class Education: Object {
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
    dynamic var id:Int = 0
    dynamic var level:Level! //วุฒิการศึกษา
    dynamic var name:String! //สถาบันการศึกษา
    dynamic var detail:String! //คณะ/สายการเรียน (มัธยมปลาย)
    dynamic var subDetail:String? //สาขา
    dynamic var grade:String? //เกรดเฉลี่ย
    dynamic var period:String? //ช่วงเวลา
    dynamic var certified:Int = 0 //ผ่านการรับรอง
    
    override class func primaryKey() -> String {
        return "id"
    }
    func getFullDetail()->String{
        var fullDetail = ""
        if detail != ""{
            fullDetail += detail + ", "
        }
        fullDetail += name
        return fullDetail
    }

    
    
}


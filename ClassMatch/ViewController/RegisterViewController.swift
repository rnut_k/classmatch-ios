//
//  RegisterViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit


class RegisterViewController: UIViewController {
    
    @IBOutlet weak var pageControl:UIPageControl!
    @IBOutlet weak var collectionView:UICollectionView!
    let pages = 4
    let registerUser = User()
    var indexPage = 0{
        didSet{
            pageControl.currentPage = indexPage
        }
    }
    var vm = RegisterViewModel()
    var flagIsFreezeView = false
    
    @IBAction func buttonBackPressed(sender:AnyObject){
        if indexPage == 0 {
            let entryViewController = Utility.getEntryViewController()
            self.presentViewController(entryViewController, animated: true, completion: nil)
        }else{
            vm.delegate.registerViewModelshoudGotoPrevious(indexPage)
            indexPage -= 1
            let indexPath = NSIndexPath(forItem: indexPage, inSection: 0)
            collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.delegate = self
        collectionView.dataSource = self
        collectionView.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        forceStopScrolling()
    }
    
    func forceStopScrolling(){
        self.collectionView.scrollEnabled   = false
        self.collectionView.pagingEnabled   = false
    }
    func startRegister(){
        showLoadingHud(nil)
        vm.startRegister()
    }
}


extension RegisterViewController:RegisterViewModelDelegate{
    func registerViewModelshoudGotoNext(index: Int) {
        if index < pages-1{
            print("RegisterViewModelDelegate::registerViewModelshoudGotoNext::index::\(index)")
            let indexPath = NSIndexPath(forItem: index+1, inSection: 0)
            collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
            indexPage += 1
            if index == 1{
                self.view.endEditing(true)
            }
        }else{
            startRegister()
        }
    }
    func registerViewModelshoudGotoPrevious(index: Int) {
        if index == 3{
            self.view.endEditing(true)
        }
        print("RegisterViewModelDelegate::registerViewModelshoudGotoPrevious::index::\(index)")
    }
    func registerViewModelDidRegisterSuccess(){
        print("\nRegisterViewController::Success::registerViewModelDidRegisWithFacebookSuccess\n")
        dismissFreezeView({
            let tabbarViewcontroller = Utility.getTabbarViewController()
            self.presentViewController(tabbarViewcontroller, animated: true, completion: nil)
        })
    }
    func registerViewModelDidRegisterError() {
        print("\nRegisterViewController::ERROR::registerViewModelDidRegisWithFacebookError\n")
        showErrorHud(ERROR_MESSAGE.register.rawValue,completion: nil)
    }
}

extension RegisterViewController:UIViewControllerTransitioningDelegate{
    func dismissFreezeView(completion:(()->Void)?){
        if flagIsFreezeView {
            self.dismissViewControllerAnimated(true){
                
                completion?()
            }
            self.flagIsFreezeView = false
        }else{
            completion?()
        }
    }
    func showLoadingHud(completion:(()->Void)?){
        dismissFreezeView {
            let progress = HUDProgressViewController(status: "Doing...")
            self.flagIsFreezeView = true
            self.presentViewController(progress, animated: true, completion: {
                
                completion?()
            })
            
        }
    }
    func showErrorHud(message:String,completion:(()->Void)?){
        dismissFreezeView {
            let alertController = UIAlertController(title: ERROR_MESSAGE.alertError.rawValue, message: message, preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                self.flagIsFreezeView = false
            })
            alertController.addAction(defaultAction)
            self.flagIsFreezeView = true
            self.presentViewController(alertController, animated: true, completion: {
                
                completion?()
            })
            
            
        }
        
        
    }
}

extension RegisterViewController:UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate{
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("register_email", forIndexPath: indexPath) as! RegisterEmailCell
            cell.buttonFacebookLogin.delegate = vm
            cell.addFacebookLoginUI()
            cell.vm = vm
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("register_name", forIndexPath: indexPath) as! RegisterNameCell
            cell.vm = vm
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("register_birthdate", forIndexPath: indexPath) as! RegisterBirthDateCell
            cell.vm = vm
            return cell
        default:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("register_password", forIndexPath: indexPath) as! RegisterPasswordCell
            cell.vm = vm
            
            return cell
        }
        
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageWidth = CGRectGetWidth(scrollView.frame)
        indexPage = Int(collectionView.contentOffset.x / pageWidth)
        pageControl.currentPage = indexPage
        
    }
    
}
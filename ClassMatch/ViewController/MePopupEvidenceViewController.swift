//
//  MePupupEvidenceViewController.swift
//  ClassMatch
//
//  Created by Mac on 12/6/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import ALCameraViewController
enum EvidenceType{
    case edu
    case test
}
class MePopupEvidenceViewController: UIViewController {

    
    var evidence_id:Int!
    var evidenceType:EvidenceType!
    @IBOutlet weak var buttonUpload:UIButton!
    @IBOutlet weak var buttonSkip:UIButton!
    @IBOutlet weak var labelEvidenceType:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonUpload.addTarget(self, action: #selector(self.didChooseImagePicker), forControlEvents: .TouchUpInside)
        buttonSkip.addTarget(self, action: #selector(self.skip), forControlEvents: .TouchUpInside)
        
    }
    func skip(){
        self.dismissFreezeViewController { 
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    override func viewWillAppear(animated: Bool) {
//        self.hideTabbar()
        super.viewWillAppear(animated)
        var textEvidenceType = ""
        if evidenceType == EvidenceType.edu{
            textEvidenceType = "หลักฐานสำหรับยืนยันการศึกษาของคุณ"
        }else{
            textEvidenceType = "หลักฐานสำหรับยืนยันการสอบ"
        }
        labelEvidenceType.text = textEvidenceType
    }

    override func viewWillDisappear(animated: Bool){
//        self.showTabbar()
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func didChooseImagePicker() {
        let cameraViewController = CameraViewController(croppingEnabled: true, allowsLibraryAccess: true) { (image, asset) in
            self.dismissViewControllerAnimated(false, completion: {
                if let image = image{
                    self.showLoadingHudViewController(nil)
                    self.uploadEvidence(image)
                }
            })
        }
        presentViewController(cameraViewController, animated: true, completion: nil)
    }
    
    
    func uploadEvidence(image:UIImage){
        ProfileManager.uploadEvidence(evidenceType, evidence_id: evidence_id, image: image) { (success) in
            if(success){
                self.dismissFreezeViewController({
                    let alertController = UIAlertController(title: "สำเร็จ", message: "ระบบได้รับหลักฐานของคุณเรียบร้อยแล้ว กรุณารอการยืนยันซักครู่ค่ะ", preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                        self.dismissFreezeViewController({ 
                            self.dismissViewControllerAnimated(false, completion: nil)
                        })
                    })
                    alertController.addAction(defaultAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                    return
                })
            }
            else{
                self.showErrorHudViewController("เกิดข้อผิดพลาดในการส่งหลักฐาน กรุณาลองใหม่อีกครั้งภายหลังค่ะ", completion: {
                
                })
            }
            
            self.dismissFreezeViewController(nil)
        }
    }

}

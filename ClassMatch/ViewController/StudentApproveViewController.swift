    //
//  StudentApproveViewController.swift
//  ClassMatch
//
//  Created by Mac on 10/2/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class StudentApproveViewController: UIViewController {

    @IBOutlet weak var imageViewTeacherProfile: UIImageView!
    @IBOutlet var buttonApproved:UIButton!
    @IBAction func buttonApprovedPressed(){
        approved()
    }
    @IBAction func buttonDeiniedPressed(){
        denied()
    }
    var admissionId:Int!
    var admission:Admission?
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        StudentHelper.getAdmission(admissionId) { (success, admission) in
            if success{
                guard let admission = admission else { return }
                self.admission = admission
                self.downloadImageTeacher()
                return
            }
        }
    }
    func downloadImageTeacher(){
        if let admission = self.admission{
            guard let imagePath = admission.imagePath else { return }
            ImageHelper.downLoadImage(imagePath, completionHandler: { (success, image) in
                if success{
                    self.imageViewTeacherProfile.image = image
                }
            })
        }
        
    }
    
    func closeViewController(){
        self.dismissFreezeViewController { 
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    func approved(){
        showLoadingHudViewController(nil)
        StudentHelper.approveAdmission(admissionId) { (success) in
            self.dismissFreezeViewController({ 
                if success{
                    let alertController = UIAlertController(title: "Classmatch", message: "ยืนยันการเลือกคุณครูเรียบร้อยแล้วค่ะ ระบบจะแจ้งเตือนอีกครั้งเมื่อคุณครูดำเนินการเรียบร้อยแล้ว ขอบคุณค่ะ", preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                        self.closeViewController()
                    })
                    alertController.addAction(defaultAction)
                    return self.presentViewController(alertController, animated: true, completion: nil)
                }
                return self.showErrorHudViewController("ระบบไม่สามารถยืนยันการเลือกของคุณได้ กรุณาลองใหม่อีกครั้งในเมนู notifications ขออภัยในความไม่สะดวก ขอบคุณค่ะ", completion: {
                    self.closeViewController()
                })
            })
        }
    }
    func denied(){
        StudentHelper.denyAdmission(admissionId) { (success) in
            self.dismissFreezeViewController({
                if success{
                    let alertController = UIAlertController(title: "Classmatch", message: "ปฏิเสธการเลือกคุณครูเรียบร้อยแล้วค่ะ ระบบจะแจ้งเตือนอีกครั้งเมื่อมีคุณครูท่านอื่นสนใจ ขอบคุณค่ะ", preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                        self.closeViewController()
                    })
                    alertController.addAction(defaultAction)
                    return self.presentViewController(alertController, animated: true, completion: nil)
                }
                return self.showErrorHudViewController("ระบบไม่สามารถยืนยันการเลือกของคุณได้ กรุณาลองใหม่อีกครั้งในเมนู notifications ขออภัยในความไม่สะดวก ขอบคุณค่ะ", completion: {
                    self.closeViewController()
                })
            })
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  MeEducationViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/28/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit
class MeAddWorkingViewController: UIViewController {

    var flagIsFreezeView = false
    let kKeyboardHeight:CGFloat = 400
    var viewFrameBackup:CGRect!
    var activeTextField:UITextField!
    var yearsDataSource = [String]()
    let working = Working()
    //MARK: IBOUTLET
    var pickerViewInput:UIPickerView!
    @IBOutlet weak var buttonPickerDone: UIButton!
    @IBOutlet weak var textfieldName: UITextField!
    @IBOutlet weak var textfieldRole: UITextField!
    @IBOutlet weak var textfieldPeriodBegin: UITextField!
    @IBOutlet weak var textfieldPeriodEnd: UITextField!
    @IBOutlet weak var textfieldOther: UITextField!
    
    @IBAction func buttonSavePressed(sender: AnyObject) {
        self.showLoadingHudViewController(nil)
        working.name = textfieldName.text
        working.detail = ""
        var periodString =  ""
        if let begin = textfieldPeriodBegin.text{
            periodString += begin
        }
        if let end =  textfieldPeriodEnd.text where textfieldPeriodEnd.text != ""{
            periodString += " - \(end)"
        }
        working.period = periodString
        working.position = textfieldRole.text
        ProfileManager.createWorking(working) { (success, working) in
            if success{
                if let working = working{
                    do{
                        try uiRealm.write({
                            let user = User.getSharedInStance()
                            user!.workings.append(working)
                            uiRealm.add(user!, update: true)
                            self.dismissFreezeViewController({ 
                                self.dismissViewControllerAnimated(false, completion: nil)
                            })
                        })
                    }catch(let error){
                        log.error(error)
                    }
                }
                
            }else{
                self.showErrorHudViewController(ERROR_MESSAGE.default_error.rawValue, completion: nil)
            }
        }
    }
    @IBOutlet weak var buttonSave: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "โปรไฟล์"
        setUpPickerView()
        setUpTextfield()
        changeBackButtonToCloseButton()
        loadYearPickerDataSource()
    }
    func setUpPickerView(){
        pickerViewInput = UIPickerView(frame: CGRect(x: 0, y: self.view.bounds.width - 250, width: self.view.bounds.width, height: 250))
        pickerViewInput.delegate = self
        pickerViewInput.dataSource = self
        pickerViewInput.backgroundColor = colorBase
    }
    func loadYearPickerDataSource(){
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy"
        if let i2 = Int(formatter.stringFromDate(NSDate())){
            for i in 1960...i2 {
                yearsDataSource.append(String(i))
            }
        }
    }
    func setUpTextfield(){
        textfieldName.placeholder = "ชื่อองค์กร"
        textfieldRole.placeholder = "ตำแหน่ง"
        textfieldPeriodBegin.placeholder = "ตั้งแต่ปี"
        textfieldPeriodEnd.placeholder = "ถึงปี (ประมาณ)"
        textfieldOther.placeholder = "อื่นๆ (หน้าที่หลัก, ความสำเร็จ, ฯลฯ)"
        textfieldName.delegate = self
        textfieldRole.delegate = self
        textfieldPeriodBegin.delegate = self
        textfieldPeriodEnd.delegate = self
        textfieldOther.delegate = self
        
        textfieldPeriodBegin.inputView = pickerViewInput
        textfieldPeriodEnd.inputView = pickerViewInput
    
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        viewFrameBackup = view.frame
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

}
extension MeAddWorkingViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextField = textField
    }
}

extension MeAddWorkingViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearsDataSource.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let choose = yearsDataSource[row]
        activeTextField.text = choose
    }
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let string =  yearsDataSource[row]
        return NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }
}

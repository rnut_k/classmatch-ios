//
//  TeachingConfirmViewController.swift
//  ClassMatch
//
//  Created by Mac on 9/22/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class TeachingConfirmViewController: UIViewController {
//    let user = User.getSharedInStance()!
    var teacher:Teacher!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDisplayable: UILabel!
    @IBOutlet weak var imageViewProfile: UIImageView!
    @IBOutlet weak var labelSubject1: UILabel!
    @IBOutlet weak var labelSubject2: UILabel!
    @IBOutlet weak var labelSubject3: UILabel!
    @IBOutlet weak var labelSubject4: UILabel!
    @IBOutlet weak var buttonPost: RoundButton!
    @IBAction func buttonPostPressed(sender: AnyObject) {
//        dismissViewController()
        ProfileManager.registerTeacher(teacher) { (success, teacher) in
            if success{
                let tabbar = self.presentingViewController as! CustomTabbarController
                let navController = tabbar.selectedViewController as! NavController
                let me = navController.topViewController as! MeViewController
                me.callApiGetProfile()
                return me.dismissViewControllerAnimated(true, completion: nil)
//                return self.dismissViewControllerAnimated(false, completion: nil)
            }
            self.showErrorHudViewController(ERROR_MESSAGE.teacher_register.rawValue, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissViewController))
        view.addGestureRecognizer(dismissGesture)
        getImageProfile()
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        updateLable()
    }
    func getImageProfile(){
        guard let image = User.getSharedInStance()!.image else { return }
        guard let path = image.path else { return }
        ImageHelper.downLoadImage(path) { (success, image) in
            if success{
                self.imageViewProfile.image = image
            }
        }
    }
    func updateLable(){
        resetLabel()
        updateProfileLable()
        updateSubjectsLabel()
    }
    func resetLabel(){
        labelName.text = ""
        labelDisplayable.text = ""
        labelSubject1.text = ""
        labelSubject2.text = ""
        labelSubject3.text = ""
        labelSubject4.text = ""
        
    }
    func updateSubjectsLabel(){
        var index = 0
        for subject in teacher.subjects{
            switch  index{
            case 0:
                labelSubject1.text = subject.name
                break
            case 1:
                labelSubject2.text = subject.name
                break
            case 2:
                labelSubject3.text = subject.name
                break
            default:
                labelSubject4.text = subject.name
                break
            }
            index += 1
        }
    }
    func updateProfileLable(){
        labelName.text = User.getSharedInStance()!.name
        labelDisplayable.text = teacher.getDisplayableInfo()
        
    }
    

    func dismissViewController(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}

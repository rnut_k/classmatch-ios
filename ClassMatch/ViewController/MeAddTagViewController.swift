    //
//  MeAddTagViewController.swift
//  ClassMatch
//
//  Created by Mac on 11/21/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import RealmSwift
import WSTagsField
class MeAddTagViewController: UIViewController {
    

    @IBAction func buttonSavePressed(sender: AnyObject) {
//        self.showLoadingHudViewController(nil)
        ProfileManager.createTags(selectedTags,completionHandler: { (success) in
            if success {
                let alertController = UIAlertController(title: "สำเร็จ", message: "เพิ่มข้อมูลหัวข้อถนัดเฉพาะเรียบร้อยแล้ว", preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                    self.dismissViewControllerAnimated(false, completion: nil)
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }else{
                self.dismissFreezeViewController(nil)
            }
            
        })
        
    }
    @IBOutlet weak var constraintHeightCardView: NSLayoutConstraint!
    @IBOutlet weak var contraintTopTableView: NSLayoutConstraint!
    @IBOutlet weak var headerView: CardView!
    @IBOutlet weak var tagsField: WSTagsField!
    @IBOutlet weak var tableView:UITableView!
    var tags = List<Tag>()
    var selectedTags = List<Tag>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        setTagField()
        changeBackButtonToCloseButton()
        title = "โปรไฟล์"
    }
    func setTagField(){
        tagsField.placeholder = TEXT.placeHolder_tag.rawValue
        tagsField.backgroundColor = .whiteColor()
        tagsField.frame = CGRect(x: 0, y: 44, width: 200, height: 70)
        tagsField.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tagsField.spaceBetweenTags = 7.0
        tagsField.font = .systemFontOfSize(15.0)
        
        tagsField.tintColor = colorBase
        tagsField.textColor = .blackColor()
        
        
        tagsField.selectedColor = .blackColor()
        tagsField.selectedTextColor = .whiteColor()
        
        // Events
        tagsField.onDidAddTag = { _,wsTag in
            let tag = Tag(value:["name":wsTag.text])
            self.selectedTags.append(tag)
        }
        
        tagsField.onDidRemoveTag = { _,tag in
            let tagModel = Tag()
            tagModel.name = tag.text
            if self.filterSelected(tagModel){
                self.removeTagFromSelectedList(tagModel)
            }
            
            
        }
        
        tagsField.onDidChangeText = { _, text in
            self.tableView.hidden = false
            guard let text = text else { return }
            self.didAutoCompleteLoad(text)
        }
        
        tagsField.onDidBeginEditing = { _ in
            print("DidBeginEditing")
        }
        
        tagsField.onDidEndEditing = { _ in
            print("DidEndEditing")
        }
        
        tagsField.onDidChangeHeightTo = { sender, height in
            print("HeightTo \(height)")
            let cap = height - 38
            if cap > 0{
                self.constraintHeightCardView.constant =  100 + cap
            }
        }
        
        
    }
    func setUpTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.scrollEnabled = true
        tableView.hidden = true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension MeAddTagViewController:UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        tableView.hidden = false
        let text = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        if text.characters.count == 0{
            tableView.hidden = true
            tags.removeAll()
            tableView.reloadData()
        }else {
            didAutoCompleteLoad(text)
        }
        
        return true
    }
    func didAutoCompleteLoad(str:String){
        if str.characters.count == 0{
            self.tableView.hidden = true
            return
        }
        tags.removeAll()
        let predicate = NSPredicate(format: "name BEGINSWITH %@", str)
        let persistantTags = uiRealm.objects(Tag.self).filter(predicate)
        log.info(persistantTags)
        for tag in persistantTags {
            if tag.name == str{
                tags.append(tag)
                tableView.reloadData()
                return
            }
            tags.append(tag)
        }
        tableView.reloadData()
        if tags.count == 0 {
            StudentHelper.getTags(str,tagsCompletionHandler: { (success,tags) in
                if success {
                    guard let tags = tags else { return }
                    for tag in tags{
                        self.tags.append(tag)
                    }
                    self.tableView.reloadData()
                }
            })
        }
        return
    }
    
    func addTagToSelectedList(tag:Tag){
        tag.setSelected(true)
        tagsField.addTag(tag.name)
    
        
    }
    func removeTagFromSelectedList(tag:Tag){
        var index = 0
        var flagExist = false
        for sTag in self.selectedTags {
            if sTag.name == tag.name{
                flagExist = true
                break
            }
            index += 1
        }
        if flagExist{
            selectedTags.removeAtIndex(index)
            tagsField.removeTag(tag.name)
            tag.setSelected(false)
        }
    }
}

extension MeAddTagViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tags.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tag = tags[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("tagCell", forIndexPath: indexPath) as! MeTagViewCell
        tag.setSelected(filterSelected(tag))
        cell.tagModel = tag
        cell.configure()
        return cell
    }
    
    func filterSelected(tag:Tag)->Bool{
        for sTag in self.selectedTags {
            if sTag.name == tag.name{
                return true
            }
        }
        return false
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row >= tags.count{
            return
        }
        let tag = tags[indexPath.row]
        filterSelected(tag) == true ? removeTagFromSelectedList(tag) : addTagToSelectedList(tag)
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    }
}
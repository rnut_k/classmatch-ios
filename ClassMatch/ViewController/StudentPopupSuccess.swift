//
//  StudentPopupSuccess.swift
//  ClassMatch
//
//  Created by Mac on 9/18/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class StudentPopupSuccess: UIViewController {

    var studying:Studying!
    //label
    @IBOutlet weak var labelDay:UILabel!
    @IBOutlet weak var labelDating:UILabel!
    @IBOutlet weak var labelPlace:UILabel!
    @IBOutlet weak var labelperiod:UILabel!
    @IBOutlet weak var labelPrice:UILabel!
    @IBOutlet weak var labelDegree:UILabel!
    @IBOutlet weak var labelAge:UILabel!
    @IBOutlet weak var labelNumberOfStudent:UILabel!
    @IBOutlet weak var labelTag:UILabel!
    @IBOutlet weak var labelMessage:UILabel!
    //button
    @IBOutlet weak var buttonEdit:UIButton!
    @IBOutlet weak var buttonSuccess:UIButton!
    
    @IBAction func buttonEditPressed(sender:AnyObject){
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    @IBAction func buttonSuccessPressed(sender:AnyObject){
        showLoadingHudViewController { 
            StudentHelper.createStudentPost(self.studying) { (success) in
                if success{
                    self.dismissViewControllerAnimated(false, completion: {
                        self.popview()
                    })
                }else{
                    self.showErrorHudViewController(MESSAGE_ERROR.STUDYING_POST.rawValue, completion: nil)
                }
            }
        }
    }
    func configureViewModelValue(){
        labelDay.text = studying.getDaysTitle()
        labelDating.text = studying.date?.toString(false)
        labelPlace.text = studying.location?.getFullDetail()
        labelPrice.text = String(studying.price)
        labelperiod.text = studying.period
        labelDegree.text = studying.degree?.name
        labelTag.text = studying.getTagsTitle()
        labelNumberOfStudent.text = String(studying.number)
        labelMessage.text = studying.message
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBarHidden = true
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        configureViewModelValue()
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.navigationBarHidden = false
    }
    func popview(){
        ad.gotoTabbarViewController(1)
    }
}

//
//  LoginViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/20/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController {
    
    var flagIsFreezeView = false
    let vm = LoginViewModel()
    var buttonFacebookLogin:FBSDKLoginButton!
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonLogin: RoundButton!
    @IBOutlet weak var viewFacebookLogin: UIView!
    
    @IBAction func buttonBackPressed(sender:AnyObject){
        let entryViewController = Utility.getEntryViewController()
        dismissFreezeView { 
            self.presentViewController(entryViewController, animated: true, completion: nil)
        }
        
    }
    @IBAction func buttonLoginPreesed(sender:AnyObject){
        moveViewToOriginal()
        if let email = textFieldEmail.text where textFieldEmail.text != ""{
            if let password = textFieldPassword.text where textFieldPassword.text != ""{
                showLoadingHud(nil)
                vm.startLogin(email, passWord: password)
            }
        }else{
            print("information not complete")
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.delegate = self
        addFacebookLoginUI()
        textFieldEmail.delegate = self
        textFieldPassword.delegate = self
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        addGestureTapToDismissKeyboard(view)
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    func addFacebookLoginUI(){
        buttonFacebookLogin = FBSDKLoginButton()
        buttonFacebookLogin.readPermissions = ["public_profile","email"]
        buttonFacebookLogin.frame = CGRectMake(0, 0, viewFacebookLogin.bounds.size.width, viewFacebookLogin.bounds.size.height)
        viewFacebookLogin.addSubview(buttonFacebookLogin)
        buttonFacebookLogin.delegate = vm
    }
    func addGestureTapToDismissKeyboard(view:UIView){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissAllInputField))
        view.addGestureRecognizer(tap)
    }
    func dismissAllInputField(){
        view.endEditing(true)
    }
    func moveViewOverlapWithKeyboard(){
        let newRect = CGRectMake(view.frame.origin.x, view.frame.origin.y - 200, view.bounds.size.width, view.bounds.size.height)
        view.frame = newRect
    }
    func moveViewToOriginal(){
        let originalRect = CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height)
        view.frame = originalRect
    }
    
}

extension LoginViewController:UIViewControllerTransitioningDelegate{
    func dismissFreezeView(completion:(()->Void)?){
        if flagIsFreezeView {
            self.dismissViewControllerAnimated(true){
                
                completion?()
            }
            self.flagIsFreezeView = false
        }else{
            completion?()
        }
    }
    func showLoadingHud(completion:(()->Void)?){
        dismissFreezeView { 
            let progress = HUDProgressViewController(status: "Doing...")
            self.flagIsFreezeView = true
            self.presentViewController(progress, animated: true, completion: {
                
            completion?()
            })
            
        }
    }
    func showErrorHud(message:String,completion:(()->Void)?){
        dismissFreezeView { 
            let alertController = UIAlertController(title: ERROR_MESSAGE.alertError.rawValue, message: message, preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
                self.flagIsFreezeView = false
            })
            alertController.addAction(defaultAction)
            self.flagIsFreezeView = true
            self.presentViewController(alertController, animated: true, completion: {
                
                completion?()
            })
            
            
        }
        
        
    }
}

extension LoginViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        moveViewOverlapWithKeyboard()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        moveViewToOriginal()
    }
}
extension LoginViewController:LoginViewModelDelegate{
    func onLoginError() {
        print("\nLoginViewController::ERROR::onLoginError\n")
        showErrorHudViewController(ERROR_MESSAGE.login.rawValue, completion: nil)
//        showErrorHud(ERROR_MESSAGE.login.rawValue,completion: nil)
    }
    func onLoginSuccess(jwt:String) {
        print("\nLoginViewController::Success::onLoginSuccess\n")
        ad.gotoTabbarViewController(1)
    }
    
    func onGetUserInforError() {
        print("\nLoginViewController::ERROR::onGetUserInforError\n")
        showErrorHudViewController(ERROR_MESSAGE.getUserInfo.rawValue, completion: nil)
    }
}

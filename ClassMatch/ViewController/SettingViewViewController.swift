//
//  SettingViewViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/28/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
class SettingViewViewController: UIViewController {

    @IBOutlet weak var buttonSignOut:UIButton!
    @IBAction func buttonSignOutPressed(sender:AnyObject){
        signOut()
        
    }
    func showErrorMessage(message:String,completion:(()->Void)?){
        let alertController = UIAlertController(title: ERROR_MESSAGE.alertError.rawValue, message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: ERROR_MESSAGE.alertOk.rawValue, style: .Default, handler: { (action) in
            
        })
        alertController.addAction(defaultAction)
        self.presentViewController(alertController, animated: true, completion: {
            completion?()
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.hidden = true
    }
    func signOut(){
        let alertController = UIAlertController(title: "ยืนยันการออกจากระบบ", message: "คุณแน่ใจว่าต้องการออกจากระบบ", preferredStyle: .ActionSheet)
        let confirmAction = UIAlertAction(title: "ยืนยัน", style: .Cancel) { (action) in
            self.presentLoadingHud()
            AuthenticationManager.signOut { (success) in
                ad.gotoEntryViewController()
            }
        }
        let cancelAction = UIAlertAction(title: "ยกเลิก", style: .Default) { (action) in
            self.dismissViewControllerAnimated(false, completion: nil)
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        
    }
}

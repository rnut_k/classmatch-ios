//
//  TeacherViewController.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import Foundation
import XLPagerTabStrip
class TeacherViewController: UITableViewController,IndicatorInfoProvider {

    
    var itemInfo: IndicatorInfo = "หาครู"
    var vm = TeacherPostViewModel()
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let adjustForTabbarInsets = UIEdgeInsetsMake(0, 0, 75, 0);
        self.tableView.contentInset = adjustForTabbarInsets
        self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
        tableView.delegate = vm
        tableView.dataSource = vm
        vm.delegate = self
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        vm.requestData()
    }
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return self.itemInfo
    }
}
extension TeacherViewController:TeacherPostViewModelDelegate{
    func didBeginStartRequest() {
//        tableView.beginUpdates()
    }
    func didEndStartRequest() {
//        tableView.endUpdates()
        tableView.reloadData()
    }
    
}



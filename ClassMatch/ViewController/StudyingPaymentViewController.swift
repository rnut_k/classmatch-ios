//
//  StudyingPaymentViewController.swift
//  ClassMatch
//
//  Created by Mac on 9/18/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import ALCameraViewController
class StudyingPaymentViewController: UIViewController {
    var admissionId:Int!
    @IBOutlet weak var labelExpireDate: UILabel!
    @IBOutlet weak var buttonUpload:UIButton!
    
    
    @IBAction func closeButtonPressed(sender: AnyObject) {
        self.dismissFreezeViewController { 
            self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    
    @IBAction func uploadButtonPressed(sender: AnyObject) {
        let cameraViewController = CameraViewController(croppingEnabled: true, allowsLibraryAccess: true) { (image, asset) in
            self.dismissViewControllerAnimated(false, completion: {
                if let image = image{
                    self.showLoadingHudViewController(nil)
                    self.uploadImageTransaction(image)
                }
            })
        }
        presentViewController(cameraViewController, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        labelExpireDate.text = ""
        getExpireDate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func getExpireDate(){
        TeacherManager.getAdmissionExpireDate(self.admissionId) { (success, isExpire, expireDate) in
            if success{
                guard let isExpire = isExpire else { return }
                if isExpire {
                    self.showErrorHudViewController(ERROR_MESSAGE.admission_expired.rawValue, completion: {
                        self.dismissFreezeViewController({ 
                            self.dismissViewControllerAnimated(false, completion: nil)
                        })
                    })
                }
                
                guard let expireDate = expireDate else { return }
                self.labelExpireDate.text = expireDate
            }else{
                self.showErrorHudViewController(ERROR_MESSAGE.admision_expired_error.rawValue, completion: { 
                    self.dismissViewControllerAnimated(false, completion: nil)
                })
            }
        }
    }
    func uploadImageTransaction(image:UIImage){
        let admissionIdString = String(admissionId)
        TeacherManager.createAdmissionTransaction(admissionIdString, image: image) { (success) in
            if(success){
                self.dismissFreezeViewController({
                    self.displayAlert("อัพโหลดหลักฐานการโอนเงินเรียบร้อยแล้ว กรุณารอการยืนยันจากระบบซักครู่ค่ะ")
                })
            }else{
                
            }
            self.dismissFreezeViewController(nil)
        }
    }
    func displayAlert(message:String){
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        let nextAction = UIAlertAction(title: "ตกลง", style: .Default) { _ in
            self.buttonUpload.enabled = false
        }
        alertController.addAction(nextAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func displayStudyingApprovedViewController(){
        ad.gotoStudyingApprovedViewController()
    }

}




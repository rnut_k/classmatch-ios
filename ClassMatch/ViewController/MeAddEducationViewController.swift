//
//  MeEducationViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/28/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit
class MeAddEducationViewController: UIViewController {

    enum InputMode:Int{
        case yearPicker  = 0
        case levelPicker = 1
    }
    var inputMode:InputMode?
    let education = Education()
    let kKeyboardHeight:CGFloat = 400
    var viewFrameBackup:CGRect!
    var levelsPickerViewDatasource = [Level]()
    var yearsDataSource = [String]()
    var activeTextField:UITextField!
    //MARK: IBOUTLET
    var pickerViewInput:UIPickerView!
    @IBOutlet weak var buttonPickerDone: UIButton!
    @IBOutlet weak var pickerToolBarView:UIView!
    @IBOutlet weak var textfieldDegree: UITextField!
    @IBOutlet weak var textfieldSchool: UITextField!
    @IBOutlet weak var textfieldFaculty: UITextField!
    @IBOutlet weak var textfieldSubFaculty: UITextField!
    @IBOutlet weak var textfieldPeriodBegin: UITextField!
    @IBOutlet weak var textfieldPeriodEnd: UITextField!
    @IBOutlet weak var textfieldGrade: UITextField!
    @IBOutlet weak var textfieldOther: UITextField!
    
    
    @IBAction func buttonSavePressed(sender: AnyObject) {
        self.showLoadingHudViewController(nil)
        education.name = textfieldSchool.text
        education.detail = textfieldFaculty.text
        education.subDetail = textfieldSubFaculty.text
        education.grade = textfieldGrade.text
        var period = ""
        if let periodBegin = textfieldPeriodBegin.text where textfieldPeriodBegin.text != ""{
            period += periodBegin
        }
        if let periodEnd = textfieldPeriodEnd.text where textfieldPeriodEnd.text != ""{
            period += " - " + periodEnd
        }
        education.period = period
        ProfileManager.createEducation(education) { (success, education) in
            if success{
                log.info("SUCCESS")
                if let education = education{
                    do{
                        try uiRealm.write({
                            let user = User.getSharedInStance()
                            user!.educations.append(education)
                            uiRealm.add(user!, update: true)
                            self.dismissFreezeViewController({ 
                                self.dismissViewControllerAnimated(false, completion: nil)
                            })
                        })
                    }catch(let error){
                        return log.error(error)
                    }
                }
                
            }
        }
    }
    @IBOutlet weak var buttonSave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "โปรไฟล์"
        setUpPickerView()
        setUpTextfield()
        changeBackButtonToCloseButton()
        loadLevelsDataSource()
        loadYearPickerDataSource()
        
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        viewFrameBackup = view.frame
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    func loadYearPickerDataSource(){
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy"
        if let i2 = Int(formatter.stringFromDate(NSDate())){
            for i in 1960...i2 {
                yearsDataSource.append(String(i))
            }
        }
    }
    func loadLevelsDataSource(){
        if let rLevels = Level.getAll(){
            levelsPickerViewDatasource = rLevels
            return
        }
        ProfileManager.getLevels { (success, levels) in
            if success{
                self.levelsPickerViewDatasource = levels!
            }
        }
        
    }
    func setUpTextfield(){
        textfieldDegree.placeholder = "เลือกระดับการศึกษา"
        textfieldSchool.placeholder = "โรงเรียน / มหาวิทยาลัย"
        textfieldFaculty.placeholder = "คณะ"
        textfieldSubFaculty.placeholder = "สาขาวิชา"
        textfieldPeriodBegin.placeholder = "ตั้งแต่ปี"
        textfieldPeriodEnd.placeholder = "ถึงปี (ประมาณ)"
        textfieldGrade.placeholder = "เกรดเฉลี่ย"
        textfieldOther.placeholder = "อื่นๆ (กิจกรรม, รางวัลที่ได้รับ, วิชาที่เรียน)"
        textfieldGrade.keyboardType = .DecimalPad
        textfieldDegree.delegate = self
        textfieldSchool.delegate = self
        textfieldFaculty.delegate = self
        textfieldSubFaculty.delegate = self
        textfieldPeriodBegin.delegate = self
        textfieldPeriodEnd.delegate = self
        textfieldGrade.delegate = self
        textfieldOther.delegate = self
        
        textfieldPeriodBegin.inputView = pickerViewInput
        textfieldPeriodEnd.inputView = pickerViewInput
        textfieldDegree.inputView = pickerViewInput
    }


}

extension MeAddEducationViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true
        
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextField = textField
        switch textField {
        case textfieldDegree:
            inputMode = InputMode.levelPicker
            break
        case textfieldPeriodBegin:
            inputMode = InputMode.yearPicker
            break
        case textfieldPeriodEnd:
            inputMode = InputMode.yearPicker
            break
        default:
            break
        }
    }
    func setUpPickerView(){
        pickerViewInput = UIPickerView(frame: CGRect(x: 0, y: self.view.bounds.width - 250, width: self.view.bounds.width, height: 250))
        pickerViewInput.delegate = self
        pickerViewInput.dataSource = self
        pickerViewInput.backgroundColor = colorBase
    }
}


extension MeAddEducationViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let inputMode = self.inputMode else { return 0 }
        switch (inputMode) {
        case InputMode.yearPicker:
            return yearsDataSource.count
        default:
            return levelsPickerViewDatasource.count
        }
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let inputMode = self.inputMode else { return }
        switch (inputMode) {
        case InputMode.yearPicker:
            let choose = yearsDataSource[row]
            activeTextField.text = choose
            break
        default:
            let choose = levelsPickerViewDatasource[row]
            education.level = choose
            activeTextField.text = choose.name
            break
        }
        
        
        
    }
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        guard let inputMode = self.inputMode else { return nil }
        switch (inputMode) {
        case InputMode.yearPicker:
            let string =  yearsDataSource[row]
            return NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        default:
            let string =  levelsPickerViewDatasource[row].name
            return NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
        }
        
    }
}

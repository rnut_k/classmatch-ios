//
//  LoginViewModel.swift
//  ClassMatch
//
//  Created by Mac on 8/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit

protocol LoginViewModelDelegate {
    func onLoginSuccess(jwt:String)
    func onLoginError()
}
class LoginViewModel:NSObject {
    
    
    var delegate:LoginViewModelDelegate!
    
    
    func startLogin(email:String,passWord:String){
        AuthenticationManager.login(email, passWord: passWord) { (success, jwt) in
            if success{
                if let jwt = jwt{
                    self.delegate.onLoginSuccess(jwt)
                    return
                }
            }
            print("\nERROR::LoginViewModel::startLogin\n")
            self.delegate.onLoginError()
        }
    }
}

extension LoginViewModel:FBSDKLoginButtonDelegate{
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if let result = result{
            if result.isCancelled{
                print("ERROR::LoginViewModel::didCompleteWithResult::isCancelled")
                self.delegate.onLoginError()
                return
            }
            let token = result.token.tokenString
            print("loginButtondidCompleteWithResult : \(token)")
            startLoginWithFacebook(token)
        }
    }
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func startLoginWithFacebook(token:String){
        AuthenticationManager.loginWithFacebook(token) { (success, jwt) in
            if success{
                if let jwt = jwt{
                    return self.delegate.onLoginSuccess(jwt)
                    
                }
            }else{
                log.error("ERROR::LoginViewModel::startLoginWithFacebook")
                return self.delegate.onLoginError()
            }
            
        }
        
    }
}

//
//  EntryViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/20/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit
class EntryViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    override func viewDidLoad() {
        collectionView.delegate = self
        collectionView.dataSource = self
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
}

extension EntryViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("entrycell", forIndexPath: indexPath) as! EntryCell
        let image:UIImage!
        let stringHeading:String!
        let stringDetail:String!
        switch indexPath.row {
        case 0:
            image = UIImage(named: "entry1")
            stringHeading = "เลือก \"งาน\" สอนพิเศษ"
            stringDetail = "รวมทุกวิชา ทุกระดับชั้น จากทุกแหล่ง\nกับวัน เวลา และสถานที่ที่ติวเตอร์สะดวก"
            break
        case 1:
            image = UIImage(named: "entry2")
            stringHeading = "เลือก \"ครู\" สอนพิเศษ"
            stringDetail = "ในวิชาและหัวข้อเฉพาะที่อยากเรียน\nกับติวเตอร์คุณภาพที่พร้อมเข้าใจ"
            break
        case 2:
            image = UIImage(named: "entry3")
            stringHeading = "แชทคุยกันโดยตรง"
            stringDetail = "นัดหมายสถานที่และเวลาเรียน\nตามตกลงกันเอง"
            break
        default:
            image = UIImage(named: "entry4")
            stringHeading = "ตามข่าวสาร"
            stringDetail = "แอดมิชชั่น รับตรง คะแนนสูง-ต่ำย้อนหลัง \nเข้าโรงเรียนชั้นนำ มหาวิทยาลัยทั้งในและต่างประเทศ"
            break
        }
        cell.imageView.image = image
        cell.labelHeading.text = stringHeading
        cell.labelDetail.text = stringDetail
        return cell
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let collectionViewSize = collectionView.bounds.size
        return collectionViewSize
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageWidth = CGRectGetWidth(scrollView.frame)
        pageControl.currentPage = Int(collectionView.contentOffset.x / pageWidth)
    }
}

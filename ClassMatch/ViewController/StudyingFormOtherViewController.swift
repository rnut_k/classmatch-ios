//
//  StudyingFormOtherViewController.swift
//  ClassMatch
//
//  Created by Arnut on 12/13/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
enum FormOtherMode{
    case age
    case degree
    case tag
    case dating
    case message
}
class StudyingFormOtherViewController: UIViewController {
    var studying:Studying!
    var mode:FormOtherMode = .age
    var agePickerData = [
        ["key":"1 ปี" ,"value":"1 ปี"],
        ["key":"3 ปี" ,"value":"3 ปี"],
        ["key":"10 ปี" ,"value":"10 ปี"]
    ]
    var degreePickerData = [Degree]()
    var datingPickerData = [
        ["key":"เสานี้เป็นต้นไป" ,"value":"เสานี้เป็นต้นไป"],
        ["key":"เสาร์หน้าเป็นไป" ,"value":"เสาร์หน้าเป็นไป"],
        ["key":"เดือนหน้า" ,"value":"เดือนหน้า"]
    ]
    let circleBullet = UIImage(named: IMAGENAME.circle_bullet.rawValue)
    let verifyBullet = UIImage(named: IMAGENAME.certified_without_text.rawValue)
    var pickerViewInput:UIPickerView!
    @IBOutlet weak var bulletText6:UILabel!
    @IBOutlet weak var bulletText7:UILabel!
    @IBOutlet weak var bulletText8:UILabel!
    @IBOutlet weak var bulletText9:UILabel!
    @IBOutlet weak var bulletText10:UILabel!
    @IBOutlet weak var bullet6:UIImageView!
    @IBOutlet weak var bullet7:UIImageView!
    @IBOutlet weak var bullet8:UIImageView!
    @IBOutlet weak var bullet9:UIImageView!
    @IBOutlet weak var bullet10:UIImageView!
    
    @IBOutlet weak var textfieldAge:UITextField!
    @IBOutlet weak var textfieldDegree:UITextField!
    @IBOutlet weak var textfieldTags:UITextField!
    @IBOutlet weak var textfieldDating:UITextField!
    @IBOutlet weak var textfieldMessage:UITextField!
    
    @IBAction func buttonNextPressed(sender:UIButton){
        if !validateData(){
            return showAlertViewController("Classmatch", message: "กรุณากรอกข้อมูลให้ครบถ้วนค่ะ", completion: nil)
        }
        if let text = textfieldTags.text{
            studying.tags.removeAll()
            let tagArray = splitTags(text)
            for item in tagArray {
                let tag = Tag(name: item)
                studying.tags.append(tag)
            }
        }
        if let message = textfieldMessage.text{
            studying.message = message
        }
        return performSegueWithIdentifier(SEGUE.studying_form_success.rawValue, sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "โพสต์เรียน"
        setUpDataSource()
        setUpPickerView()
        setUpTextfield()
    }
    func setUpDataSource(){
        StudentHelper.getDegrees { (success, degree) in
            if success{
                if let degrees = degree{
                    self.degreePickerData = degrees
                }
            }
        }
    }
    func setUpPickerView(){
        pickerViewInput = UIPickerView(frame: CGRect(x: 0, y: self.view.bounds.width - 250, width: self.view.bounds.width, height: 250))
        pickerViewInput.delegate = self
        pickerViewInput.dataSource = self
        pickerViewInput.backgroundColor = colorBase
    }
    func setUpTextfield(){
        textfieldAge.delegate = self
        textfieldDegree.delegate = self
        textfieldTags.delegate = self
        textfieldDating.delegate = self
        textfieldMessage.delegate = self
        
        textfieldAge.inputView = pickerViewInput
        textfieldDegree.inputView = pickerViewInput
        textfieldDating.inputView = pickerViewInput
    }
    
    func validateData()->Bool{
        var validated = true
        if textfieldAge.text == ""{
            validated = false
            bullet6.image = circleBullet
            bulletText6.hidden = false
        }else{
            bullet6.image = verifyBullet
            bulletText6.hidden = true
        }
        
        if textfieldDegree.text == ""{
            validated = false
            bullet7.image = circleBullet
            bulletText7.hidden = false
        }else{
            bullet7.image = verifyBullet
            bulletText7.hidden = true
        }
        
        if textfieldTags.text == ""{
            validated = false
            bullet8.image = circleBullet
            bulletText8.hidden = false
        }else{
            bullet8.image = verifyBullet
            bulletText8.hidden = true
        }
        
        if textfieldDating.text == ""{
            validated = false
            bullet9.image = circleBullet
            bulletText9.hidden = false
        }else{
            bullet9.image = verifyBullet
            bulletText9.hidden = true
        }
        
        if textfieldMessage.text == ""{
            validated = false
            bullet10.image = circleBullet
            bulletText10.hidden = false
        }else{
            bullet10.image = verifyBullet
            bulletText10.hidden = true
        }
        return validated
    }
    
    
    
    //MARK: PREPARE FOR SEGUE
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.destinationViewController {
        case let studentPopupSuccess as StudentPopupSuccess:
            studentPopupSuccess.modalPresentationStyle = .Custom
            studentPopupSuccess.studying = studying
        default:
            break
        }
    }
 
    
}

extension StudyingFormOtherViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        switch textField {
        case textfieldAge:
            mode = .age
            break
        case textfieldDegree:
            mode = .degree
            break
        case textfieldTags:
            mode = .tag
            break
        case textfieldDating:
            mode = .dating
            break
        case textfieldMessage:
            mode = .message
            break
        default:
            break
        }
        pickerViewInput.reloadAllComponents()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        validateData()
    }
}

//MARK: Helper
func getDateFromReadableString(text:String)->NSDate{
    return NSDate()
}
func splitTags(str:String)->[String]{
    let strArr = str.characters.split{$0 == " "}.map(String.init)
    return strArr
}

//MARK: PICKERVIEW
extension StudyingFormOtherViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch mode {
        case .age:
            return agePickerData.count
        case .degree:
            return degreePickerData.count
        case .dating:
            return datingPickerData.count
        default:
            return 0
        }
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch mode {
        case .age:
            let val = agePickerData[row]["value"]
            textfieldAge.text = val
            studying.age = val
        case .degree:
            let degree = degreePickerData[row]
            textfieldDegree.text = degree.name
            studying.degree = degree
        case .dating:
            let value = datingPickerData[row]["value"]
            let date = getDateFromReadableString(value!)
            studying.date = date
            textfieldDating.text = value
        default:
            break
        }
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var str = ""
        switch mode {
        case .age:
            str = agePickerData[row]["key"]!
            break
        case .degree:
            str = degreePickerData[row].name
            break
        case .dating:
            str = datingPickerData[row]["key"]!
            break
        default:
            break
        }
        return NSAttributedString(string: str, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }
}
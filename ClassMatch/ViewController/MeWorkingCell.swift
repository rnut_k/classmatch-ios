//
//  MeWorkingTableViewCell.swift
//  ClassMatch
//
//  Created by Mac on 8/27/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class MeWorkingCell: UITableViewCell {
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var labelDetail:UILabel!
    @IBOutlet weak var labelperiodTime:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(item:Working){
        
        labelName.text = item.name
        
        if let position = item.position{
            labelDetail.text = position
        }
        
        if let periodTime = item.period{
            labelperiodTime.text = periodTime
        }
    }

}

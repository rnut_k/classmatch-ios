//
//  StudyingFormGeneralViewController.swift
//  ClassMatch
//
//  Created by Arnut on 12/13/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import GooglePlacePicker
enum TextFieldMode{
    case subject
    case place_home
    case place_other
    case period
    case price
}

class StudyingFormGeneralViewController: UIViewController {

    var subjectPickerData = [Subject]()
    var periodPickerData = [
        ["key":"1 ชั่วโมง","value":"1 ชั่วโมง"],
        ["key":"2 ชั่วโมง","value":"2 ชั่วโมง"],
        ["key":"3 ชั่วโมง","value":"3 ชั่วโมง"],
        ["key":"ครึ่งวัน","value":"ครึ่งวัน"],
        ]
    var pickerViewInput:UIPickerView!
    var mode:TextFieldMode = .subject
    var studying:Studying!
    let imageToggleActive = UIImage(named: IMAGENAME.controlToggleActive.rawValue)
    let imageToggleInActive = UIImage(named: IMAGENAME.controlToggleInActive.rawValue)
    let circleBullet = UIImage(named: IMAGENAME.circle_bullet.rawValue)
    let verifyBullet = UIImage(named: IMAGENAME.certified_without_text.rawValue)
    @IBOutlet weak var bulletText1:UILabel!
    @IBOutlet weak var bulletText2:UILabel!
    @IBOutlet weak var bulletText3:UILabel!
    @IBOutlet weak var bulletText4:UILabel!
    @IBOutlet weak var bulletText5:UILabel!
    @IBOutlet weak var bullet1:UIImageView!
    @IBOutlet weak var bullet2:UIImageView!
    @IBOutlet weak var bullet3:UIImageView!
    @IBOutlet weak var bullet4:UIImageView!
    @IBOutlet weak var bullet5:UIImageView!
    
    @IBOutlet weak var textFieldSubject:UITextField!
    @IBOutlet weak var textFieldPlaceHome:UITextField!
    @IBOutlet weak var textFieldPlaceOther:UITextField!
    @IBOutlet weak var textFieldPeriod:UITextField!
    @IBOutlet weak var textFieldPrice:UITextField!
    
    @IBOutlet weak var controlDay1:UIButton!
    @IBOutlet weak var controlDay2:UIButton!
    @IBOutlet weak var controlDay3:UIButton!
    @IBOutlet weak var controlDay4:UIButton!
    @IBOutlet weak var controlDay5:UIButton!
    @IBOutlet weak var controlDay6:UIButton!
    @IBOutlet weak var controlDay7:UIButton!
    
    @IBOutlet weak var controlPlaceHome:UIButton!
    @IBOutlet weak var controlPlaceOther:UIButton!
    
    
    @IBOutlet weak var controlPrice250:UIButton!
    @IBOutlet weak var controlPrice300:UIButton!
    @IBOutlet weak var controlPrice500:UIButton!
    @IBOutlet weak var controlPrice750:UIButton!
    @IBOutlet weak var controlPrice1000:UIButton!
    @IBOutlet weak var controlPriceCustom:UIButton!
    
    @IBOutlet weak var scrollView:UIScrollView!
    @IBAction func nextButtonPressed(sender:UIButton){
        if !validateData(){
            return showAlertViewController("Classmatch", message: "กรุณากรอกรายละเอียดให้ครบถ้วนค่ะ", completion: nil)
        }
        return performSegueWithIdentifier(SEGUE.studying_form_other.rawValue, sender: nil)
    }
    //MARK: VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "โพสต์เรียน"
        setUpPickerViewDataSource()
        setUpPickerView()
        setUpTextField()
        setUpPriceControl()
        setUpDayControl()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        setUpScrollView()
    }
    //MARK: validate data
    func validateData()->Bool{
        var validated = true
        if textFieldSubject.text == ""{
            validated = false
            bullet1.image = circleBullet
            bulletText1.hidden = false
        }else{
            
            bullet1.image = verifyBullet
            bulletText1.hidden = true
        }
        if studying.days.count == 0{
            validated = false
            bullet2.image = circleBullet
            bulletText2.hidden = false
        }else{
            bullet2.image = verifyBullet
            bulletText2.hidden = true
        }
        if studying.location == nil{
            validated = false
            bullet3.image = circleBullet
            bulletText3.hidden = false
        }else{
            bullet3.image = verifyBullet
            bulletText3.hidden = true
        }
        if textFieldPeriod.text == ""{
            validated = false
            bullet4.image = circleBullet
            bulletText4.hidden = false
        }else{
            bullet4.image = verifyBullet
            bulletText4.hidden = true
        }
        if studying.price == 0{
            validated = false
            bullet5.image = circleBullet
            bulletText5.hidden = false
        }else{
            bullet5.image = verifyBullet
            bulletText5.hidden = true
        }
        
        return validated
    }
    //MARK: SETUP
    func setUpPriceControl(){
        controlPrice250.setImage(imageToggleInActive, forState: .Normal)
        controlPrice300.setImage(imageToggleInActive, forState: .Normal)
        controlPrice500.setImage(imageToggleInActive, forState: .Normal)
        controlPrice750.setImage(imageToggleInActive, forState: .Normal)
        controlPrice1000.setImage(imageToggleInActive, forState: .Normal)
        controlPriceCustom.setImage(imageToggleInActive, forState: .Normal)
    }
    func setUpDayControl(){
        controlDay1.setImage(imageToggleInActive, forState: .Normal)
        controlDay2.setImage(imageToggleInActive, forState: .Normal)
        controlDay3.setImage(imageToggleInActive, forState: .Normal)
        controlDay4.setImage(imageToggleInActive, forState: .Normal)
        controlDay5.setImage(imageToggleInActive, forState: .Normal)
        controlDay6.setImage(imageToggleInActive, forState: .Normal)
        controlDay7.setImage(imageToggleInActive, forState: .Normal)
        
        controlDay1.tag = 1
        controlDay2.tag = 2
        controlDay3.tag = 3
        controlDay4.tag = 4
        controlDay5.tag = 5
        controlDay6.tag = 6
        controlDay7.tag = 7
    }
    func setUpPickerViewDataSource(){
        if let subjects = Subject.getSubjects(){
            subjectPickerData = subjects
        }else{
            StudentHelper.getSubjects({ (success, subjects) in
                if success{
                    if let subjects = subjects{
                        return self.subjectPickerData = subjects
                    }
                }
            })
        }
    }
    func setUpPickerView(){
        pickerViewInput = UIPickerView(frame: CGRect(x: 0, y: self.view.bounds.width - 250, width: self.view.bounds.width, height: 250))
        pickerViewInput.delegate = self
        pickerViewInput.dataSource = self
        pickerViewInput.backgroundColor = colorBase
    }
    func setUpTextField(){
        textFieldSubject.delegate = self
        textFieldPlaceHome.delegate = self
        textFieldPlaceOther.delegate = self
        textFieldPeriod.delegate = self
        textFieldPrice.delegate = self
        
        textFieldSubject.inputView = pickerViewInput
        textFieldPeriod.inputView = pickerViewInput
        textFieldPrice.keyboardType = .NumberPad
    }
    func setUpScrollView(){
        self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.size.width, height: 850)
    }
    
   
    //MARK: Price Control logic
    @IBAction func priceControlPressed(sender:UIButton){
        textFieldPrice.text = ""
        textFieldPrice.resignFirstResponder()
        switch sender {
        case controlPrice250:
            studying.price = 250
            break
        case controlPrice300:
            studying.price = 300
            break
        case controlPrice500:
            studying.price = 500
            break
        case controlPrice750:
            studying.price = 750
            break
        case controlPrice1000:
            studying.price = 1000
            break
        case controlPriceCustom:
            textFieldPrice.becomeFirstResponder()
            break
        default:
            break
        }
        clearRadioPriceControl()
        sender.setImage(imageToggleActive, forState: .Normal)
        validateData()
    }
    func clearRadioPriceControl(){
        controlPrice250.setImage(imageToggleInActive, forState: .Normal)
        controlPrice300.setImage(imageToggleInActive, forState: .Normal)
        controlPrice500.setImage(imageToggleInActive, forState: .Normal)
        controlPrice750.setImage(imageToggleInActive, forState: .Normal)
        controlPrice1000.setImage(imageToggleInActive, forState: .Normal)
        controlPriceCustom.setImage(imageToggleInActive, forState: .Normal)
    }
    
    //MARK: DAYS CONTROL logic
    @IBAction func daysControlPressed(sender:UIButton){
        let currentControl = sender
        if currentControl.selected{
            detachDays(currentControl.tag)
            currentControl.setImage(imageToggleInActive, forState: .Normal)
            currentControl.selected = false
        }else{
            attachDays(currentControl.tag)
            currentControl.setImage(imageToggleActive, forState: .Normal)
            currentControl.selected = true
        }
        validateData()
    }
    func attachDays(dayId:Int){
        if let dayModel = uiRealm.objectForPrimaryKey(Day.self, key: dayId) {
            studying.days.append(dayModel)
        }
    }
    func detachDays(dayId:Int){
        var flagFind = false
        var index = 0
        for day in studying.days {
            if day.id == dayId{
                flagFind = true
                break
            }
            index += 1
        }
        if flagFind{
            studying.days.removeAtIndex(index)
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SEGUE.studying_form_other.rawValue{
            let vc = segue.destinationViewController as! StudyingFormOtherViewController
            vc.studying = studying
        }
    }
}
//MARK: TEXTFIELD
extension StudyingFormGeneralViewController:UITextFieldDelegate{
    func textFieldDidEndEditing(textField: UITextField) {
        validateData()
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        switch textField {
        case textFieldSubject:
            mode = .subject
            pickerViewInput.reloadAllComponents()
            break
        case textFieldPlaceHome:
            presentGooglePlaceField()
            return false
        case textFieldPlaceOther:
            presentGooglePlaceField()
            return false
        case textFieldPeriod:
            mode = .period
            pickerViewInput.reloadAllComponents()
            break
        case textFieldPrice:
            break
        default:
            break
        }
        return true
    }
}

extension StudyingFormGeneralViewController: GMSAutocompleteViewControllerDelegate {
    //MARK: Place Control Logic
    @IBAction func placeControlPressed(sender:UIButton){
        mode = sender == controlPlaceHome ? .place_home : .place_other
        presentGooglePlaceField()
    }
    func resetControlPlace(){
        textFieldPlaceOther.text = ""
        textFieldPlaceHome.text = ""
        studying.location = nil
        controlPlaceHome.setImage(imageToggleInActive, forState: .Normal)
        controlPlaceHome.selected = false
        controlPlaceOther.setImage(imageToggleInActive, forState: .Normal)
        controlPlaceOther.selected = false
    }
    func presentGooglePlaceField() {
        resetControlPlace()
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.presentViewController(autocompleteController, animated: true, completion: nil)
    }
    func dismissGooglePlaceField(){
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    func cancelPickLocation(){
        dismissGooglePlaceField()
        resetControlPlace()
        validateData()
        
    }
    // Handle the user's selection.
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        let location = ParserHelper.parsePlace(place, save: true)
        var text = location.name
        if let detail = location.detail{
            text = text + detail
        }
        studying.location = location
        if mode == .place_home {
            textFieldPlaceHome.text = text
            textFieldPlaceHome.selected = true
            controlPlaceHome.setImage(imageToggleActive, forState: .Normal)
            controlPlaceOther.setImage(imageToggleInActive, forState: .Normal)
        }else{
            textFieldPlaceOther.text = text
            textFieldPlaceOther.selected = true
            controlPlaceOther.setImage(imageToggleActive, forState: .Normal)
            controlPlaceHome.setImage(imageToggleInActive, forState: .Normal)
        }
        dismissGooglePlaceField()
        validateData()
    }
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // TODO: handle the error.
        print("Error: ", error.description)
        cancelPickLocation()
    }
    
    // User canceled the operation.
    func wasCancelled(viewController: GMSAutocompleteViewController) {
        cancelPickLocation()
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}


//MARK: PICKERVIEW
extension StudyingFormGeneralViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch mode {
        case .subject:
            return subjectPickerData.count
        case .period:
            return periodPickerData.count
        default:
            return 0
        }
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch mode {
        case .subject:
            let subject = subjectPickerData[row]
            studying.subject = subject
            return textFieldSubject.text = subject.name
        case .period:
            let value = periodPickerData[row]["value"]
            let key = periodPickerData[row]["value"]
            studying.period = key
            return textFieldPeriod.text = value
        default:
            break
        }
    }
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var str = ""
        switch mode {
        case .subject:
            if let subjectName = subjectPickerData[row].name{
                str = subjectName
            }
            break
        case .period:
            str = periodPickerData[row]["value"]!
            break
        default:
            break
        }
        return NSAttributedString(string: str, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }
}

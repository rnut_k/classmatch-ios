//
//  StudentPostFormViewController.swift
//  ClassMatch
//
//  Created by Mac on 9/11/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
import UIKit
import GooglePlaces

class StudentPostFormViewController: UIViewController {

    let kKeyboardHeight:CGFloat = 330
    var collectionviewFrameBackup:CGRect!
    
    //MARK: VIEWMODEL
    var vm:StudentPostFormViewModel!
    
    //MARK: IBOUTLET
    @IBOutlet weak var pickerView:UIPickerView!
    @IBOutlet weak var buttonPickerDone:UIButton!
    @IBOutlet weak var buttonNext:UIButton!
    @IBOutlet weak var pageControl:UIPageControl!
    @IBOutlet weak var collectionView:UICollectionView!
    
    @IBOutlet weak var pickerViewToolbar: UIToolbar!
    
//    let registerUser = User()
    var indexPage = 0{
        didSet{
            pageControl.currentPage = indexPage
        }
    }
    //MARK: IBACTION
    @IBAction func buttonPickerDone(sender:UIButton){
        dismissAllInputField()
    }
    

    //MARK: GOTO NEXT PAGE
    @IBAction func buttonNextPreesed(sender:UIButton){
        dismissAllInputField()
        if indexPage < vm.pages-1 {
            validateData(indexPage)
        }else{
            //TODO: confirm success post
            performSegueWithIdentifier(SEGUE.confirm_success_post.rawValue, sender: nil)
        }
    }
    func gotoNextPage(){
        indexPage += 1
        let indexPath = NSIndexPath(forItem: indexPage, inSection: 0)
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        changeToBackButton()
    }
    @IBAction func buttonBackPreesed(sender:AnyObject){
        dismissAllInputField()
        if indexPage == 1{
            changeBackButtonToCloseButton()
        }
        if indexPage > 0 {
            indexPage -= 1
            let indexPath = NSIndexPath(forItem: indexPage, inSection: 0)
            collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        }else{
            navigationController?.popViewControllerAnimated(false)
        }
    }
    //MARK: Validate Data
    func validateData(index:Int) {
        switch index {
        case 0:
            vm.validateNumberOfStudent()
            break
        case 1:
            vm.validateMainData()
            break
        case 2:
            vm.validateOtherData()
        default:
            break
        }
    }
    
    func validateOtherData()->Bool{
        return false
    }
}

extension StudentPostFormViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vm = StudentPostFormViewModel()
        vm.delegate = self
        collectionView.dataSource = vm
        collectionView.delegate = vm
        self.automaticallyAdjustsScrollViewInsets = false
        changeBackButtonToCloseButton()
        setUpPickerView()
        

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        dismissAllInputField()
//        addGestureTapToDismissKeyboard(view)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        collectionviewFrameBackup = collectionView.frame
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
}




//MARK: UI LOGIC
extension StudentPostFormViewController{
//    func addGestureTapToDismissKeyboard(view:UIView){
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissAllInputField))
//        view.addGestureRecognizer(tap)
//    }
//    func detectIsKeyboardOverlap(frame:CGRect){
//        let keyboardPoint = CGRectMake(0, self.view.bounds.size.height - kKeyboardHeight, self.view.bounds.size.width, self.view.bounds.size.height)
//        if CGRectContainsRect(keyboardPoint,frame ){
//            let newY = frame.origin.y - keyboardPoint.origin.y
//            let newRect = CGRectMake(collectionView.frame.origin.x, collectionView.frame.origin.y - newY, collectionView.bounds.size.width, collectionView.bounds.size.height)
//            collectionView.frame = newRect
//        }else{
//            resetCollectionViewFrame()
//        }
//        
//    }
    func setUpPickerView(){
        pickerView.delegate = vm
        pickerView.dataSource = vm
        pickerView.backgroundColor = colorBase
    }
    func presentPickerView(){
        pickerView.reloadAllComponents()
        pickerView.hidden = false
        pickerViewToolbar.hidden = false
    }
    func presentKeyboard(){
        dismissAllInputField()
    }
    func dismissAllInputField(){
//        resetCollectionViewFrame()
        pickerViewToolbar.hidden = true
        pickerView.hidden = true
//        view.endEditing(true)
    }
    func resetCollectionViewFrame(){
        if collectionviewFrameBackup != nil{
            collectionView.frame = collectionviewFrameBackup
        }
        
    }
    func presentGooglePlaceField() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = vm
        self.presentViewController(autocompleteController, animated: true, completion: nil)
    }
    func dismissGooglePlaceField(){
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    //create own back button
    func changeToBackButton(){
        let image = UIImage(named: "backButton")
        let backButton = UIBarButtonItem(image: image, style: .Plain, target: self, action: #selector(StudentPostFormViewController.buttonBackPreesed(_:)))
        self.navigationItem.leftBarButtonItem = backButton;
    }
    override func changeBackButtonToCloseButton(){
        let image = UIImage(named: "closeButton")
        let backButton = UIBarButtonItem(image: image, style: .Plain, target: self, action: #selector(StudentPostFormViewController.buttonBackPreesed(_:)))
        self.navigationItem.leftBarButtonItem = backButton;
    }
}

//MARK: StudentPostFormDelegate
extension StudentPostFormViewController: StudentPostFormDelegate {
    func didErrorValidate(index:Int){
        showErrorHudViewController("คุณกรอกข้อมูลไม่ครบถ้วน กรุณาลองใหม่อีกครั้งค่ะ", completion:nil)
        collectionView.reloadData()
    }
    func didSuccessValidate(index: Int) {
        gotoNextPage()
    }
    func didReloadCollectionView() {
        collectionView.reloadData()
    }
    func didOpenPicker(sender:AnyObject) {
        log.debug("didOpenPicker")
        presentPickerView()
//        if let textfield = sender as? UITextField{
//            detectIsKeyboardOverlap(textfield.frame)
//        }
    }
    func didPresentKeyboard(sender:AnyObject){
        presentKeyboard()
//        if let textfield = sender as? UITextField{
//            detectIsKeyboardOverlap(textfield.frame)
//        }
    }
    func shouldPresentGooglePlaceField(){
        presentGooglePlaceField()
    }
    func shouldDismissGooglePlaceField(){
        dismissGooglePlaceField()
    }
    
    func didDismissKeyboard(sender:AnyObject){
        dismissAllInputField()
        resetCollectionViewFrame()
    }
    func didDismissInput() {
        dismissAllInputField()
//        resetCollectionViewFrame()
    }

}

extension StudentPostFormViewController:UIScrollViewDelegate{

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageWidth = CGRectGetWidth(scrollView.frame)
        indexPage = Int(collectionView.contentOffset.x / pageWidth)
        pageControl.currentPage = indexPage
        
    }
}

//MARK: PREPARE FOR SEGUE
extension StudentPostFormViewController{
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.destinationViewController {
        case let studentPopupSuccess as StudentPopupSuccess:
            studentPopupSuccess.modalPresentationStyle = .Custom
//            studentPopupSuccess.vm = vm
        default:
            break
        }
    }
}

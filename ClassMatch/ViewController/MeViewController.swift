//
//  MeViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/27/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit
import AVFoundation
import ALCameraViewController
import Alamofire
enum ME_CELL_IDENTIFIER:String{
    case whosee = "whoseeCell"
    case basicInfo = "basicInfoCell"
    case iconstatus = "iconStatusCell"
    case speciality = "specialityCell"
    case education = "educationCell"
    case working = "workingCell"
    case testing = "testingCell"
    case rating = "ratingCell"
    case review = "reviewCell"
    case header_tags = "headerTagsCell"
    case header_working = "headerWorkingCell"
    case header_education = "headerEducationCell"
    case header_testing = "headerTestingCell"
}

enum ME_SECTION_TITLE:String{
    case speciality = "หัวข้อถนัดเฉพาะ"
    case education = "ประวัติการศึกษา"
    case working = "ประวัติการทำงาน"
    case testing = "คะแนนสอบวัดผล"
    case rating = "คะแนนจากนักเรียนและผู้ปกครอง"
    case review = "รีวิวจากนักเรียนและผู้ปกครอง"
}

class MeViewController: UIViewController,UIViewControllerTransitioningDelegate {

    @IBOutlet weak var tableView:UITableView!
    var vm = MeViewModel()
    var flagIsFreezeView = false
    var flagWhoSeeCellHeight = CGFloat(90.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Me"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        vm.delegate = self
        callApiGetProfile()
    }
    
    func updateImage(){
        if let imageModel = User.getSharedInStance()!.image{
            if let path = imageModel.path{
                ImageHelper.downLoadImage(path, completionHandler: { (success, image) in
                    if success{
                        log.info("SUCCESS::IMAGEPROFILE::DOWNLOAD")
                        self.vm.meImage = image
                        return self.tableView.reloadData()
                    }
                    return log.error()
                })
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.hidden = false
        tableView.reloadData()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    func callApiGetProfile(){
        
        ProfileManager.getProfile { (success) in
            if success{
                User.getSharedInStance()!.teacher?.save()
                User.getSharedInStance()!.save()
                log.info(User.getSharedInStance()!.teacher?.save())
                self.tableView.reloadData()
                self.updateImage()

                
            }
        }
    }
}
extension MeViewController{
    func hideWhoSeeCell(){
        flagWhoSeeCellHeight = 0
        tableView.reloadData()
    }
}
extension MeViewController:UITableViewDataSource,UITableViewDelegate{
    //MARK: NUMBER OF SECTION
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 7
        return 5
    }
    //MARK: NUMBER OF ROW
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 2:
            return User.getSharedInStance()!.educations.count
        case 3:
            return User.getSharedInStance()!.workings.count
        case 4:
            return User.getSharedInStance()!.testings.count
        default:
            return 1
        }
    }
    
    //MARK: ROW CUSTOM
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.whosee.rawValue, forIndexPath: indexPath) as! MeWhoSeeCell
                cell.buttonCloseWhoSeeCell.addTarget(self, action: #selector(MeViewController.hideWhoSeeCell), forControlEvents: .TouchUpInside)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.basicInfo.rawValue) as! MeBasicInfoCell
                cell.vm = vm
                cell.configureCell()
                cell.imageViewProfileImage.image = vm.meImage
                cell.removeSeparator(cell.bounds.width)
                return cell
            default:
                let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.iconstatus.rawValue, forIndexPath: indexPath) as! MeStatusCell
                cell.buttonPublic.addTarget(self, action: #selector(self.publicProfilePressed), forControlEvents: .TouchUpInside)
                cell.addSectionTopSeparator(tableView, color: tableView.separatorColor!)
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.speciality.rawValue, forIndexPath: indexPath) as! MeSpecialityCell
            cell.configureCell()
            return cell
        case 2:
            let education = User.getSharedInStance()!.educations[indexPath.row]
            let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.education.rawValue, forIndexPath: indexPath) as! MeEducationCell
            cell.vm = vm
            cell.configureCell(education)
            cell.removeSeparator(cell.bounds.width)
            cell.buttonApprove.addTarget(self, action: Selector(self.performEvidenceEducation(indexPath)), forControlEvents: .TouchUpInside)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.working.rawValue, forIndexPath: indexPath) as! MeWorkingCell
            let working = User.getSharedInStance()!.workings[indexPath.row]
            cell.configureCell(working)
            cell.removeSeparator(cell.bounds.width)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.testing.rawValue, forIndexPath: indexPath) as! MeTestingCell
            let testing = User.getSharedInStance()!.testings[indexPath.row]
            cell.vm = vm
            cell.testing = testing
            cell.configure()
            return cell
        case 5:
            let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.review.rawValue, forIndexPath: indexPath)
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier(ME_CELL_IDENTIFIER.rating.rawValue, forIndexPath: indexPath)
            return cell
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                //who see
                return flagWhoSeeCellHeight
            case 1:
                //basicInfo
                return 305
            default:
                //whosee
                return 88
            }
        case 1:
            //speciality
            return 55
        case 2:
            //education
            return 110
        case 3:
            //working
            return 90
        default:
            return 90
        }
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var identifier:String!
        
        var color = colorBase
        switch section {
            
        case 0:
            return nil
        case 1:
            identifier = ME_CELL_IDENTIFIER.header_tags.rawValue
            color = tableView.separatorColor
            break
        case 2:
            identifier = ME_CELL_IDENTIFIER.header_education.rawValue
            break
        case 3:
            identifier = ME_CELL_IDENTIFIER.header_working.rawValue
            break
        case 4:
            identifier = ME_CELL_IDENTIFIER.header_testing.rawValue
            break
        default:
            return nil
        }
        let headerCell = tableView.dequeueReusableCellWithIdentifier(identifier) as! MeHeaderCell
        headerCell.addSectionTopSeparator(tableView, color: color)
        return headerCell

    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 30
        }
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return nil
        case 1:
            return ME_SECTION_TITLE.speciality.rawValue
        case 2:
            return ME_SECTION_TITLE.education.rawValue
        case 3:
            return ME_SECTION_TITLE.working.rawValue
        case 4:
            return ME_SECTION_TITLE.testing.rawValue
        case 5:
            return ME_SECTION_TITLE.rating.rawValue
        case 6:
            return ME_SECTION_TITLE.review.rawValue
        default:
            return nil
        }
    }
    func performEvidence(type:EvidenceType,item:AnyObject){
        if type == .edu{
            guard let education = item as? Education else { return }
            performSegueWithIdentifier(SEGUE.popup_evidence_education.rawValue, sender: education)
            return
        }
        guard let testing = item as? Testing else { return }
        performSegueWithIdentifier(SEGUE.popup_evidence_testing.rawValue, sender: testing)
        return
    }
    func performEvidenceEducation(indexPath:NSIndexPath){
        
    }
    func performEvidenceTesting(indexPath:NSIndexPath){
        
    }
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        switch indexPath.section {
//        case 2:
//            //            return ME_SECTION_TITLE.education.rawValue
//            performSegueWithIdentifier(SEGUE.popup_evidence_education.rawValue, sender: User.getSharedInStance()!.educations[indexPath.row])
//        case 4:
//            //            return ME_SECTION_TITLE.testing.rawValue
//            performSegueWithIdentifier(SEGUE.popup_evidence_testing.rawValue, sender: User.getSharedInStance()!.testings[indexPath.row])
////        case 0:
////        case 1:
//////            return ME_SECTION_TITLE.speciality.rawValue
////        case 3:
//////            return ME_SECTION_TITLE.working.rawValue
////        case 5:
//////            return ME_SECTION_TITLE.rating.rawValue
////        case 6:
//////            return ME_SECTION_TITLE.review.rawValue
//        default:
//            return
//        }
//        
//    }
}

//MARK: PREPAREFORSEGUE
extension MeViewController{
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.destinationViewController {
        case let teachingConfirmViewController as TeachingConfirmViewController:
            teachingConfirmViewController.modalPresentationStyle = .Custom
            teachingConfirmViewController.teacher = User.getSharedInStance()!.teacher
        case let popupViewController as MePopupEvidenceViewController:
            popupViewController.modalPresentationStyle = .Custom
            guard let identifier = segue.identifier else{ return }
            guard let sender = sender else{ return }
            switch identifier {
            case SEGUE.popup_evidence_education.rawValue:
                popupViewController.evidenceType = .edu
                let education = sender as! Education
                popupViewController.evidence_id = education.id
                break
            case SEGUE.popup_evidence_testing.rawValue:
                popupViewController.evidenceType = .test
                let testing = sender as! Testing
                popupViewController.evidence_id = testing.id
                break
            default:
                break
            }
            break
        default:
            break
        }
        

    }
    func publicProfilePressed(){
        ProfileManager.checkIsTeacher { (success, isTeacher) in
            if success && isTeacher{
                return self.performSegueWithIdentifier(SEGUE.me_public_info.rawValue, sender: nil)
            }
            return self.performSegueWithIdentifier("teaching_create", sender: nil)
        }
    }
}
extension MeViewController:MeViewModelDelegate{
    func didChoosePublicMenu() {
        return self.performSegueWithIdentifier(SEGUE.me_public_info.rawValue, sender: nil)
    }
    func didChooseImagePicker() {
        let cameraViewController = CameraViewController(croppingEnabled: true, allowsLibraryAccess: true) { (image, asset) in
            print("CameraViewController::choosed")
            self.dismissViewControllerAnimated(false, completion: {
                if let image = image{
                    self.showLoadingHudViewController(nil)
                    self.uploadImageProfile(image)
                }
            })
        }
        presentViewController(cameraViewController, animated: true, completion: nil)
    }
    
    func uploadImageProfile(image:UIImage){
        ProfileManager.updateImageProfile(image) { (success) in
            if(success){
                self.vm.meImage = image
                self.tableView.reloadData()
            }
        }
    }
}

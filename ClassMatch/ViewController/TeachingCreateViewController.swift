//
//  TeachingCreateViewController.swift
//  ClassMatch
//
//  Created by Mac on 9/23/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import HUDKit

class TeachingCreateViewController: UIViewController {

    var vm = TeacherCreateViewModel()
    
    
    @IBOutlet weak var imageViewProfile:UIImageView!
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var labelDisplayable:UILabel!
    @IBOutlet weak var labelSubject1:UILabel!
    @IBOutlet weak var labelSubject2:UILabel!
    @IBOutlet weak var labelSubject3:UILabel!
    @IBOutlet weak var labelSubject4:UILabel!
    
    @IBOutlet weak var buttonPost:UIButton!
    @IBOutlet weak var tableView:UITableView!
    
    @IBAction func buttonPost(sender:AnyObject){
        self.showLoadingHudViewController(nil)
        let vc = self.parentViewController?.presentingViewController as! TeachingConfirmViewController
        vc.teacher = vm.teacher
        vc.dismissViewControllerAnimated(true, completion: nil)
//        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func resetLabel(){
        let imagePlaceHolder = "placeholder-profile-square"
        imageViewProfile.image = UIImage(named: imagePlaceHolder)
        labelName.text = User.getSharedInStance()!.name
        labelDisplayable.text = ""
        labelSubject1.text = ""
        labelSubject2.text = ""
        labelSubject3.text = ""
        labelSubject4.text = ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.delegate = self
        vm.requestDataSource()
        changeBackButtonToCloseButton()
        tableView.dataSource = vm
        tableView.delegate = vm
        resetLabel()
        getImageProfile()
        
        
    }
    func getImageProfile(){
        let user = User.getSharedInStance()!
        guard let path = user.image?.path else {return}
        ImageHelper.downLoadImage(path) { (success, image) in
            if success {
                return self.imageViewProfile.image = image
            }
            self.showErrorHudViewController(MESSAGE_ERROR.DOWNLOAD_IMAGE.rawValue, completion: nil)
        }
    }
    

    

}

extension TeachingCreateViewController:TeacherCreateViewModelDelegate {
    func didReloadData() {
        tableView.reloadData()
    }
    func didUpdateLabelDisplayable(profile:String) {
        labelDisplayable.text = profile
    }
    func didUpdateLabelSubjects() {
        var index =  0
        for subject in vm.teacher.subjects {
            switch index {
            case 0:
                labelSubject1.text = subject.name!
                break
            case 1:
                labelSubject2.text = subject.name!
                break
            case 2:
                labelSubject3.text = subject.name!
                break
            default:
                labelSubject4.text = subject.name!
                break
            }
            index += 1
        }
    }
}


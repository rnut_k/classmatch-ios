//
//  StudentPostIntroduceViewController.swift
//  ClassMatch
//
//  Created by Mac on 10/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class StudentPostIntroduceViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        changeBackButtonToCloseButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

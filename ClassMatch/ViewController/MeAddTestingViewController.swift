//
//  MeEducationViewController.swift
//  ClassMatch
//
//  Created by Mac on 8/28/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class MeAddTestingViewController: UIViewController {
    var viewFrameBackup:CGRect!
    var activeTextField:UITextField!
    //MARK: IBOUTLET
    @IBOutlet weak var buttonPickerDone: UIButton!
    var pickerViewInput:UIDatePicker!
    @IBOutlet weak var pickerToolBarView:UIView!
    
    @IBOutlet weak var textfieldName: UITextField!
    @IBOutlet weak var textfieldResult: UITextField!
    @IBOutlet weak var textfieldDating: UITextField!
    @IBOutlet weak var textfieldOther: UITextField!
    
    @IBAction func buttonSavePressed(sender: AnyObject) {
        showLoadingHudViewController(nil)
        let testingModel = Testing()
        testingModel.name = textfieldName.text
        testingModel.result                     = textfieldResult.text
        testingModel.message = textfieldOther.text
        if let date = textfieldDating.text {
            let dateformatter = NSDateFormatter()
            dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateObj = dateformatter.dateFromString(date)
            testingModel.dating = dateObj
        }
        ProfileManager.createTesting(testingModel) { (success) in
            if success{
                User.getSharedInStance()!.addTesting(testingModel)
                self.dismissFreezeViewController({ 
                    self.dismissViewControllerAnimated(false, completion: nil)
                })
            }else{
                self.showErrorHudViewController(ERROR_MESSAGE.default_error.rawValue, completion: nil)
            }
        }
    }
    @IBOutlet weak var buttonSave: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "โปรไฟล์"
        setUpPickerView()
        setUpTextfield()
        changeBackButtonToCloseButton()
        pickerViewInput.addTarget(self, action: #selector(handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    

    func setUpTextfield(){
        
        
        textfieldName.placeholder = "ชื่อการสอบวัดผล"
        textfieldResult.placeholder = "ผลคะแนนสอบ"
        textfieldDating.placeholder = "วันที่"
        textfieldDating.inputView = pickerViewInput
        textfieldOther.placeholder = "อื่นๆ (คะแนนแต่ละส่วน ฯลฯ)"
        textfieldOther.delegate = self
        textfieldDating.delegate = self
        textfieldResult.delegate = self
        textfieldName.delegate = self
    
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

}

extension MeAddTestingViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextField = textField
    }
    func setUpPickerView(){
        pickerViewInput = UIDatePicker(frame: CGRect(x: 0, y: self.view.bounds.width - 250, width: self.view.bounds.width, height: 250))
        pickerViewInput.datePickerMode = .Date;
        pickerViewInput.backgroundColor = colorBase
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        textfieldDating.text = sender.date.toString()
    }
    
    

}




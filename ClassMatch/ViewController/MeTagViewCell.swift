//
//  MeTagViewCell.swift
//  ClassMatch
//
//  Created by Mac on 11/22/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class MeTagViewCell: UITableViewCell {

    var tagModel:Tag!
    @IBOutlet weak var labelBullet:UILabel!
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var selectStatusImageView:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        self.selectedTag = true
//        updateSelectUI()
    }

    func configure(){
        labelName.text = tagModel.name
        updateSelectUI()
    }
    func updateSelectUI(){
        if tagModel.flagSelected {
            labelBullet.textColor = colorSecond
            labelName.textColor = colorSecond
            selectStatusImageView.image = UIImage(named: IMAGENAME.true_sign.rawValue)
        }else {
            labelBullet.textColor = colorThird
            labelName.textColor = colorThird
            selectStatusImageView.image = UIImage(named: IMAGENAME.plus_sign.rawValue)
        }
    }
}

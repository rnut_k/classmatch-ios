//
//  StudentPostFormNumberOfStudentViewController.swift
//  ClassMatch
//
//  Created by Arnut on 12/13/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class StudyingFormNumberOfStudentViewController: UIViewController {

    let studying = Studying()
    var numberOfStudent = 0
    var numberPickerData =  [
        ["key": "2","value": 2],
        ["key": "3","value": 3],
        ["key": "4","value": 4],
        ["key": "5","value": 5],
        ["key": "มากกว่า 5","value": 6]
    ]
    
    @IBOutlet weak var labelMultipleUnit: UILabel!
    @IBOutlet weak var labelMultiple: UILabel!
    @IBOutlet weak var labelSingle: UILabel!
    @IBOutlet weak var textfieldNumber:UITextField!
    @IBOutlet weak var imageButtonSingle:UIImageView!
    @IBOutlet weak var imageButtonMultiple:UIImageView!
    var pickerViewInput:UIPickerView!
    
    @IBAction func buttonNextPressed(sender:UIButton){
        if !validateNumberOfStudent(){
            return showAlertViewController("Classmatch", message: "กรุณาเลือกจำนวนนักเรียนที่ต้องการก่อนค่ะ", completion: nil)
        }
        studying.number = numberOfStudent
        return performSegueWithIdentifier(SEGUE.studying_form_general.rawValue, sender: nil)
    }
    func validateNumberOfStudent()->Bool{
        return numberOfStudent != 0 ? true : false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "โพสต์เรียน"
        changeBackButtonToCloseButton()
        setUpButton()
        setUpGesture()
        setUpPickerView()
        setUpTextField()
    }
    func setUpButton(){
        imageButtonSingle.image = UIImage(named: IMAGENAME.button_student_single_deAction.rawValue)
        imageButtonMultiple.image = UIImage(named: IMAGENAME.button_student_multiple_deAction.rawValue)
    }
    func setUpPickerView(){
        pickerViewInput = UIPickerView(frame: CGRect(x: 0, y: self.view.bounds.width - 250, width: self.view.bounds.width, height: 250))
        pickerViewInput.delegate = self
        pickerViewInput.dataSource = self
        pickerViewInput.backgroundColor = colorBase
    }
    
    func setUpGesture(){
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.chooseSingleStudent))
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.chooseMultipleStudent))
        imageButtonSingle.addGestureRecognizer(gesture)
        imageButtonMultiple.addGestureRecognizer(gesture2)
    }
    func setUpTextField(){
        textfieldNumber.delegate = self
        textfieldNumber.inputView = pickerViewInput
    }
    func chooseSingleStudent(sender:AnyObject){
        textfieldNumber.resignFirstResponder()
        numberOfStudent = 1
        //colorize single
        imageButtonSingle.image = UIImage(named: IMAGENAME.button_student_single_action.rawValue)
        labelSingle.textColor = colorBase
        //dim multiple
        imageButtonMultiple.image = UIImage(named: IMAGENAME.button_student_multiple_deAction.rawValue)
        textfieldNumber.text = ""
        labelMultiple.textColor = UIColor.grayColor()
        labelMultipleUnit.textColor = UIColor.grayColor()
    }
    func chooseMultipleStudent(){
        textfieldNumber.becomeFirstResponder()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let identifierSegue =  segue.identifier else { return }
        if identifierSegue == SEGUE.studying_form_general.rawValue{
            let vc = segue.destinationViewController as! StudyingFormGeneralViewController
            vc.studying = studying
        }
    }
}

extension StudyingFormNumberOfStudentViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(textField: UITextField) {
        
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        //dim single
        imageButtonSingle.image = UIImage(named: IMAGENAME.button_student_single_deAction.rawValue)
        labelSingle.textColor = UIColor.grayColor()
        //colorize multiple
        imageButtonMultiple.image = UIImage(named: IMAGENAME.button_student_multiple_action.rawValue)
        labelMultiple.textColor = colorBase
        labelMultipleUnit.textColor = colorBase
    }
}

//MARK: PICKERVIEW
extension StudyingFormNumberOfStudentViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numberPickerData.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = numberPickerData[row]["value"] as! Int
        let key = numberPickerData[row]["key"] as! String
        studying.number = value
        textfieldNumber.text = key
        
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let value = numberPickerData[row]["key"] as! String
        let str = value
        return NSAttributedString(string: str, attributes: [NSForegroundColorAttributeName:UIColor.whiteColor()])
    }
}

//
//  MeSpecialityCell.swift
//  ClassMatch
//
//  Created by Mac on 11/25/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import RealmSwift
class MeSpecialityCell: UITableViewCell {

    var tags:List<Tag>?
    @IBOutlet weak var labelTag:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(){
        guard let user = User.getSharedInStance() else { return }
        let tags = user.tags
        
        let myMutableString = NSMutableAttributedString()
        for tag in tags{
            //----bullet---//
            let attributeString = NSAttributedString(string: "●", attributes: [NSForegroundColorAttributeName:colorBase])
            myMutableString.appendAttributedString(attributeString)
            //-----tag.name---//
            let attributeStringTag = NSMutableAttributedString(string: tag.name, attributes: [NSForegroundColorAttributeName:colorBaseSecond])
            attributeStringTag.addAttributes([NSFontAttributeName:UIFont.systemFontOfSize(13.0)], range: NSRange(location:0,length:tag.name.characters.count))
            myMutableString.appendAttributedString(attributeStringTag)
            //----space---//
            let attributeStringSpace = NSAttributedString(string:"  ", attributes: [NSForegroundColorAttributeName:UIColor.blackColor()])
            myMutableString.appendAttributedString(attributeStringSpace)
        }
        labelTag.attributedText = myMutableString
        labelTag.numberOfLines = 0
        labelTag.sizeToFit()
    }

}

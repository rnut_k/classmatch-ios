//
//  TeacherPostViewModel.swift
//  ClassMatch
//
//  Created by Mac on 9/24/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit


protocol TeacherPostViewModelDelegate {
    func didBeginStartRequest()
    func didEndStartRequest()
//    func didReloadData()
}
class TeacherPostViewModel: NSObject {
    private var teachers = [Teacher]()
    var delegate:TeacherPostViewModelDelegate!
    
    
    func get(index:Int)->Teacher?{
        return teachers[index]
    }
    func count()->Int{
        return teachers.count
    }
    
    func requestData(){
        delegate.didBeginStartRequest()
        StudentHelper.getAllTeacher { (success, teachers) in
            if success{
                if let teachers = teachers{
                    log.info("teacher::\(teachers)")
                    self.teachers = teachers
                    return self.delegate.didEndStartRequest()
                }
            }
            return self.delegate.didEndStartRequest()
        }
    }
    override init() {
        super.init()
    }
    
    
}

extension TeacherPostViewModel:UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            //top suggestion
            return 0
        default:
            //all teacher
            return count()
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cellIdentifier = "teacher_header_cell"
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
            cell.removeSeparator(tableView.bounds.width)
            return cell
        case 1:
            let cellIdentifier = "teacher_filter_cell"
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
            cell.removeSeparator(tableView.bounds.width)
            return cell
        case 2:
            //top suggesttion
            let cell = tableView.dequeueReusableCellWithIdentifier("teacher_post_cell", forIndexPath: indexPath) as! TeacherPostCell
            cell.removeSeparator(tableView.bounds.width)
            return cell
        default:
            //all teachers
            let cell = tableView.dequeueReusableCellWithIdentifier("teacher_post_cell", forIndexPath: indexPath) as! TeacherPostCell
            cell.removeSeparator(tableView.bounds.width)
            guard let teacher = get(indexPath.row) else { return cell }
            cell.teacher = teacher
            cell.configureCell()
            
            guard let user = teacher.user else { return cell }
            guard let image = user.image else { return cell }
            guard let path = image.path else { return cell }
            ImageHelper.downLoadImage(path) { (success, image) in
                if success {
                    cell.imageViewProfile.image = image
                }
            }
            
            return cell
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 180
        case 1:
            return 55
        default:
            return 175
        }
    }
}

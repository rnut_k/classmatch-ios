//
//  StudentViewController.swift
//  ClassMatch
//
//  Created by Mac on 9/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import Foundation
import XLPagerTabStrip
import NYAlertViewController

class StudentViewController: UITableViewController,IndicatorInfoProvider{

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var postTableView:UITableView!
    //refresh control
    
    var flagDidHidenCollectionAndPostCell = false
    
    var postRefreshControl:UIRefreshControl!
    var itemInfo: IndicatorInfo = "หานักเรียน"
    var vm = StudentPostViewModel()
    
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.delegate = self
        vm.delegatePostCell = self
        collectionView.delegate = self
        collectionView.dataSource = self
        
        postTableView.delegate = vm
        postTableView.dataSource = vm
        self.automaticallyAdjustsScrollViewInsets = false
        initialRefreshControl()
        
    }
    
    func initialRefreshControl(){
        // Initialize the refresh control.
        postRefreshControl = UIRefreshControl()
        postRefreshControl.backgroundColor = UIColor.whiteColor()
        postRefreshControl.tintColor = colorBase
        postRefreshControl.addTarget(self, action: #selector(StudentViewController.requestData), forControlEvents: .ValueChanged)
        postTableView.addSubview(postRefreshControl)
        
    }
    func requestData(){
        updatePostRefreshControlTitle()
        vm.requestPostDataFromServer()
    }
    func updatePostRefreshControlTitle(){
        let formatter = NSDateFormatter()
        formatter.dateStyle = .MediumStyle
        let strTitleRefresh = "Last Updated: " + formatter.stringFromDate(NSDate())
        let attrsDictionary:[String:AnyObject] = [NSForegroundColorAttributeName:colorBase]
        let attribuiteTitle = NSAttributedString(string: strTitleRefresh, attributes: attrsDictionary)
        postRefreshControl.attributedTitle = attribuiteTitle
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        requestData()
    }
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return self.itemInfo
    }
    

}

//MARK: STATIC TABLEVIEW
extension StudentViewController{
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var heightData:[CGFloat]
        let gapForUser:CGFloat = 10
        var tabbarHeight:CGFloat = 0.0
        if let tabBarController = tabBarController{
            tabbarHeight = tabBarController.tabBar.bounds.size.height
        }
        if !flagDidHidenCollectionAndPostCell {
            heightData = [77,77,tableView.bounds.size.height - ( 77 + 67)  - tabbarHeight - gapForUser]
        }else{
            heightData = [0,0,tableView.bounds.size.height - tabbarHeight - gapForUser]
        }
        
        switch indexPath.row {
        case 0:
            return heightData[0]
        case 1:
            return heightData[1]
        default:
            return heightData[2]
        }
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAtIndexPath: indexPath)
        cell.removeSeparator(tableView.bounds.size.width)
        return cell;
    }
}

//MARK: COLLECTIONVIEW
extension StudentViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("subjectCell", forIndexPath: indexPath) as! SubjectCell
        let subject = vm.subjects[indexPath.row]
        cell.configureCell(subject)
        return cell
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vm.subjects.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let collectionViewSize = CGSizeMake(66, 66)
        return collectionViewSize
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
}


//MARK: View Model Delegate
extension StudentViewController:StudentPostViewModelDelegate{
    func didEndRequestSubjects() {
        collectionView.reloadData()
    }
    func didScrollToRow(index: Int) {
        if index == 5 {
            dispatch_async(dispatch_get_main_queue(), { 
                self.tableView.beginUpdates()
                print("didScrollToThirdRow::\(index)")
                self.flagDidHidenCollectionAndPostCell = true
                self.tableView.endUpdates()
            })
        }
        else if index == 0 {
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.beginUpdates()
                print("didScrollToThirdRow::\(index)")
                self.flagDidHidenCollectionAndPostCell = false
                self.tableView.endUpdates()
            })
        }
    }
    func beginRequest() {
        self.tableView.beginUpdates()
    }
    func endRequest() {
        self.tableView.endUpdates()
        self.tableView.reloadData()
        if postRefreshControl.refreshing{
            postRefreshControl.endRefreshing()
        }
    }
    func didFailRequest() {
        presentErrorPopup("เกิดข้อผิดพลาด", message: "ไม่สามารถเรียกข้อมูลการโพสต์หานักเรียนได้ กรุณาลองใหม่อีกครั้งภายหลังค่ะ")
    }
}
extension StudentViewController:StudentPostCellDelegate{
    func didTabAdmission(studying: Studying) {
        self.showLoadingHudViewController(nil)
        TeacherManager.getAdmissionQueue(studying) { (success,message,queueCount) in
            if success{
                return self.createAdmission(studying,queueCount: queueCount)
            }else{
                if let message = message{
                    if message == MESSAGE_API.NOT_AUTHORIZE.rawValue{
                        return self.presentErrorPopup("เกิดข้อผิดพลาด", message: "คุณยังไม่ได้รับอนุญาติให้สามารถรับสอน กรุณาลงทะเบียนเป็นคุณครูก่อนดำเนินการรับสอนค่ะ")
                    }
                }
                self.dismissFreezeViewController({
                    return self.presentErrorPopup("เกิดข้อผิดพลาดในระบบ", message: "กรุณาลองใหม่อีกครั้ง หรือติดต่อเจ้าหน้าที่เพื่อให้การช่วยเหลือค่ะ")
                })
            }
        }
    }
    func didTabSeeDetail(studying: Studying) {
        print("StudentViewController::didTabSeeDetail")
    }
    func createAdmission(studying:Studying,queueCount:Int){
        let alertViewController = createQueueComfirmationPopup(queueCount, subject: studying.subject!.name!)
        // Add alert actions
        let confirmAction = NYAlertAction(
            title: "ยืนยัน",
            style: .Cancel,
            handler: { (action: NYAlertAction!) -> Void in
                self.dismissViewControllerAnimated(true, completion: {
                    TeacherManager.createAdmission(studying) { (success, isActive,message) in
                        if success{
                            if isActive{
                                return self.presentStudyingApprovalViewController()
                            }else{
                                return self.presentErrorPopup("ยืนยันการรับสอนแล้ว", message: "ระบบจะแจ้งเตือนอีกครั้งเมื่อถึงคิวของท่าน ขอบคุณค่ะ")
                            }
                        }else{
                            if let message = message{
                                return self.presentErrorPopup("เกิดข้อผิดพลาด", message: message)
                            }
                            return self.presentErrorPopup("เกิดข้อผิดพลาด", message: "ไม่สามารถยืนยันการรับสอนของคุณได้ กรุณาลองใหม่อีกครั้งภายหลังค่ะ")
                        }
                    }
                })
            }
        )
        alertViewController.addAction(confirmAction)
        // Present the alert view controller
        if let presentedViewController =  self.presentedViewController{
            presentedViewController.dismissViewControllerAnimated(false, completion: {
                self.presentViewController(alertViewController, animated: true, completion: nil)
            })
        }else{
            self.presentViewController(alertViewController, animated: true, completion: nil)
        }
    }

}


//MARK: DISPLAY ANY VIEWCONTROLLER
extension StudentViewController{
    func presentErrorPopup(title:String,message:String){
        let alertViewController = NYAlertViewController()
        // Set a title and message
        alertViewController.title = title
        alertViewController.message = message
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 10.0
        alertViewController.cancelButtonTitleFont = UIFont(name: "AvenirNext-Medium", size: 16.0)
        alertViewController.cancelButtonColor = UIColor.clearColor()
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.transitionStyle = .Fade
        // Present the alert view controller
        if let presentedViewController =  self.presentedViewController{
            presentedViewController.dismissViewControllerAnimated(false, completion: { 
                self.presentViewController(alertViewController, animated: true, completion: nil)
            })
        }else{
            self.presentViewController(alertViewController, animated: true, completion: nil)
        }
    }
    func createQueueComfirmationPopup(queueCount:Int,subject:String)->NYAlertViewController{
        let alertViewController = NYAlertViewController()
        // Set a title and message
        var title = "คุณอยู่ในคิวลำดับ"
        if queueCount == 0{
            title += "แรก"
        }else{
            title += "ที่ " + String(queueCount)
        }
        alertViewController.title = title
        alertViewController.message = "ยืนยันรับสอน " + subject + "?"
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 10.0
        alertViewController.cancelButtonTitleFont = UIFont(name: "AvenirNext-Medium", size: 16.0)
        alertViewController.cancelButtonColor = UIColor.clearColor()
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.transitionStyle = .Fade
        return alertViewController
    }

    func presentStudyingApprovalViewController(){
        ad.gotoStudyingApprovalViewController()
    }
}



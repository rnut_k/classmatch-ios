//
//  AppDelegate+Auth.swift
//  ClassMatch
//
//  Created by Arnut on 12/9/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import Foundation
extension AppDelegate: AuthicationDelegate{
    func didNotAuthentication() {
        dismissHudViewController { }
    }
    //MARK: AuthenticationManager Delegate
    func didFailedAuthentication() {
        AuthenticationManager.signOut { (success) in
            self.presentAlertSessionExpire()
        }
    }
    func didSuccessAuthentication() {
        onLineFireBaseSystem()
        ProfileManager.getProfile { (success) in
            if success{
                
            }
        }
        ChatManager.getRooms { (success, rooms) in
            if success{
                if let rooms = rooms{
                    cm.addRooms(rooms)
                }
            }
        }
        //chat
        ChatManager.start()
        gotoTabbarViewController(1)
        
    }
}

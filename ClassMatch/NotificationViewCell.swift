//
//  NotificationViewCell.swift
//  ClassMatch
//
//  Created by Mac on 12/10/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
enum StatusNotification{
    case waiting
    case approved
    case active
    case cancel
}
class NotificationViewCell: UITableViewCell {
    private var status:StatusNotification = .waiting
    private var detail:String?
    @IBOutlet weak var labelAdmissionStatus:UILabel!
    @IBOutlet weak var labelDetail:UILabel!
    @IBOutlet weak var imageViewStatus:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelAdmissionStatus.text = ""
    }
    
    func configureCell(notificationStatus:StatusNotification,notificationDetail:String,admissionStatus:AdmissionStatus?){
        status = notificationStatus
        detail = notificationDetail
        //TODO: add image to imageViewStatus
        var statusImage:UIImage!
        switch status {
        case .waiting:
            statusImage = UIImage(named: IMAGENAME.icon_waiting.rawValue)
            break
        case .approved:
            statusImage = UIImage(named: IMAGENAME.certified_without_text.rawValue)
            break
        case .active:
            statusImage = UIImage(named: IMAGENAME.icon_active.rawValue)
            break
        case .cancel:
            statusImage = UIImage(named: IMAGENAME.icon_cancel.rawValue)
            break
        }
        imageViewStatus.image = statusImage
        guard let detail = detail else { return }
        labelDetail.text = detail
        
        if status == .approved{
            return labelAdmissionStatus.text = ""
        }
        
        guard let admissionStatus = admissionStatus else {
            return labelAdmissionStatus.text = ""
        }
        var strStatus = ""
        switch  admissionStatus {
        case .Waiting:
            strStatus = "ระหว่างรอคุณครูที่สนใจสอน"
            break
        case .Active:
            strStatus = "รอการยืนยันจากคุณ"
            break
        case .Approved:
            strStatus = "ระหว่างรอการดำเนินการของคุณครู"
            break
        case .Canceled:
            strStatus = "ระหว่างรอคุณครูคนใหม่ที่สนใจ"
            break
        }
        labelAdmissionStatus.text = strStatus
    }
    
    
    
    
}

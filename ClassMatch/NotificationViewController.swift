//
//  NotificationViewController.swift
//  ClassMatch
//
//  Created by Mac on 12/7/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var tableView:UITableView!
    var vm:NotificationViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        vm = NotificationViewModel(controller: self)
        setUpTableView()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        vm.callApi()
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    private func setUpTableView(){
        tableView.dataSource = vm
        tableView.delegate = vm
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

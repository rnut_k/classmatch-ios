//
//  AppDelegate.swift
//  ClassMatch
//
//  Created by Mac on 8/20/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//
import UIKit
import FBSDKCoreKit
import RealmSwift
import GooglePlaces
import Firebase
import FirebaseDatabase
import XCGLogger
import IQKeyboardManagerSwift
import HUDKit
let ad = UIApplication.sharedApplication().delegate as! AppDelegate
var cm:ChatManager = ChatManager()  
let log = XCGLogger(identifier: "advancedLogger", includeDefaultDestinations: false)
let uiRealm = try! Realm()
                     
                     
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var activeTime = NSDate()
    var window: UIWindow?
    var didProcessInNotification = false
    var myConnectionsRef:FIRDatabaseReference!
    var connectedRef:FIRDatabaseReference!
    var authManager = AuthenticationManager.sharedInstance
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //log
        setUpLog()
        //google map
        GMSPlacesClient.provideAPIKey("AIzaSyCBncDBwjhhtGUjvZ1aqVhO-ZD0TzfKeHQ")
        //firebase 
        FIRApp.configure()
        let notificationsTypes:UIUserNotificationType = [.Alert,.Badge,.Sound]
        let notificationSettings = UIUserNotificationSettings(forTypes: notificationsTypes, categories: nil)
        application.registerForRemoteNotifications()
        application.registerUserNotificationSettings(notificationSettings)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.firTokenRefreshNotificaiton),name: kFIRInstanceIDTokenRefreshNotification, object: nil)
        //auth
        authManager.delegate = self
        //facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        application.statusBarStyle = .LightContent
        //realm
        log.info("realm path \(Realm.Configuration.defaultConfiguration.description)")
        //IQKeyboard Manager
        IQKeyboardManager.sharedManager().enable = true
        
        
        return true
    }
    func applicationDidBecomeActive(application: UIApplication) {
        
        activeTime = NSDate()
        if AuthenticationManager.checkIsFirstCome(){
            ProfileManager.getBasicInformation()
        }else{
            presentHudInRootViewController()
            authManager.start()
        }
        
    }
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .Unknown)
    }
    
    func applicationWillResignActive(application: UIApplication) {
        FIRDatabase.database().goOffline()
    }
    func applicationDidEnterBackground(application: UIApplication) {
    }
    func applicationWillEnterForeground(application: UIApplication) {
    }
    func applicationWillTerminate(application: UIApplication) {
    }
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        let settings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // Print full message.
        print("%@", userInfo)
        didProcessInNotification = true
        application.applicationIconBadgeNumber = 0
        guard let message = userInfo["aps"] as? [String:AnyObject] else { return }
        guard let aps_category = message["category"] as? String else { return }
        
        switch aps_category {
        case NOTIFICATION_CATEGORY.STUDYING_ADMISSION_ACTIVE.rawValue:
            if let admissionId = userInfo["admission_id"]?.integerValue{
                log.info("category::studying_approval::admission_id::\(admissionId)")
                return gotoStudentApproveStudyingViewController(admissionId)
            }
            return log.error("ERROR::category::studying_approval::not found admissionId")
        case NOTIFICATION_CATEGORY.STUDYING_ADMISSION_APPROVED.rawValue:
            if let admissionId = userInfo["admission_id"]?.integerValue{
                log.info("category::studying_approval::admission_id::\(admissionId)")
                return gotoStudyingApprovalPaymentViewController(admissionId)
            }
            return log.error("ERROR::category::studying_approval::not found admissionId")
        case NOTIFICATION_CATEGORY.STUDYING_COMPLETE.rawValue:
            return gotoStudyingApprovedViewController()
        case NOTIFICATION_CATEGORY.STUDYING_TRANSACTION_RENEW.rawValue :
            if let admissionId = userInfo["admission_id"]?.integerValue{
                log.info("category::STUDYING_TRANSACTION_RENEW::admission_id::\(admissionId)")
                return presentAlertReNewTransaction(admissionId)
            }
            break
        case NOTIFICATION_CATEGORY.STUDYING_ADMISSION_CACELED.rawValue :
            if let admissionId = userInfo["admission_id"]?.integerValue{
                log.info("category::STUDYING_ADMISSION_CACELED::admission_id::\(admissionId)")
                return presentAlertAdmissionCanceled(admissionId)
            }
            break
        case NOTIFICATION_CATEGORY.CHAT.rawValue:
            switch application.applicationState {
            case .Inactive:
                return cm.tapMessageFromNotification(userInfo)
            default:
                return
            }
        default:
            break
        }
    }

    
    //MARK:FIREBASE
    func firTokenRefreshNotificaiton(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token(){
            User.setSharedAPN(refreshedToken)
            shouldSubscibeNotification()
        }
        connectToFcm()
    }
    func connectToFcm() {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    func shouldSubscibeNotification(){
        guard let user = User.getSharedInStance() else { return }
        guard let notificationId = User.getSharedAPN() else { return }
        let strUserId = String(user.id)
        let topic = "/topics/" + strUserId
        FIRMessaging.messaging().subscribeToTopic(topic)
        AuthenticationManager.updateApnToken(notificationId)
    }
    func onLineFireBaseSystem(){
        FBSDKAppEvents.activateApp()
        FIRDatabase.database().goOnline()
        guard let user = User.getSharedInStance() else { return }
        let strUserId = String(user.id)
        myConnectionsRef = FIRDatabase.database().referenceWithPath("Onlines/Users")
        connectedRef = FIRDatabase.database().referenceWithPath(".info/connected")
        connectedRef.observeEventType(.Value, withBlock: { snapshot in
            guard let connected = snapshot.value as? Bool where connected else {
                return
            }
            let con = self.myConnectionsRef.childByAutoId()
            con.setValue(strUserId)
            con.onDisconnectRemoveValue()
        })
    }
    func offLineFirebaseSystem(){
        FIRDatabase.database().goOffline()
        if let user = User.getSharedInStance(){
            let strUserId = String(user.id)
            let topic = "/topics/" + strUserId
            FIRMessaging.messaging().unsubscribeFromTopic(topic)
            FIRMessaging.messaging().disconnect()
            FIRInstanceID.instanceID().deleteIDWithHandler({ (error) in
                if error != nil{
                    log.error("description::\(error.debugDescription)")
                }else{
                    log.info("token deleted")
                }
            });
        }
    }
    
    
    //MARK: SETUP LOG
    func setUpLog(){
        // Create a logger object with no destinations
        // Create a destination for the system console log (via NSLog)
        let systemLogDestination = XCGNSLogDestination(owner: log, identifier: "advancedLogger.systemLogDestination")
        // Optionally set some configuration options
        systemLogDestination.outputLogLevel = .Debug
        systemLogDestination.showLogIdentifier = false
        systemLogDestination.showFunctionName = true
        systemLogDestination.showThreadName = true
        systemLogDestination.showLogLevel = true
        systemLogDestination.showFileName = true
        systemLogDestination.showLineNumber = true
        systemLogDestination.showDate = true
        // Add the destination to the logger
        log.addLogDestination(systemLogDestination)
        // Create a file log destination
        let fileLogDestination = XCGFileLogDestination(owner: log, writeToFile: "/path/to/file", identifier: "advancedLogger.fileLogDestination")
        // Optionally set some configuration options
        fileLogDestination.outputLogLevel = .Debug
        fileLogDestination.showLogIdentifier = false
        fileLogDestination.showFunctionName = true
        fileLogDestination.showThreadName = true
        fileLogDestination.showLogLevel = true
        fileLogDestination.showFileName = true
        fileLogDestination.showLineNumber = true
        fileLogDestination.showDate = true
        // Process this destination in the background
        fileLogDestination.logQueue = XCGLogger.logQueue
        // Add the destination to the logger
        log.addLogDestination(fileLogDestination)
        // Add basic app info, version info etc, to the start of the logs
        log.logAppDetails()
    }
}




                
                     
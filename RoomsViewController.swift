//
//  RoomsViewController.swift
//  ClassMatch
//
//  Created by Mac on 10/25/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit

class RoomsViewController: UITableViewController {
    let vm = RoomViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle()
        tableView.delegate = vm
        tableView.dataSource = vm
        vm.controller = self
    }
    func setTitle(){
        title = "Message"
    }
    override func viewDidAppear(animated: Bool) {
        if let shoutRoom =  cm.shoutRoom{
            gotoRoom(shoutRoom)
        }
        super.viewDidAppear(animated)
        vm.requestRooms()
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SEGUE.chat.rawValue{
            let chatViewController = segue.destinationViewController as! ChatViewController
            guard let room = sender as? Room else { return }
            chatViewController.room = room
        }
    }
    override func viewDidDisappear(animated: Bool) {
        vm.removeDelegateRoomMessage()
        super.viewDidDisappear(animated)
    }
    func gotoRoom(room:Room){
        performSegueWithIdentifier(SEGUE.chat.rawValue, sender: room)
    }
}

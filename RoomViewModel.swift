//
//  RoomViewModel.swift
//  ClassMatch
//
//  Created by Mac on 10/26/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
class RoomViewModel: NSObject,UITableViewDelegate,UITableViewDataSource {
    weak var controller:RoomsViewController!
    
    override init() {
        super.init()
    }
    convenience init(controller:RoomsViewController) {
        self.init()
        self.controller = controller
    }
    func get(index:Int)->Room?{
        if index < cm.roomObservers.count{
            return cm.roomObservers[index].room
        }
        return nil
    }
    func requestRooms(){
        ChatManager.getRooms { (success, rooms) in
            if success{
                guard let rooms = rooms else { return }
                cm.addRooms(rooms)
                self.initDelegateRoomMessage()
                self.controller.tableView.reloadData()
            }
        }
    }
    func initDelegateRoomMessage(){
        for room in cm.getRooms() {
            room.delegate = self
        }
    }
    func removeDelegateRoomMessage(){
        for room in cm.getRooms() {
            room.delegate = nil
        }
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cm.roomObservers.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CELL.ROOM_CELL.rawValue, forIndexPath: indexPath) as! RoomViewCell
        guard let room = get(indexPath.row) else{ return cell }
        cell._room = room
        cell.configureCell()
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        guard let room = get(indexPath.row) else{ return }
        controller.gotoRoom(room)
    }
}
//MARK: RoomMessageDelegate
extension RoomViewModel:RoomMessageDelegate{
    func roomDidChangeParnerReadMessageId(messageId: String) {
        
    }
    func roomDidReceiveMessage(room: Room, message: Message) {
        guard let index = cm.getRooms().indexOf(room) else { return }
        let indexPath = NSIndexPath(forRow: index, inSection: 0)
        controller.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    }
}

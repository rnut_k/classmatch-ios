//
//  RoomViewCell.swift
//  ClassMatch
//
//  Created by Mac on 10/25/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
import Haneke
class RoomViewCell: UITableViewCell {

    
    var _room:Room!
    @IBOutlet weak var imageViewProfile:UIImageView!
    @IBOutlet weak var labelName:UILabel!
    @IBOutlet weak var labelLastMessage:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        labelName.text = ""
        labelLastMessage.text = ""
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configureCell(){
        labelName.text = _room.getPartner().name
        if let lastMessage = _room.messages.last{
            labelLastMessage.text = lastMessage.text
        }
        guard let image = _room.getRoomImage() else { return }
        guard let path = image.path else{ return }
        ImageHelper.downLoadImage(path, completionHandler: { (success, image) in
            if success{
                if let img = image{
                    self.imageViewProfile.image = img
                }
            }
        })
    }
}

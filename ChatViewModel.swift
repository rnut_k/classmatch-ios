//
//  ChatViewModel.swift
//  ClassMatch
//
//  Created by Mac on 10/15/2559 BE.
//  Copyright © 2559 NStudio. All rights reserved.
//

import UIKit
//import Firebase
import FirebaseDatabase
class ChatViewModel: NSObject {
    
    var ref: FIRDatabaseReference!
    private var _refHandle: FIRDatabaseHandle!
    required override init() {
        super.init()
        ref = FIRDatabase.database().reference()
    }
    
    func configureDatabase(room:Room) {
        ref.child("Chat/Rooms").child(room.path)
    }
    
}
